package einvoice

import (
	"errors"

	jwt "github.com/dgrijalva/jwt-go"
)

type EInvoiceToken interface {
	CheckUnitCode(code string) bool
	GetSeller() string
	GetAuth() string
}

type NumaxEIToken jwt.MapClaims

func (n NumaxEIToken) GetSeller() string {
	data, ok := n["data"]
	if !ok {
		return ""
	}
	dm, ok := data.(map[string]interface{})
	if !ok {
		return ""
	}
	no, ok := dm["uniformNo"]
	if !ok {
		return ""
	}
	return no.(string)
}

func (n NumaxEIToken) CheckUnitCode(code string) bool {
	s := n.GetSeller()
	return s == code
}

func (n NumaxEIToken) GetAuth() string {
	data, ok := n["data"]
	if !ok {
		return ""
	}
	dm, ok := data.(map[string]interface{})
	if !ok {
		return ""
	}
	auth, ok := dm["auth"]
	if !ok {
		return ""
	}
	return auth.(string)
}

type NumaxConf struct {
	Url string
}

func (nc *NumaxConf) NumaxParseToken(tokenStr string) (EInvoiceToken, error) {
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		return []byte("root123456"), nil
	})
	if token == nil {
		return nil, errors.New("token is nil")
	}
	if token.Valid {
		mc := token.Claims.(jwt.MapClaims)
		return NumaxEIToken(mc), nil
	} else if ve, ok := err.(*jwt.ValidationError); ok {
		if ve.Errors&jwt.ValidationErrorMalformed != 0 {
			return nil, errors.New("That's not even a token")
		} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
			// Token is either expired or not active yet
			return nil, errors.New("Timing is everything")
		} else {
			return nil, err
		}
	}
	return nil, err
}
