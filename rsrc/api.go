package rsrc

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

type ApiError struct {
	StatusCode int
	Message    string
}

func NewApiError(code int, msg string) ApiError {
	return ApiError{StatusCode: code, Message: msg}
}

func (e ApiError) Error() string {
	return fmt.Sprintf("%v: %v", e.StatusCode, e.Message)
}

type APIHandler struct {
	Path   string
	Next   func(http.ResponseWriter, *http.Request)
	Method string
	Auth   bool
	Group  []string
}

type API interface {
	GetAPIs() []*APIHandler
	GetName() string
	Init()
}

type APIConf struct {
	Port        string                     `yaml:"port,omitempty"`
	EnableCORS  bool                       `yaml:"cors"`
	AllowSystem string                     `yaml:"allowSys,omitempty"`
	Middle      map[string]bool            `yaml:"middle,omitempty"`
	Apis        map[string]bool            `yaml:"api,omitempty"`
	Audit       map[string]map[string]bool `yaml:"audit,omitempty"`
}

func (ac *APIConf) apiEnable(name string) bool {
	if v, ok := ac.Apis[name]; ok {
		return v
	}
	return true
}

func (ac *APIConf) middleEnable(name string) bool {
	if v, ok := ac.Middle[name]; ok {
		return v
	}
	return true
}

func (ac *APIConf) getMiddleList(ml []Middle) []middleware {
	var middlewares []middleware
	for _, m := range ml {
		if !ac.middleEnable(m.GetName()) {
			continue
		}
		middlewares = append(middlewares, m.GetMiddleWare())
	}
	return middlewares
}

func (ac *APIConf) InitAPI(
	r *mux.Router,
	middles []Middle,
	authMiddle authMiddleInter,
	apis ...API,
) {
	ml := ac.getMiddleList(middles)
	for _, myapi := range apis {
		if !ac.apiEnable(myapi.GetName()) {
			continue
		}
		myapi.Init()
		for _, handler := range myapi.GetAPIs() {
			if authMiddle != nil {
				authMiddle.AddAuthPath(handler.Path, handler.Method, handler.Auth, handler.Group)
			}
			r.HandleFunc(handler.Path, buildChain(handler.Next, ml...)).Methods(handler.Method)
		}
	}
}

type authMiddleInter interface {
	AddAuthPath(path string, method string, isAuth bool, group []string)
}

type Middle interface {
	GetName() string
	GetMiddleWare() func(f http.HandlerFunc) http.HandlerFunc
}

type middleware func(http.HandlerFunc) http.HandlerFunc

// buildChain builds the middlware chain recursively, functions are first class
func buildChain(f http.HandlerFunc, m ...middleware) http.HandlerFunc {
	// if our chain is done, use the original handlerfunc
	if len(m) == 0 {
		return f
	}
	// otherwise nest the handlerfuncs
	return m[0](buildChain(f, m[1:len(m)]...))
}
