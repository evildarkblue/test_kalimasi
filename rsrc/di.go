package rsrc

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"time"

	"kalimasi/rsrc/db"
	"kalimasi/rsrc/einvoice"
	"kalimasi/rsrc/log"
	"kalimasi/rsrc/mail"
	"kalimasi/rsrc/storage"
	"kalimasi/util"

	"github.com/globalsign/mgo"
	yaml "gopkg.in/yaml.v2"
)

type di struct {
	Firebasedb *db.FirebaseDB       `yaml:"fireDB"`
	Mongodb    *db.MongoV2          `yaml:"mongodb,omitempty"`
	Search     *db.Searchv7         `yaml:"elastic,omitempty"`
	Log        *log.Logger          `yaml:"log,omitempty"`
	APIConf    APIConf              `yaml:"api,omitempty"`
	Location   *time.Location       `yaml:"-"`
	JWTConf    *JwtConf             `yaml:"jwtConf"`
	FBStorage  *storage.FileStorage `yaml:"fbStorage,omitempty"`
	GCP        struct {
		CredentialsFile string `yaml:"credentialsFile"`
	} `yaml:"gcp"`
	Report struct {
		FontPath string `yaml:"fontPath"`
	} `yaml:"report"`

	MailConf            *mail.SendGridConf `yaml:"mail,omitempty"`
	RedirectURL         map[string]string  `yaml:"redirectURL"`
	*einvoice.NumaxConf `yaml:"einvoice"`
}

func (d *di) GetFBStorage() storage.Storage {
	if d.FBStorage == nil {
		panic("not support")
	}
	s, err := d.FBStorage.GetStorage()
	if err != nil {
		panic(err)
	}
	return s
}

func (d *di) GetLog() *log.Logger {
	return d.Log
}

func (d *di) GetAPIConf() APIConf {
	return d.APIConf
}

func (d *di) GetJWTConf() *JwtConf {
	return d.JWTConf
}

func (d *di) GetLocation() *time.Location {
	return d.Location
}

func (d *di) GetMailServ() mail.MailServ {
	if d.MailConf == nil {
		panic("not support")
	}
	return &mail.SendGridServ{
		SendGridConf: d.MailConf,
	}
}

func (d *di) GetMongoByKey(k string) *mgo.Database {
	if d == nil || d.Mongodb == nil {
		panic("not init di")
	}
	d.Log.Debug("get mongo key is :" + k)
	return d.Mongodb.GetDB(k)
}

func (d *di) GetMongoByReq(r *http.Request) *mgo.Database {
	key := util.GetConnKey(r)
	return d.GetMongoByKey(key)
}

func (d *di) GetFirebaseByReq(r *http.Request) *db.FirebaseDB {
	return d.Firebasedb.New(r)
}

func (d *di) GetSearch() *db.Searchv7 {
	if d == nil || d.Search == nil {
		panic("not set search")
	}
	return d.Search
}

func (d *di) GetFontMap() map[string]string {
	return map[string]string{
		"tw-m": filepath.Join(d.Report.FontPath, "TW-Medium.ttf"),
		"tw-r": filepath.Join(d.Report.FontPath, "TW-Regular.ttf"),
	}
}

func (d *di) GetGCPCredentialPath() string {
	return d.GCP.CredentialsFile
}

func (d *di) GetRedirectUrl(k string) string {
	r, ok := d.RedirectURL[k]
	if !ok {
		return ""
	}
	return r
}

var mydi *di

func GetDI() *di {
	if mydi == nil {
		panic("not init di")
	}
	return mydi
}

func InitConfByFile(f string, timezone string) {
	yamlFile, err := ioutil.ReadFile(f)
	if err != nil {
		panic(err)
	}
	mydi = &di{}
	err = yaml.Unmarshal(yamlFile, mydi)
	if err != nil {
		panic(err)
	}
	loc, err := time.LoadLocation(timezone)
	if err != nil {
		panic(err)
	}

	mydi.Location = loc
	mydi.Log.StartLog()
	util.InitValidator()
}

// 初始化設定檔，讀YAML檔
func IniConfByEnv(env string, timezone string) {
	const confFileTpl = "conf/%s/config.yml"
	InitConfByFile(fmt.Sprintf(confFileTpl, env), timezone)
}

func (d *di) Close(key string) {
	if d.Mongodb != nil {
		d.Mongodb.Close(key)
	}
	// if d.Redis != nil {
	// 	d.Redis.Close(key)
	// }
	// if d.SearchV2 != nil {
	// 	d.SearchV2.Close(key)
	// }
}
