package mail

import (
	"fmt"
	"os"

	"kalimasi/util"

	sendgrid "github.com/sendgrid/sendgrid-go"
	sgMail "github.com/sendgrid/sendgrid-go/helpers/mail"
)

type MailServ interface {
	GetHtmlTemplateFile(filename string) string
	GetTextTemplateFile(filename string) string
	Subject(s string) MailServ
	PlaintText(p string) MailServ
	Html(h string) MailServ
	SendSingle(name, mail string) error
}

type SendGridConf struct {
	From *sgMail.Email      `yaml:"from"`
	Bcc  *sgMail.BccSetting `yaml:"bcc"`

	TemplatePath struct {
		HTML string `yaml:"html"`
		Text string `yaml:"text"`
	} `yaml:"templatePath"`

	s, p, h string
}

type SendGridServ struct {
	*SendGridConf
}

const sendgrid_key = "SENDGRID_API_KEY"

func (sgc *SendGridServ) GetHtmlTemplateFile(file string) string {
	return util.StrAppend(sgc.TemplatePath.HTML, file)
}

func (sgc *SendGridServ) GetTextTemplateFile(file string) string {
	return util.StrAppend(sgc.TemplatePath.Text, file)
}

func (sgc *SendGridServ) Subject(s string) MailServ {
	sgc.s = s
	return sgc
}

func (sgc *SendGridServ) PlaintText(p string) MailServ {
	sgc.p = p
	return sgc
}

func (sgc *SendGridServ) Html(h string) MailServ {
	sgc.h = h
	return sgc
}

func (sg *SendGridServ) SendSingle(name, mail string) error {
	to := sgMail.NewEmail(name, mail)
	message := sgMail.NewSingleEmail(sg.From, sg.s, to, sg.p, sg.h)
	if sg.Bcc != nil {
		message.SetMailSettings(sgMail.NewMailSettings().SetBCC(sg.Bcc))
	}
	client := sendgrid.NewSendClient(os.Getenv(sendgrid_key))
	response, err := client.Send(message)
	if err != nil {
		return err
	} else {
		fmt.Println("sendgrid mail")
		fmt.Println(response.StatusCode)
		fmt.Println(response.Body)
		fmt.Println(response.Headers)
	}
	return nil
}
