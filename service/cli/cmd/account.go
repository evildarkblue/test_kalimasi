package cmd

import (
	"fmt"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/model"
	"kalimasi/rsrc"

	"github.com/globalsign/mgo/bson"
	"github.com/google/uuid"
	"github.com/urfave/cli"
)

type AccountCmd bool

func (cmd AccountCmd) Enable() bool {
	return bool(cmd)
}

func (cmd AccountCmd) GetCommands() []cli.Command {
	return []cli.Command{
		{
			Name:    "fix-accout",
			Aliases: []string{"fa"},
			Usage:   "刪除多餘重覆會計帳",
			Action:  cmd.fixAccount,
		},
		{
			Name:    "incomeStatment-Detail",
			Aliases: []string{"incomeStatement"},
			Usage:   "損益表明細",
			Action:  cmd.incomeStatementDetail,
		},
	}
}

func (cc *AccountCmd) incomeStatementDetail(c *cli.Context) error {
	initDI(c)

	unitcode := c.Args().Get(0)
	cid, _ := doc.GetCompanyId(unitcode, false)
	startTime := c.Args().Get(1)
	endTime := c.Args().Get(2)
	code := c.Args().Get(3)

	di := rsrc.GetDI()
	uid := uuid.New()
	mdb := di.GetMongoByKey(uid.String())
	mongo := model.GetMgoDBModel(mdb)
	input := input.QueryReportByYearAndTimeRage{
		TimeRange: input.TimeRange{
			StartTimeStr: startTime,
			EndTimeStr:   endTime,
		},
	}

	if err := input.Validate(); err != nil {
		return err
	}

	a := &doc.AccountJoinReceipt{
		Account: doc.Account{
			CompanyID: cid,
		},
		Receipt: &doc.Receipt{
			CompanyID: cid,
		},
	}
	fmt.Println(input.GetMgoQuery())
	q := a.GetPipeline(input.GetMgoQuery())
	fmt.Println(q.GetPipeline())
	i, err := mongo.PipelineAll(a, q.GetPipeline())
	if err != nil {
		return err
	}
	fmt.Println("aaaa")
	total := 0
	if result, ok := i.([]*doc.AccountJoinReceipt); ok {
		for _, r := range result {
			if r.Code != code {
				continue
			}
			total += r.Amount
			fmt.Println(r.Code, r.Name, r.Typ, r.Amount, r.Receipt.DateTime, r.Receipt.VoucherNumber, r.Receipt.Number, r.Receipt.Amount)
		}
	}
	fmt.Println(total)
	return nil
}

func (cc *AccountCmd) fixAccount(c *cli.Context) error {
	initDI(c)
	di := rsrc.GetDI()
	unitcode := c.Args().Get(0)
	log := di.GetLog()

	company := &doc.Company{}
	key := uuid.New().String()
	db := di.GetMongoByKey(key)
	mgoDB := model.GetMgoDBModel(db)

	err := mgoDB.FindOne(company, bson.M{"unitcode": unitcode})
	if err != nil {
		log.Err(err.Error())
		return err
	}
	log.Info("start fix company: " + company.Name)
	acc := &doc.Account{CompanyID: company.ID}

	result, err := mgoDB.Find(acc, bson.M{}, 0, 0)
	if err != nil {
		log.Err(err.Error())
		return err
	}

	acclist, ok := result.([]*doc.Account)
	if !ok {
		log.Err("data type error")
	}
	var query bson.M
	receipt := &doc.Receipt{CompanyID: company.ID}
	rebate := &doc.Rebate{CompanyID: company.ID}
	trasfer := &doc.Transfer{CompanyID: company.ID}
	for _, a := range acclist {
		query = bson.M{
			"$or": []bson.M{
				{"debitaccterm": bson.M{"$elemMatch": bson.M{"id": a.ID}}},
				{"creditaccterm": bson.M{"$elemMatch": bson.M{"id": a.ID}}},
			},
		}

		result, _ := mgoDB.Find(receipt, query, 0, 0)
		if rarr, ok := result.([]*doc.Receipt); ok && len(rarr) > 0 {
			continue
		}
		result, _ = mgoDB.Find(rebate, query, 0, 0)
		if reArr, ok := result.([]*doc.Rebate); ok && len(reArr) > 0 {
			continue
		}
		result, _ = mgoDB.Find(trasfer, query, 0, 0)
		if tArr, ok := result.([]*doc.Transfer); ok && len(tArr) > 0 {
			continue
		}

		err = mgoDB.RemoveByID(a)
		if err != nil {
			log.Err(err.Error())
		}
		log.Info("not found : " + a.ID.Hex() + " and remove")
	}
	log.Info("fix end")
	return nil
}
