package cmd

import (
	"fmt"
	"kalimasi/doc"
	"kalimasi/model"
	"kalimasi/rsrc"

	"github.com/globalsign/mgo/bson"
	"github.com/google/uuid"
	"github.com/urfave/cli"
)

type ReceiptCmd bool

func (cmd ReceiptCmd) Enable() bool {
	return bool(cmd)
}

func (cmd ReceiptCmd) GetCommands() []cli.Command {
	return []cli.Command{
		{
			Name:    "fix-receipt-accout-type",
			Aliases: []string{"fras"},
			Usage:   "同步更新憑證會計帳分類",
			Action:  cmd.fixReceiptAccTyp,
		},
	}
}

type FixAccType struct {
	doc.Account
}

func (fac *FixAccType) GetUpdateField() bson.M {
	return bson.M{
		"typ": fac.Typ,
	}
}

func (cc *ReceiptCmd) fixReceiptAccTyp(c *cli.Context) error {
	initDI(c)
	di := rsrc.GetDI()
	unitcode := c.Args().Get(0)
	log := di.GetLog()

	company := &doc.Company{}
	key := uuid.New().String()
	db := di.GetMongoByKey(key)
	mgoDB := model.GetMgoDBModel(db)

	err := mgoDB.FindOne(company, bson.M{"unitcode": unitcode})
	if err != nil {
		log.Err(err.Error())
		return err
	}
	log.Info("find company: " + company.Name)

	receipt := &doc.Receipt{CompanyID: company.ID}

	result, err := mgoDB.Find(receipt, bson.M{}, 0, 0)
	if err != nil {
		log.Err(err.Error())
		return err
	}

	reclist, ok := result.([]*doc.Receipt)
	if !ok {
		log.Err("data type error")
	}

	account := &FixAccType{
		Account: doc.Account{CompanyID: company.ID},
	}
	for _, r := range reclist {
		log.Info(fmt.Sprintln("start update: ", r.Number))
		for _, a := range r.CreditAccTerm {
			account.ID = a.ID
			account.Typ = doc.TypeAccountCredit
			err = mgoDB.Update(account, nil)
			if err != nil {
				log.Err(err.Error())
			}
		}
		for _, a := range r.DebitAccTerm {
			account.ID = a.ID
			account.Typ = doc.TypeAccountDebit
			err = mgoDB.Update(account, nil)
			if err != nil {
				log.Err(err.Error())
			}
		}
		log.Info(fmt.Sprintln("end update: ", r.Number))
	}

	return nil
}
