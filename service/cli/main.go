package main

import (
	"log"
	"os"

	"kalimasi/service/cli/cmd"
)

func main() {
	app := cmd.GetCliApp(
		cmd.ReceiptCmd(true), cmd.AccountCmd(true),
	)
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
