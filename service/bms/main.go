package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	bmsV1 "kalimasi/api/bms/v1"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/middle"
	"kalimasi/model"
	"kalimasi/rsrc"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"github.com/rs/cors"
)

var (
	v = flag.Bool("v", false, "version")

	Version   = "1.0.0"
	BuildTime = "2000-01-01T00:00:00+0800"
)

func before() {
	// 要再移到cli中
	di := rsrc.GetDI()
	mdb := di.GetMongoByKey("main")
	um := model.GetUserModel(mdb)
	if !um.ServiceHasAcc(doc.UserServiceBSM) {
		bm := model.GetBmsModel(mdb)
		bm.Create(&input.CreateBmsManager{
			Account:  "it@numax.com.tw",
			Password: "123456",
			Name:     "it",
			Phone:    "02-25991258",
		}, nil)
	}
}

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
		log.Printf("Defaulting to port %s", port)
	}

	// [START setting_port]
	env := os.Getenv("ENV")
	if env == "" {
		env = "dev"
		log.Printf("Defaulting to ENV %s", env)
	}

	timezone := os.Getenv("TIMEZONE")
	if timezone == "" {
		timezone = "Asia/Taipei"
		log.Printf("Defaulting to timezone %s", timezone)
	}

	rsrc.IniConfByEnv(env, timezone)

	di := rsrc.GetDI()
	router := mux.NewRouter()
	authMiddle := middle.AuthMiddle("auth")

	di.APIConf.InitAPI(
		router,
		[]rsrc.Middle{
			middle.DebugMiddle("debug"),
			middle.DBMiddle("db"),
			authMiddle,
		},
		authMiddle,
		bmsV1.AuthAPI("authV1"),
	)

	di.GetLog().Info(fmt.Sprintf("start service formicidae with version %s and build time %s", Version, BuildTime))

	if di.APIConf.EnableCORS {
		c := cors.New(cors.Options{
			AllowedOrigins:   []string{"http://eaccount.numax.com.tw", "https://eaccount.numax.com.tw"},
			AllowCredentials: true,
			AllowedHeaders:   []string{"Fb-Token", "Auth-Token", "Content-Type"},
			AllowedMethods:   []string{"POST", "GET", "PUT", "DELETE"},
			// Enable Debugging for testing, consider disabling in production
			Debug: false,
		})
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), c.Handler(router)))
	} else {
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), router))
	}

}
