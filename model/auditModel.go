package model

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"kalimasi/input"
	"kalimasi/util"
	"net/http"
	"time"

	"kalimasi/doc"
	"kalimasi/rsrc"
	"kalimasi/rsrc/log"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type AuditData interface {
	GetAuditSummary() map[string]interface{}
	GetAuditType() string
	// 是否為資料修改審核
	HasAuditID() bool
	GetAuditUpdateDoc(companyID bson.ObjectId) *doc.AuditUpdateDoc
}

type AuditDoc interface {
	GetAuditID() bson.ObjectId
}

type auditModel struct {
	dbmodel *mgoDBModel

	log *log.Logger
}

func GetAuditModel(mgodb *mgo.Database) *auditModel {
	mongo := GetMgoDBModel(mgodb)
	return &auditModel{dbmodel: mongo, log: mongo.log}
}

// 系統管理員驗證
func SystemAudit(data AuditData, w http.ResponseWriter, req *http.Request) bool {
	di := rsrc.GetDI()
	dbclt := di.GetMongoByReq(req)
	qv := util.GetQueryValue(req, []string{"aid"}, true)
	ui := input.GetUserInfo(req)
	aid := qv["aid"].(string)
	am := GetAuditModel(dbclt)
	if bson.IsObjectIdHex(aid) {
		bid := bson.ObjectIdHex(aid)
		// 已審核過，不需再審核
		if am.IsSysPass(bid, data, ui) {
			return false
		}
	}

	const mailto = "stevendothebest@gmail.com"
	auditSetting := doc.AuditSetting{
		IsAudit: true,
		AuditProcess: []doc.AuditProcess{
			{
				Typ:   doc.AuditProcTypAcc,
				Value: mailto,
			},
		},
	}

	// 產生審核申請
	err := am.CreateSys(auditSetting, data, req)
	if err != nil {
		// 出錯，卡審核不建立資料
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return true
	}
	w.WriteHeader(http.StatusAccepted)

	mailServ := di.GetMailServ()
	const title = "[審核通知]有客人要建立公司資料囉"
	const content = "如題"
	err = mailServ.Subject(title).Html(content).SendSingle(mailto, mailto)
	if err != nil {
		di.GetLog().Err(err.Error())
	}
	// 需先審核
	return true
}

// 公司內部驗證
func CompanyAudit(data AuditData, w http.ResponseWriter, req *http.Request) bool {

	ui := input.GetUserInfo(req)
	c := &doc.Company{ID: ui.CompanyID}
	di := rsrc.GetDI()
	dbclt := di.GetMongoByReq(req)
	dbm := GetMgoDBModel(dbclt)
	err := dbm.FindByID(c)
	if err != nil {
		// 查不到公司資料，審核
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return true
	}

	if !c.IsAudit {
		// 公司不需要審核
		return false
	}

	qv := util.GetQueryValue(req, []string{"aid"}, true)
	aid := qv["aid"].(string)
	am := GetAuditModel(dbclt)
	if bson.IsObjectIdHex(aid) {
		bid := bson.ObjectIdHex(aid)
		// 已審核過，不需再審核
		if err = am.IsPass(bid, data, ui); err == nil {
			return false
		} else {
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte(err.Error()))
			return true
		}
	}
	// 產生審核申請
	aoid, err := am.Create(c.AuditSetting, data, req)
	if err != nil {
		// 出錯，卡審核不建立資料
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return true
	}

	if data.HasAuditID() {
		doc := data.GetAuditUpdateDoc(ui.CompanyID)
		doc.AuditID = aoid
		dbm.Update(doc, ui)
	}

	w.WriteHeader(http.StatusAccepted)
	// 需先審核
	return true
}

func (bm *auditModel) GetAuditDoc(audit AuditDoc, companyID bson.ObjectId) *doc.Audit {
	oid := audit.GetAuditID()
	if !oid.Valid() {
		return nil
	}
	a := &doc.Audit{ID: oid, CompanyID: companyID}
	err := bm.dbmodel.FindByID(a)
	if err != nil {
		bm.log.Debug(err.Error())
		return nil
	}
	return a
}

func (bm *auditModel) GetAuditState(audit AuditDoc, companyID bson.ObjectId) string {
	a := bm.GetAuditDoc(audit, companyID)
	if a == nil {
		return "none"
	}
	return a.State
}

func (bm *auditModel) GetSysAuditList(applicantId bson.ObjectId) []*doc.AuditSys {
	as := &doc.AuditSys{}
	q := bson.M{
		"applicant.id": applicantId,
		"typ":          "company-new",
		"state":        bson.M{"$ne": doc.AuditStatePass},
	}
	r, err := bm.dbmodel.Find(as, q, 0, 0)
	if err != nil {
		bm.log.Err(err.Error())
		return nil
	}
	if as, ok := r.([]*doc.AuditSys); ok {
		return as
	}
	return nil
}

func (bm *auditModel) CreateSys(as doc.AuditSetting, data AuditData, req *http.Request) error {
	s := util.GetFullUrlStr(req)
	cs, err := bm.getChecksum(data)
	if err != nil {
		return err
	}

	b, err := bm.dataEncode(data)
	if err != nil {
		return err
	}
	u := input.GetUserInfo(req)
	// 未登入註冊會員審核
	if u == nil {
		u = input.GetGuestInfo(req)
	}
	if !bson.IsObjectIdHex(u.ID) {
		return errors.New("invalid user id")
	}
	if len(as.AuditProcess) == 0 {
		return errors.New("invalid process")
	}

	cp := as.AuditProcess[0]
	var ass *doc.SimpleUser
	if cp.Typ == doc.AuditProcTypAcc {
		au := &doc.User{}
		err = bm.dbmodel.FindOne(au, bson.M{"email": cp.Value})
		if err != nil {
			return err
		}
		ass = &doc.SimpleUser{
			ID:   au.ID,
			Name: au.DisplayName,
		}
	}

	a := &doc.AuditSys{
		State:      doc.AuditStateNew,
		Typ:        data.GetAuditType(),
		Method:     req.Method,
		ActURL:     s,
		Summary:    data.GetAuditSummary(),
		SerialJSON: b,
		Checksum:   cs,
		Applicant: doc.SimpleUser{
			ID:   bson.ObjectIdHex(u.ID),
			Name: u.Name,
		},
		Assessor:    ass,
		Process:     as.AuditProcess,
		CurrProcess: 0,
	}
	err = bm.dbmodel.Save(a, u)
	if err != nil {
		return err
	}
	return nil
}

func (bm *auditModel) Create(as doc.AuditSetting, data AuditData, req *http.Request) (bson.ObjectId, error) {
	s := util.GetFullUrlStr(req)
	cs, err := bm.getChecksum(data)
	if err != nil {
		return "", err
	}

	b, err := bm.dataEncode(data)
	if err != nil {
		return "", err
	}
	u := input.GetUserInfo(req)
	if !bson.IsObjectIdHex(u.ID) {
		return "", errors.New("invalid user id")
	}

	if len(as.AuditProcess) == 0 {
		return "", errors.New("invalid process")
	}

	cp := as.AuditProcess[0]
	var ass *doc.SimpleUser
	if cp.Typ == doc.AuditProcTypAcc {
		if !bson.IsObjectIdHex(cp.Value) {
			return "", errors.New("invalid user id")
		}
		bid := bson.ObjectIdHex(cp.Value)
		au := &doc.User{ID: bid}
		err = bm.dbmodel.FindByID(au)
		if err != nil {
			return "", err
		}
		ass = &doc.SimpleUser{
			ID:   bid,
			Name: au.DisplayName,
		}
	}

	preAuditID := req.Header.Get("X-PreAudit-ID")
	preOid, _ := doc.GetObjectID(preAuditID)
	var preIDs []bson.ObjectId
	if preOid.Valid() {
		audit := &doc.Audit{
			ID:        preOid,
			CompanyID: u.CompanyID,
		}
		bm.dbmodel.FindByID(audit)
		preIDs = append(audit.PreAuditID, preOid)
	}

	a := &doc.Audit{
		CompanyID:  u.CompanyID,
		PreAuditID: preIDs,
		State:      doc.AuditStateNew,
		Typ:        data.GetAuditType(),
		Method:     req.Method,
		ActURL:     s,
		Summary:    data.GetAuditSummary(),
		SerialJSON: b,
		Checksum:   cs,
		Applicant: doc.SimpleUser{
			ID:   bson.ObjectIdHex(u.ID),
			Name: u.Name,
		},
		Assessor:    ass,
		Process:     as.AuditProcess,
		CurrProcess: 0,
	}
	err = bm.dbmodel.Save(a, u)
	if err != nil {
		return "", err
	}
	return a.ID, nil
}

func (bm *auditModel) IsSysPass(id bson.ObjectId, data interface{}, ui *input.ReqUser) bool {
	a := &doc.AuditSys{
		ID: id,
	}
	err := bm.dbmodel.FindByID(a)
	if err != nil {
		bm.log.Debug(err.Error())
		return false
	}
	cs, err := bm.getChecksum(data)
	if err != nil {
		bm.log.Debug(err.Error())
		return false
	}
	return (a.Checksum == cs && a.State == doc.AuditStatePass)
}

func (bm *auditModel) IsPass(id bson.ObjectId, data interface{}, ui *input.ReqUser) error {
	a := &doc.Audit{
		ID:        id,
		CompanyID: ui.CompanyID,
	}
	err := bm.dbmodel.FindByID(a)
	if err != nil {
		bm.log.Debug(err.Error())
		return errors.New("not found audit: " + id.Hex())
	}
	if a.State != doc.AuditStatePass {
		return errors.New("audit not pass")
	}
	cs, err := bm.getChecksum(data)
	if err != nil {
		bm.log.Debug(err.Error())
		return errors.New("check sum gen error")
	}
	if a.Checksum != cs {
		return errors.New("checksum check error")
	}
	return nil
}

// 審核
func (bm *auditModel) Apply(qid bson.ObjectId, ua *input.UpdateAudit, ui *input.ReqUser) error {
	a := &doc.Audit{ID: qid, CompanyID: ui.CompanyID}
	err := bm.dbmodel.FindByID(a)
	if err != nil {
		return rsrc.NewApiError(http.StatusNotFound, "not found: "+qid.Hex())
	}
	if a.CurrProcess >= uint16(len(a.Process)) {
		return rsrc.NewApiError(http.StatusForbidden, "process run out")
	}

	cp := a.Process[a.CurrProcess]
	if cp.Typ != doc.AuditProcTypPerm && cp.Typ != doc.AuditProcTypAcc {
		return rsrc.NewApiError(http.StatusInternalServerError, "process setting error")
	} else if cp.Typ == doc.AuditProcTypPerm && ui.Perm != cp.Value {
		return rsrc.NewApiError(http.StatusForbidden, "permission not allow")
	} else if cp.Typ == doc.AuditProcTypAcc && ui.ID != cp.Value {
		return rsrc.NewApiError(http.StatusForbidden, "permission not allow")
	}

	if ua.State == doc.AuditStateReject {
		a.State = ua.State
		a.CurrProcess++
	} else if ua.State == doc.AuditStatePass {
		a.CurrProcess++
		if a.CurrProcess == uint16(len(a.Process)) {
			a.State = ua.State
		} else {
			a.State = doc.AuditStateProcess
		}
	} else {
		return rsrc.NewApiError(http.StatusBadRequest, "invalid state")
	}
	a.Records = a.AddRecord(doc.SimpleUser{
		ID: bson.ObjectIdHex(ui.ID), Name: ui.Name,
	}, ua.State, ua.Message)

	if a.State != doc.AuditStatePass {
		return bm.dbmodel.Update(a, ui)
	}
	err = bm.dbmodel.Update(a, ui)
	if err != nil {
		return err
	}
	req, err := http.NewRequest(a.Method, fmt.Sprintf("%s?aid=%s", a.ActURL, a.ID.Hex()), bytes.NewBuffer(a.SerialJSON))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	user := &doc.User{ID: a.Applicant.ID}
	err = bm.dbmodel.FindByID(user)
	if err != nil {
		return err
	}
	t, err := rsrc.GetDI().GetJWTConf().GetToken(
		map[string]interface{}{
			"sub": user.ID.Hex(),
			"acc": user.Email,
			"nam": user.DisplayName,
			"per": "audit",
			"cat": ui.CompanyID.Hex(),
		})
	if err != nil {
		return err
	}
	req.Header.Set("Auth-Token", *t)
	timeout := time.Duration(2 * time.Second)
	client := http.Client{Timeout: timeout}
	r, err := client.Do(req)
	if err != nil {
		return err
	}
	if r.StatusCode == http.StatusOK {
		return nil
	}
	return errors.New("post error")
}

func (bm *auditModel) getChecksum(data interface{}) (string, error) {
	b, err := bm.dataEncode(data)
	if err != nil {
		return "", err
	}
	return util.MD5Byte(b), nil
}

func (bm *auditModel) dataEncode(data interface{}) ([]byte, error) {
	var b bytes.Buffer
	enc := json.NewEncoder(&b)
	// Encode (send) the value.
	err := enc.Encode(data)
	if err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}
