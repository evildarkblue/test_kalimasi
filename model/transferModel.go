package model

import (
	"errors"
	"fmt"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/rsrc/log"
	"kalimasi/util"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

type transferModel struct {
	dbmodel *mgoDBModel
	log     *log.Logger
}

func GetTransferModel(mgodb *mgo.Database) *transferModel {
	mongo := GetMgoDBModel(mgodb)
	return &transferModel{dbmodel: mongo, log: mongo.log}
}

func (bm *transferModel) Create(bi *input.CreateTransfer, u *input.ReqUser) error {
	bs := bi.GetTransferDoc()
	var ops []txn.Op

	bs.SetCompany(u.GetCompany())
	sm := NewSerialModel(bm.dbmodel.db)
	// serial will get table serialNumber's "voucherNumber"
	serial, err := sm.GetAndUpdateSerial(bs)
	if err != nil {
		return err
	}
	bs.VoucherNumber = util.GetVoucherNumber(bs.DateTime, serial)

	for _, d := range bi.DebitAccTerm {
		acc := d.GetAccount(bi.Date, doc.TypeAccountDebit, bi.BudgetId, u.CompanyID, bs.VoucherNumber, false)
		ops = append(ops, acc.GetSaveTxnOp(u))
		bs.DebitAccTerm = append(bs.DebitAccTerm, acc.GetMiniAccount())
	}

	for _, c := range bi.CreditAccTerm {
		acc := c.GetAccount(bi.Date, doc.TypeAccountCredit, bi.BudgetId, u.CompanyID, bs.VoucherNumber, false)
		ops = append(ops, acc.GetSaveTxnOp(u))
		bs.CreditAccTerm = append(bs.CreditAccTerm, acc.GetMiniAccount())
	}

	ops = append(ops, bs.GetSaveTxnOp(u))

	return bm.dbmodel.RunTxn(ops)
}

func (bm *transferModel) Modify(pi *input.PutTransfer, u *input.ReqUser) error {
	or := &doc.Transfer{ID: pi.ID, CompanyID: u.CompanyID}
	err := bm.dbmodel.FindByID(or)
	if err != nil {
		return err
	}
	var ops []txn.Op

	delAcc := &doc.Account{CompanyID: u.CompanyID}
	for _, d := range pi.DeleteAccterm {
		delAcc.ID = d
		ops = append(ops, delAcc.GetDelTxnOp())
	}

	or.DebitAccTerm = or.DebitAccTerm[:0]
	or.CreditAccTerm = or.CreditAccTerm[:0]

	for _, d := range pi.DebitAccTerm {
		acc := d.GetAccount(pi.Date, doc.TypeAccountDebit, pi.BudgetId, or.CompanyID, or.VoucherNumber, false)
		if acc.ID.Valid() {
			ops = append(ops, acc.GetUpdateTxnOp(
				bson.D{
					{Name: "code", Value: d.Code},
					{Name: "name", Value: d.Name},
					{Name: "amount", Value: d.Amount},
					{Name: "summary", Value: d.Desc},
					{Name: "datetime", Value: pi.Date},
					{Name: "year", Value: pi.Date.Year() - 1911},
					{Name: "budgetid", Value: pi.BudgetId},
					{Name: "typ", Value: doc.TypeAccountDebit},
				}))
		} else {
			ops = append(ops, acc.GetSaveTxnOp(u))
		}
		or.DebitAccTerm = append(or.DebitAccTerm, acc.GetMiniAccount())
	}

	for _, d := range pi.CreditAccTerm {
		acc := d.GetAccount(pi.Date, doc.TypeAccountCredit, pi.BudgetId, u.CompanyID, or.VoucherNumber, false)
		if acc.ID.Valid() {
			ops = append(ops, acc.GetUpdateTxnOp(
				bson.D{
					{Name: "code", Value: d.Code},
					{Name: "name", Value: d.Name},
					{Name: "amount", Value: d.Amount},
					{Name: "summary", Value: d.Desc},
					{Name: "datetime", Value: pi.Date},
					{Name: "year", Value: pi.Date.Year() - 1911},
					{Name: "budgetid", Value: pi.BudgetId},
					{Name: "typ", Value: doc.TypeAccountCredit},
				}))
		} else {
			ops = append(ops, acc.GetSaveTxnOp(u))
		}

		or.CreditAccTerm = append(or.CreditAccTerm, acc.GetMiniAccount())
	}

	ops = append(ops, or.GetUpdateTxnOp(
		bson.D{
			{Name: "datetime", Value: pi.Date},
			{Name: "year", Value: pi.Date.Year() - 1911},
			{Name: "summary", Value: pi.Summary},
			{Name: "budgetid", Value: pi.BudgetId},
			{Name: "debitaccterm", Value: or.DebitAccTerm},
			{Name: "creditaccterm", Value: or.CreditAccTerm},
		}))

	err = bm.dbmodel.RunTxn(ops)
	if err != nil {
		return err
	}
	return bm.dbmodel.addDocLog(or, u, doc.ActUpdate)
}

func (bm *transferModel) Delete(pi doc.DocInter, u *input.ReqUser) error {
	or := &doc.Transfer{ID: pi.GetID(), CompanyID: u.CompanyID}
	err := bm.dbmodel.FindByID(or)
	if err != nil {
		return err
	}
	var ops []txn.Op

	for _, a := range or.CreditAccTerm {
		ops = append(ops, a.GetDelTxnOp(u.CompanyID))
	}

	for _, a := range or.DebitAccTerm {
		ops = append(ops, a.GetDelTxnOp(u.CompanyID))
	}

	ops = append(ops, or.GetDelTxnOp())

	err = bm.dbmodel.RunTxn(ops)
	if err != nil {
		return err
	}
	return bm.dbmodel.addDocLog(or, u, doc.ActDelete)
}

func (bm *transferModel) ReplaceAccterm(inp *input.AccTermRepl, rep *doc.AccTerm, u *input.ReqUser) (failReceipt []bson.ObjectId, err error) {
	r := &doc.Transfer{CompanyID: inp.CompanyId}
	result, err := bm.dbmodel.Find(r, bson.M{"_id": bson.M{"$in": inp.Ids}}, 0, 0)
	if err != nil {
		return inp.Ids, err
	}
	reclist, ok := result.([]*doc.Transfer)
	if !ok {
		return inp.Ids, errors.New("data type error")
	}
	for _, r = range reclist {
		var ops []txn.Op
		l := len(r.DebitAccTerm)
		var a *doc.MiniAccount
		for i := 0; i < l; i++ {
			a = r.DebitAccTerm[i]
			if a.Code == inp.Target {
				r.DebitAccTerm[i].Code = rep.Code
				r.DebitAccTerm[i].Name = rep.Name
				ops = append(ops, a.GetUpdateTxnOp(
					bson.D{
						{Name: "code", Value: rep.Code},
						{Name: "name", Value: rep.Name},
					}, u.CompanyID))
			}
		}
		l = len(r.CreditAccTerm)
		for i := 0; i < l; i++ {
			a = r.CreditAccTerm[i]
			if a.Code == inp.Target {
				r.CreditAccTerm[i].Code = rep.Code
				r.CreditAccTerm[i].Name = rep.Name
				ops = append(ops, a.GetUpdateTxnOp(
					bson.D{
						{Name: "code", Value: rep.Code},
						{Name: "name", Value: rep.Name},
					}, u.CompanyID))
			}
		}
		ops = append(ops, r.GetUpdateTxnOp(
			bson.D{
				{Name: "debitaccterm", Value: r.DebitAccTerm},
				{Name: "creditaccterm", Value: r.CreditAccTerm},
			}))

		err = bm.dbmodel.RunTxn(ops)
		if err != nil {
			failReceipt = append(failReceipt, r.ID)
			bm.log.Err(fmt.Sprintf("replace receipt %s fail: %s", r.ID.Hex(), err.Error()))
		}
	}
	return failReceipt, err
}
