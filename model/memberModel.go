package model

import (
	"errors"
	"fmt"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/rsrc/log"
	"kalimasi/util"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

type memberModel struct {
	dbmodel *mgoDBModel
	atmodel *accTermModel
	log     *log.Logger
}

func GetMemberModel(mgodb *mgo.Database) *memberModel {
	mongo := GetMgoDBModel(mgodb)
	return &memberModel{
		dbmodel: mongo,
		atmodel: GetAccTermModel(mgodb),
		log:     mongo.log,
	}
}

func (bm *memberModel) IsMemberExists(companyid bson.ObjectId, idNumber string, mtype string) bool {
	m := &doc.Member{
		IDnumber:  idNumber,
		Typ:       mtype,
		CompanyID: companyid,
	}
	m.ID = m.GetID()
	err := bm.dbmodel.FindByID(m)
	return err == nil
}

func (bm *memberModel) Create(bi *input.CreateMember, u *input.ReqUser) error {
	// query company
	company := &doc.Company{ID: u.CompanyID}
	err := bm.dbmodel.FindByID(company)
	if err != nil {
		return err
	}

	// member save
	bs := bi.GetMemberDoc()
	bs.CompanyID = u.CompanyID
	var ops []txn.Op
	ops = append(ops, bs.GetSaveTxnOp(u))

	// query assets accTerm
	pay, ok := company.GetPayMethod(bi.Account.Method)
	if !ok {
		return errors.New(fmt.Sprintf("company pay method not set: %s, %s", u.CompanyID.Hex(), bi.Account.Method))
	}
	assetsAcc, err := bm.atmodel.FindAccTerm(u.CompanyID, doc.TypeAccTermAssets, pay.AccTermCode)
	if err != nil {
		return err
	}
	sm := NewSerialModel(bm.dbmodel.db)
	// query income accTerm
	if bi.Account.AnnualFee > 0 {
		am, ok := company.GetIncome("annualFee")
		if !ok {
			return errors.New(fmt.Sprintf("company income not set annualFee: %s", u.CompanyID.Hex()))
		}

		incomeAcc, err := bm.atmodel.FindAccTerm(
			u.CompanyID, doc.TypeAccTermIncome, am.AccTermCode)

		if err != nil {
			return err
		}

		debitAcc, creditAcc, pr := getPayRecordAndAccount(
			assetsAcc, incomeAcc, bi.Account.Date,
			am, pay, bi.Account.AnnualFee, bi.Account.Desc,
			u.CompanyID, bs.ID, sm)

		ops = append(ops, debitAcc.GetSaveTxnOp(u))
		ops = append(ops, creditAcc.GetSaveTxnOp(u))
		pr.DebitAccTerm = debitAcc.GetMiniAccount()
		pr.CreditAccTerm = creditAcc.GetMiniAccount()
		ops = append(ops, pr.GetSaveTxnOp(u))
	}

	if bi.Account.JoinFee > 0 {
		am, ok := company.GetIncome("joinFee")
		if !ok {
			return errors.New(fmt.Sprintf("company income not set joinFee: %s", u.CompanyID.Hex()))
		}

		incomeAcc, err := bm.atmodel.FindAccTerm(u.CompanyID, doc.TypeAccTermIncome, am.AccTermCode)
		if err != nil {
			return err
		}

		debitAcc, creditAcc, pr := getPayRecordAndAccount(
			assetsAcc, incomeAcc, bi.Account.Date,
			am, pay, bi.Account.JoinFee, bi.Account.Desc,
			u.CompanyID, bs.ID, sm)

		ops = append(ops, debitAcc.GetSaveTxnOp(u))
		ops = append(ops, creditAcc.GetSaveTxnOp(u))
		pr.DebitAccTerm = debitAcc.GetMiniAccount()
		pr.CreditAccTerm = creditAcc.GetMiniAccount()

		ops = append(ops, pr.GetSaveTxnOp(u))
	}

	return bm.dbmodel.RunTxn(ops)
}

func getPayRecordAndAccount(
	assetsAcc, incomeAcc *doc.AccTerm, date time.Time,
	feeMapping, methodMapping *doc.AccMapping, fee int, desc string,
	companyid bson.ObjectId, memberid bson.ObjectId, sm *serialModel) (debit *doc.Account, credit *doc.Account, pay *doc.PayRecord) {

	pay = &doc.PayRecord{
		Date:          date,
		Name:          feeMapping.Name,
		Display:       feeMapping.Display,
		Amount:        fee,
		Method:        methodMapping.Name,
		DisplayMethod: methodMapping.Display,
		Desc:          desc,
		CompanyID:     companyid,
		MemberId:      &memberid,
	}
	serial, err := sm.GetAndUpdateSerial(pay)
	if err != nil {
		panic(err)
	}
	pay.VoucherNumber = util.GetVoucherNumber(pay.Date, serial)
	inputDebit := input.InputAccTerm{
		Name:   assetsAcc.Name,
		Code:   assetsAcc.Code,
		Amount: fee,
		Desc:   desc,
	}
	debit = inputDebit.GetAccount(date, doc.TypeAccountDebit, nil, companyid, pay.VoucherNumber, false)

	inputCredit := input.InputAccTerm{
		Name:   incomeAcc.Name,
		Code:   incomeAcc.Code,
		Amount: fee,
		Desc:   desc,
	}
	credit = inputCredit.GetAccount(date, doc.TypeAccountCredit, nil, companyid, pay.VoucherNumber, false)

	return
}

func (bm *memberModel) Modify(pi *input.PutMember, u *input.ReqUser) error {
	bs := &doc.Member{ID: pi.ID, CompanyID: u.GetCompany()}
	err := bm.dbmodel.FindByID(bs)
	if err != nil {
		return err
	}

	um := pi.GetMemberDoc()

	um.ID = bs.ID
	um.CompanyID = u.GetCompany()
	err = bm.dbmodel.Update(um, u)
	if err != nil {
		return err
	}
	return bm.dbmodel.addDocLog(bs, u, doc.ActUpdate)
}

func (bm *memberModel) AddPayRecord(memberid bson.ObjectId, cp *input.CreatePayRecord, u *input.ReqUser) (*bson.ObjectId, error) {
	// query member
	bs := &doc.Member{ID: memberid, CompanyID: u.GetCompany()}
	err := bm.dbmodel.FindByID(bs)
	if err != nil {
		return nil, err
	}

	// query company
	company := &doc.Company{ID: u.CompanyID}
	err = bm.dbmodel.FindByID(company)
	if err != nil {
		return nil, err
	}

	pay, ok := company.GetPayMethod(cp.Method)
	if !ok {
		return nil, errors.New(fmt.Sprintf("company pay method not set: %s, %s", u.CompanyID.Hex(), cp.Method))
	}
	assetsAcc, err := bm.atmodel.FindAccTerm(u.CompanyID, doc.TypeAccTermAssets, pay.AccTermCode)
	if err != nil {
		return nil, err
	}

	am, ok := company.GetIncome(cp.PayType)
	if !ok {
		return nil, errors.New(fmt.Sprintf("company income not set %s: %s", cp.PayType, u.CompanyID.Hex()))
	}

	incomeAcc, err := bm.atmodel.FindAccTerm(u.CompanyID, doc.TypeAccTermIncome, am.AccTermCode)
	if err != nil {
		return nil, err
	}

	sm := NewSerialModel(bm.dbmodel.db)
	debitAcc, creditAcc, pr := getPayRecordAndAccount(
		assetsAcc, incomeAcc, cp.Date,
		am, pay, cp.Amount, cp.Desc,
		u.CompanyID, memberid, sm)

	var ops []txn.Op
	ops = append(ops, debitAcc.GetSaveTxnOp(u))
	ops = append(ops, creditAcc.GetSaveTxnOp(u))
	pr.DebitAccTerm = debitAcc.GetMiniAccount()
	pr.CreditAccTerm = creditAcc.GetMiniAccount()

	ops = append(ops, pr.GetSaveTxnOp(u))
	id := pr.GetID()
	return &id, bm.dbmodel.RunTxn(ops)
}

func (bm *memberModel) DeletePayment(pid bson.ObjectId, u *input.ReqUser) error {
	pr := &doc.PayRecord{ID: pid, CompanyID: u.CompanyID}
	err := bm.dbmodel.FindByID(pr)
	if err != nil {
		return err
	}
	var ops []txn.Op
	ops = append(ops, pr.CreditAccTerm.GetDelTxnOp(u.CompanyID))
	ops = append(ops, pr.DebitAccTerm.GetDelTxnOp(u.CompanyID))
	ops = append(ops, pr.GetDelTxnOp())
	err = bm.dbmodel.RunTxn(ops)
	if err != nil {
		return err
	}
	return bm.dbmodel.addDocLog(pr, u, doc.ActDelete)
}
