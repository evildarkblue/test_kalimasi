package model

import (
	"fmt"
	"net/url"
	"os"
	"strings"
	"testing"

	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/model/reportgener"
	"kalimasi/rsrc"
	"kalimasi/util"

	"github.com/globalsign/mgo/bson"
	"github.com/stretchr/testify/assert"
)

func getbalancebudget() *reportgener.BalanceBudget {
	sbb11 := &reportgener.SettingBalanceBudget{
		Category:    "1",
		SubCategory: nil,
		AccCode:     "4100",
		AccTerm:     "入會費",
		Name:        "入會費",
	}

	sbb12 := &reportgener.SettingBalanceBudget{
		Category:    "2",
		SubCategory: nil,
		AccCode:     "4101",
		AccTerm:     "常年會費",
		Name:        "常年會費",
	}

	sbb13 := &reportgener.SettingBalanceBudget{
		Category:    "3",
		SubCategory: nil,
		AccCode:     "4102",
		AccTerm:     "事業費",
		Name:        "事業費",
	}

	sbb14 := &reportgener.SettingBalanceBudget{
		Category:    "4",
		SubCategory: nil,
		AccCode:     "4103",
		AccTerm:     "會員捐款",
		Name:        "會員捐款",
	}

	sbb151 := &reportgener.SettingBalanceBudget{
		Category:    "1",
		SubCategory: nil,
		AccCode:     "410401",
		AccTerm:     "政府補助收入",
		Name:        "政府補助收入",
	}

	sbb152 := &reportgener.SettingBalanceBudget{
		Category:    "2",
		SubCategory: nil,
		AccCode:     "410402",
		AccTerm:     "其他補助收入",
		Name:        "其他補助收入",
	}

	sbb15 := &reportgener.SettingBalanceBudget{
		Category:    "5",
		SubCategory: []*reportgener.SettingBalanceBudget{sbb151, sbb152},
		AccCode:     "",
		AccTerm:     "",
		Name:        "補助收入",
	}

	sbb16 := &reportgener.SettingBalanceBudget{
		Category:    "6",
		SubCategory: nil,
		AccCode:     "4106",
		AccTerm:     "委託收益",
		Name:        "委託收益",
	}

	sbb17 := &reportgener.SettingBalanceBudget{
		Category:    "7",
		SubCategory: nil,
		AccCode:     "4107",
		AccTerm:     "會員服務收入",
		Name:        "會員服務收入",
	}

	sbb18 := &reportgener.SettingBalanceBudget{
		Category:    "8",
		SubCategory: nil,
		AccCode:     "4108",
		AccTerm:     "專案計畫收入",
		Name:        "專案計畫收入",
	}

	sbb19 := &reportgener.SettingBalanceBudget{
		Category:    "9",
		SubCategory: nil,
		AccCode:     "4109",
		AccTerm:     "利息收入",
		Name:        "利息收入",
	}

	sbb110 := &reportgener.SettingBalanceBudget{
		Category:    "10",
		SubCategory: nil,
		AccCode:     "4110",
		AccTerm:     "其他收入",
		Name:        "其他收入",
	}

	sbb1 := &reportgener.SettingBalanceBudget{
		Category:    "1",
		SubCategory: []*reportgener.SettingBalanceBudget{sbb11, sbb12, sbb13, sbb14, sbb15, sbb16, sbb17, sbb18, sbb19, sbb110},
		AccCode:     "",
		Name:        "本會收入",
	}

	sbb211 := &reportgener.SettingBalanceBudget{
		Category:    "1",
		SubCategory: nil,
		AccCode:     "600001",
		AccTerm:     "員工薪資",
		Name:        "員工薪資",
	}

	sbb212 := &reportgener.SettingBalanceBudget{
		Category:    "2",
		SubCategory: nil,
		AccCode:     "600002",
		AccTerm:     "勞保費(業主)",
		Name:        "勞保費(業主)",
	}

	sbb213 := &reportgener.SettingBalanceBudget{
		Category:    "3",
		SubCategory: nil,
		AccCode:     "600003",
		AccTerm:     "健保費(業主)",
		Name:        "健保費(業主)",
	}

	sbb214 := &reportgener.SettingBalanceBudget{
		Category:    "4",
		SubCategory: nil,
		AccCode:     "600004",
		AccTerm:     "團保費(業主)",
		Name:        "團保費(業主)",
	}

	sbb215 := &reportgener.SettingBalanceBudget{
		Category:    "5",
		SubCategory: nil,
		AccCode:     "600005",
		AccTerm:     "退休金(業主)",
		Name:        "退休金(業主)",
	}

	sbb216 := &reportgener.SettingBalanceBudget{
		Category:    "6",
		SubCategory: nil,
		AccCode:     "600006",
		AccTerm:     "兼職人員車馬費",
		Name:        "兼職人員車馬費",
	}

	sbb217 := &reportgener.SettingBalanceBudget{
		Category:    "7",
		SubCategory: nil,
		AccCode:     "600007",
		AccTerm:     "保險補助費",
		Name:        "保險補助費",
	}

	sbb218 := &reportgener.SettingBalanceBudget{
		Category:    "8",
		SubCategory: nil,
		AccCode:     "600008",
		AccTerm:     "年終成績考核獎金",
		Name:        "年終成績考核獎金",
	}

	sbb219 := &reportgener.SettingBalanceBudget{
		Category:    "9",
		SubCategory: nil,
		AccCode:     "600009",
		AccTerm:     "不休假獎金",
		Name:        "不休假獎金",
	}

	sbb2110 := &reportgener.SettingBalanceBudget{
		Category:    "10",
		SubCategory: nil,
		AccCode:     "600010",
		AccTerm:     "加班值班費",
		Name:        "加班值班費",
	}

	sbb2111 := &reportgener.SettingBalanceBudget{
		Category:    "11",
		SubCategory: nil,
		AccCode:     "600011",
		AccTerm:     "其他人事費",
		Name:        "其他人事費",
	}

	sbb21 := &reportgener.SettingBalanceBudget{
		Category: "1",
		SubCategory: []*reportgener.SettingBalanceBudget{
			sbb211, sbb212, sbb213, sbb214, sbb215, sbb216,
			sbb217, sbb218, sbb219, sbb2110, sbb2111},
		AccCode: "",
		Name:    "人事費",
	}

	sbb221 := &reportgener.SettingBalanceBudget{
		Category:    "1",
		SubCategory: nil,
		AccCode:     "600201",
		AccTerm:     "文具、書報、雜誌費",
		Name:        "文具、書報、雜誌費",
	}

	sbb222 := &reportgener.SettingBalanceBudget{
		Category:    "2",
		SubCategory: nil,
		AccCode:     "600202",
		AccTerm:     "印刷費",
		Name:        "印刷費",
	}

	sbb223 := &reportgener.SettingBalanceBudget{
		Category:    "3",
		SubCategory: nil,
		AccCode:     "600203",
		AccTerm:     "水電燃料費",
		Name:        "水電燃料費",
	}

	sbb224 := &reportgener.SettingBalanceBudget{
		Category:    "4",
		SubCategory: nil,
		AccCode:     "600204",
		AccTerm:     "旅運費",
		Name:        "旅運費",
	}

	sbb225 := &reportgener.SettingBalanceBudget{
		Category:    "5",
		SubCategory: nil,
		AccCode:     "600205",
		AccTerm:     "郵電費",
		Name:        "郵電費",
	}

	sbb226 := &reportgener.SettingBalanceBudget{
		Category:    "6",
		SubCategory: nil,
		AccCode:     "600206",
		AccTerm:     "大樓管理費",
		Name:        "大樓管理費",
	}

	sbb227 := &reportgener.SettingBalanceBudget{
		Category:    "7",
		SubCategory: nil,
		AccCode:     "600207",
		AccTerm:     "租賦費",
		Name:        "租賦費",
	}

	sbb228 := &reportgener.SettingBalanceBudget{
		Category:    "8",
		SubCategory: nil,
		AccCode:     "600208",
		AccTerm:     "修繕維護費",
		Name:        "修繕維護費",
	}

	sbb229 := &reportgener.SettingBalanceBudget{
		Category:    "9",
		SubCategory: nil,
		AccCode:     "600209",
		AccTerm:     "財產保險費",
		Name:        "財產保險費",
	}

	sbb2210 := &reportgener.SettingBalanceBudget{
		Category:    "10",
		SubCategory: nil,
		AccCode:     "600210",
		AccTerm:     "公共關係費",
		Name:        "公共關係費",
	}

	sbb2211 := &reportgener.SettingBalanceBudget{
		Category:    "11",
		SubCategory: nil,
		AccCode:     "600211",
		AccTerm:     "人事查核費",
		Name:        "人事查核費",
	}

	sbb2212 := &reportgener.SettingBalanceBudget{
		Category:    "12",
		SubCategory: nil,
		AccCode:     "600212",
		AccTerm:     "其他辦公費",
		Name:        "其他辦公費",
	}

	sbb22 := &reportgener.SettingBalanceBudget{
		Category: "2",
		SubCategory: []*reportgener.SettingBalanceBudget{
			sbb221, sbb222, sbb223, sbb224, sbb225, sbb226,
			sbb227, sbb228, sbb229, sbb2210, sbb2211, sbb2212},
		AccCode: "",
		Name:    "辦公費",
	}

	sbb231 := &reportgener.SettingBalanceBudget{
		Category:    "1",
		SubCategory: nil,
		AccCode:     "600301",
		AccTerm:     "會議費",
		Name:        "會議費",
	}

	sbb232 := &reportgener.SettingBalanceBudget{
		Category:    "2",
		SubCategory: nil,
		AccCode:     "600302",
		AccTerm:     "聯誼活動費",
		Name:        "聯誼活動費",
	}

	sbb233 := &reportgener.SettingBalanceBudget{
		Category:    "3",
		SubCategory: nil,
		AccCode:     "600303",
		AccTerm:     "業務推展費",
		Name:        "業務推展費",
	}

	sbb234 := &reportgener.SettingBalanceBudget{
		Category:    "4",
		SubCategory: nil,
		AccCode:     "600304",
		AccTerm:     "展覽費",
		Name:        "展覽費",
	}

	sbb235 := &reportgener.SettingBalanceBudget{
		Category:    "5",
		SubCategory: nil,
		AccCode:     "600305",
		AccTerm:     "考察觀摩費",
		Name:        "考察觀摩費",
	}

	sbb236 := &reportgener.SettingBalanceBudget{
		Category:    "6",
		SubCategory: nil,
		AccCode:     "600306",
		AccTerm:     "會刊（訊）編印費",
		Name:        "會刊（訊）編印費",
	}

	sbb237 := &reportgener.SettingBalanceBudget{
		Category:    "7",
		SubCategory: nil,
		AccCode:     "600307",
		AccTerm:     "調查統計費",
		Name:        "調查統計費",
	}

	sbb238 := &reportgener.SettingBalanceBudget{
		Category:    "8",
		SubCategory: nil,
		AccCode:     "600308",
		AccTerm:     "接受委託業務費",
		Name:        "接受委託業務費",
	}

	sbb239 := &reportgener.SettingBalanceBudget{
		Category:    "9",
		SubCategory: nil,
		AccCode:     "600309",
		AccTerm:     "內部作業組織業務費",
		Name:        "內部作業組織業務費",
	}

	sbb2310 := &reportgener.SettingBalanceBudget{
		Category:    "10",
		SubCategory: nil,
		AccCode:     "600310",
		AccTerm:     "研究發展費",
		Name:        "研究發展費",
	}

	sbb2311 := &reportgener.SettingBalanceBudget{
		Category:    "11",
		SubCategory: nil,
		AccCode:     "600311",
		AccTerm:     "社會服務費",
		Name:        "社會服務費",
	}

	sbb2312 := &reportgener.SettingBalanceBudget{
		Category:    "12",
		SubCategory: nil,
		AccCode:     "600312",
		AccTerm:     "其他業務費",
		Name:        "其他業務費",
	}

	sbb23 := &reportgener.SettingBalanceBudget{
		Category: "3",
		SubCategory: []*reportgener.SettingBalanceBudget{
			sbb231, sbb232, sbb233, sbb234, sbb235, sbb236,
			sbb237, sbb238, sbb239, sbb2310, sbb2311, sbb2312},
		AccCode: "",
		Name:    "業務費",
	}

	sbb24 := &reportgener.SettingBalanceBudget{
		Category:    "4",
		SubCategory: nil,
		AccCode:     "6004",
		Name:        "購置費",
	}

	sbb25 := &reportgener.SettingBalanceBudget{
		Category:    "5",
		SubCategory: nil,
		AccCode:     "6005",
		Name:        "折舊",
	}

	sbb26 := &reportgener.SettingBalanceBudget{
		Category:    "6",
		SubCategory: nil,
		AccCode:     "6006",
		Name:        "繳納上級團體會費",
	}

	sbb27 := &reportgener.SettingBalanceBudget{
		Category:    "7",
		SubCategory: nil,
		AccCode:     "6007",
		Name:        "繳納其他團體會費",
	}

	sbb28 := &reportgener.SettingBalanceBudget{
		Category:    "8",
		SubCategory: nil,
		AccCode:     "6008",
		Name:        "捐助費",
	}

	sbb29 := &reportgener.SettingBalanceBudget{
		Category:    "9",
		SubCategory: nil,
		AccCode:     "6009",
		Name:        "專案計畫支出",
	}

	sbb210 := &reportgener.SettingBalanceBudget{
		Category:    "10",
		SubCategory: nil,
		AccCode:     "6010",
		Name:        "雜項支出",
	}

	sbb0211 := &reportgener.SettingBalanceBudget{
		Category:    "11",
		SubCategory: nil,
		AccCode:     "6011",
		Name:        "利息費用",
	}

	sbb0212 := &reportgener.SettingBalanceBudget{
		Category:    "12",
		SubCategory: nil,
		AccCode:     "6012",
		Name:        "匯兌損失",
	}

	sbb0213 := &reportgener.SettingBalanceBudget{
		Category:    "13",
		SubCategory: nil,
		AccCode:     "6012",
		Name:        "預備金",
	}

	sbb0214 := &reportgener.SettingBalanceBudget{
		Category:    "13",
		SubCategory: nil,
		AccCode:     "6014",
		Name:        "提撥基金",
	}

	sbb2 := &reportgener.SettingBalanceBudget{
		Category: "2",
		SubCategory: []*reportgener.SettingBalanceBudget{
			sbb21, sbb22, sbb23, sbb24, sbb25, sbb26,
			sbb27, sbb28, sbb29, sbb210, sbb0211, sbb0212, sbb0213, sbb0214},
		AccCode: "",
		Name:    "本會支出",
	}

	bb := &reportgener.BalanceBudget{
		Title: "this is title",
	}
	bb.AddSetting(sbb1)
	bb.AddSetting(sbb2)
	return bb
}

func getfinalaccount() *reportgener.FinalAccount {
	sbb11 := &reportgener.SettingBalanceBudget{
		Category:    "1",
		SubCategory: nil,
		AccCode:     "1000",
		AccTerm:     "會員費",
		Name:        "會員費",
	}
	sbb12 := &reportgener.SettingBalanceBudget{
		Category:    "2",
		SubCategory: nil,
		AccCode:     "1000",
		AccTerm:     "常年會費",
		Name:        "常年會費",
	}

	sbb1 := &reportgener.SettingBalanceBudget{
		Category:    "1",
		SubCategory: []*reportgener.SettingBalanceBudget{sbb11, sbb12},
		AccCode:     "",
		Name:        "本會收入",
	}

	sbb211 := &reportgener.SettingBalanceBudget{
		Category:    "1",
		SubCategory: nil,
		AccCode:     "2001",
		AccTerm:     "員工給薪",
		Name:        "員工給薪",
	}

	sbb212 := &reportgener.SettingBalanceBudget{
		Category:    "2",
		SubCategory: nil,
		AccCode:     "2001",
		AccTerm:     "加班費",
		Name:        "加班費",
	}

	sbb21 := &reportgener.SettingBalanceBudget{
		Category:    "1",
		SubCategory: []*reportgener.SettingBalanceBudget{sbb211, sbb212},
		AccCode:     "",
		Name:        "人事支出",
	}

	sbb2 := &reportgener.SettingBalanceBudget{
		Category:    "2",
		SubCategory: []*reportgener.SettingBalanceBudget{sbb21},
		AccCode:     "",
		Name:        "本會支出",
	}

	bb := &reportgener.FinalAccount{}
	bb.Title = "this is title final"
	bb.AddSetting(sbb1)
	bb.AddSetting(sbb2)
	return bb
}

func Test_saveSetting(t *testing.T) {
	bb := getbalancebudget()
	rsrc.InitConfByFile("../conf/test/config.yml", "Asia/Taipei")
	di := rsrc.GetDI()
	mdbm := di.GetMongoByKey("testKey")
	rm := GetReportModel(mdbm)
	u := &input.ReqUser{
		Name:      "peter",
		Account:   "ch.focke@gmail.com",
		CompanyID: bson.ObjectIdHex("fffffffffffffffa00000000"),
	}
	err := rm.SaveSetting(bb, u)
	fmt.Println(err)
	assert.True(t, false)
}

func Test_balanceBudgetReport(t *testing.T) {
	bb := getbalancebudget()
	rsrc.InitConfByFile("../conf/test/config.yml", "Asia/Taipei")
	di := rsrc.GetDI()
	mdb := di.GetMongoByKey("testKey")
	mdbm := GetMgoDBModel(mdb)
	budget := &doc.BudgetStatement{
		ID:        bson.ObjectIdHex("5e84052b363c913afc4725c9"),
		CompanyID: bson.ObjectIdHex("ffffffffffffffff00000000"),
	}
	err := mdbm.FindByID(budget)
	fmt.Println(err, budget)
	lastbudget := &doc.BudgetStatement{
		ID:        bson.ObjectIdHex("5e842a29f470745057380184"),
		CompanyID: bson.ObjectIdHex("ffffffffffffffff00000000"),
	}

	err = mdbm.FindByID(lastbudget)
	fmt.Println(err, lastbudget)
	bb.SetCurrentBudget(budget)
	bb.SetLastBudget(lastbudget)
	bb.Year = budget.Year
	bb.CompanyID = budget.CompanyID

	os.Setenv("BUCKET", "kalimasi.appspot.com")
	rm := GetReportModel(mdb)
	url, err := rm.GeneratePDF(bb, nil)

	fmt.Println(url, err)
	assert.True(t, false)
}

func Test_groupAccount(t *testing.T) {
	a := &doc.GroupAccount{
		Account: doc.Account{
			CompanyID: bson.ObjectIdHex("ffffffffffffffff00000000"),
		},
	}
	q := a.GetFinalPipeline(bson.M{"year": -1910})
	rsrc.InitConfByFile("../conf/test/config.yml", "Asia/Taipei")
	di := rsrc.GetDI()
	mdb := di.GetMongoByKey("testKey")
	mdbm := GetMgoDBModel(mdb)
	i, err := mdbm.PipelineAll(a, q.GetPipeline())
	if result, ok := i.([]*doc.GroupAccount); ok {
		for _, r := range result {
			fmt.Println(r.Amount, r.Name, r.ID.Code, r.ID.Typ)
		}
	}
	fmt.Println("aaaa", i, err)
	assert.True(t, false)
}

func Test_finalAccountReport(t *testing.T) {
	bb := getfinalaccount()
	rsrc.InitConfByFile("../conf/test/config.yml", "Asia/Taipei")
	di := rsrc.GetDI()
	mdb := di.GetMongoByKey("testKey")
	mdbm := GetMgoDBModel(mdb)
	budget := &doc.BudgetStatement{
		ID:        bson.ObjectIdHex("5e84052b363c913afc4725c9"),
		CompanyID: bson.ObjectIdHex("ffffffffffffffff00000000"),
	}

	err := mdbm.FindByID(budget)
	if err != nil {
		fmt.Println(err)
	}
	budget.Year = -1910

	am := GetAccountModel(mdb)
	fa, err := am.GetFinalAccount(bson.ObjectIdHex("ffffffffffffffff00000000"), budget.Year)
	if err != nil {
		fmt.Println(err)
	}

	bb.SetCurrentBudget(fa)
	bb.SetLastBudget(budget)
	bb.Year = budget.Year
	bb.CompanyID = budget.CompanyID

	os.Setenv("BUCKET", "kalimasi.appspot.com")
	rm := GetReportModel(mdb)
	url, err := rm.GeneratePDF(bb, nil)

	fmt.Println(url, err)
	assert.True(t, false)
}
/*
func Test_balanceSheetReport(t *testing.T) {
	rsrc.InitConfByFile("../conf/test/config.yml", "Asia/Taipei")

	bb := &reportgener.BalanceSheet{}
	bb.Title = "test balance sheet"
	bb.Year = 109
	bb.CompanyID = bson.ObjectIdHex("ffffffffffffffff00000000")

	bb.AddAssetsSetting(&reportgener.SettingBalanceSheet{
		Name: "流動資產",
		Sub: []*reportgener.BalanceSheetItem{
			&reportgener.BalanceSheetItem{
				Name:    "庫存現金",
				AccCode: "1105",
			},
			&reportgener.BalanceSheetItem{
				Name:    "銀行存款",
				AccCode: "1110",
			},
			&reportgener.BalanceSheetItem{
				Name:    "有價證券",
				AccCode: "1111",
			},
			&reportgener.BalanceSheetItem{
				Name:    "應收票據",
				AccCode: "1113",
			},
			&reportgener.BalanceSheetItem{
				Name:    "應收帳款",
				AccCode: "1123",
			},
			&reportgener.BalanceSheetItem{
				Name:    "短期墊款",
				AccCode: "1114",
			},
			&reportgener.BalanceSheetItem{
				Name:    "其他應收款",
				AccCode: "1120",
			},
			&reportgener.BalanceSheetItem{
				Name:    "預付款項",
				AccCode: "1121",
			},
			&reportgener.BalanceSheetItem{
				Name:    "應收利息",
				AccCode: "1115",
			},
			&reportgener.BalanceSheetItem{
				Name:    "銀行存款－基金",
				AccCode: "1112",
			},
			&reportgener.BalanceSheetItem{
				Name:    "進項稅額",
				AccCode: "1116",
			},
			&reportgener.BalanceSheetItem{
				Name:    "留抵稅額",
				AccCode: "1117",
			},
			&reportgener.BalanceSheetItem{
				Name:    "應收稅額",
				AccCode: "1118",
			},
			&reportgener.BalanceSheetItem{
				Name:    "暫付款",
				AccCode: "1125",
			},
			&reportgener.BalanceSheetItem{
				Name:    "其他流動資產",
				AccCode: "1122",
			},
		},
	})
	bb.AddAssetsSetting(&reportgener.SettingBalanceSheet{
		Name: "固定資產",
		Sub: []*reportgener.BalanceSheetItem{
			&reportgener.BalanceSheetItem{
				Name:    "土地",
				AccCode: "1410",
			},
			&reportgener.BalanceSheetItem{
				SpecialSub: []*reportgener.BalanceSheetItem{
					&reportgener.BalanceSheetItem{
						Name:    "房屋及建築",
						AccCode: "1420",
					},
					&reportgener.BalanceSheetItem{
						Name:    "減累計折舊－房屋及建築",
						AccCode: "14201",
					},
				},
			},
			&reportgener.BalanceSheetItem{
				SpecialSub: []*reportgener.BalanceSheetItem{
					&reportgener.BalanceSheetItem{
						Name:    "事務器械設備",
						AccCode: "1430",
					},
					&reportgener.BalanceSheetItem{
						Name:    "減累計折舊－事務器械設備",
						AccCode: "14301",
					},
				},
			},
			&reportgener.BalanceSheetItem{
				SpecialSub: []*reportgener.BalanceSheetItem{
					&reportgener.BalanceSheetItem{
						Name:    "儀器設備",
						AccCode: "1450",
					},
					&reportgener.BalanceSheetItem{
						Name:    "減累計折舊－儀器設備",
						AccCode: "14501",
					},
				},
			},
			&reportgener.BalanceSheetItem{
				SpecialSub: []*reportgener.BalanceSheetItem{
					&reportgener.BalanceSheetItem{
						Name:    "交通運輸設備",
						AccCode: "1440",
					},
					&reportgener.BalanceSheetItem{
						Name:    "減累計折舊－交通運輸設備",
						AccCode: "14401",
					},
				},
			},
			&reportgener.BalanceSheetItem{
				SpecialSub: []*reportgener.BalanceSheetItem{
					&reportgener.BalanceSheetItem{
						Name:    "雜項設備",
						AccCode: "1460",
					},
					&reportgener.BalanceSheetItem{
						Name:    "減累計折舊－雜項設備",
						AccCode: "14601",
					},
				},
			},
			&reportgener.BalanceSheetItem{
				Name:    "其他固定資產",
				AccCode: "1470",
			},
			&reportgener.BalanceSheetItem{
				Name:    "存出保證金",
				AccCode: "1480",
			},
			&reportgener.BalanceSheetItem{
				Name:    "雜項資產",
				AccCode: "1490",
			},
		},
	})
	bb.AddLiabSetting(&reportgener.SettingBalanceSheet{
		Name: "流動負債",
		Sub: []*reportgener.BalanceSheetItem{
			&reportgener.BalanceSheetItem{
				Name:    "銀行透支",
				AccCode: "2114",
			},
			&reportgener.BalanceSheetItem{
				Name:    "短期借款",
				AccCode: "2100",
			},
			&reportgener.BalanceSheetItem{
				Name:    "應付票據",
				AccCode: "2113",
			},
			&reportgener.BalanceSheetItem{
				Name:    "銷項稅額",
				AccCode: "2101",
			},
			&reportgener.BalanceSheetItem{
				Name:    "應付薪資",
				AccCode: "21021",
			},
			&reportgener.BalanceSheetItem{
				Name:    "代扣員工勞保費",
				AccCode: "21022",
			},
			&reportgener.BalanceSheetItem{
				Name:    "代扣員工健保費",
				AccCode: "21023",
			},
			&reportgener.BalanceSheetItem{
				Name:    "代扣員工退休金",
				AccCode: "21024",
			},
			&reportgener.BalanceSheetItem{
				Name:    "代扣所得稅",
				AccCode: "21025",
			},
			&reportgener.BalanceSheetItem{
				Name:    "其他代收款項",
				AccCode: "2108",
			},
			&reportgener.BalanceSheetItem{
				Name:    "應付勞保費",
				AccCode: "2103",
			},
			&reportgener.BalanceSheetItem{
				Name:    "應付健保費",
				AccCode: "2104",
			},
			&reportgener.BalanceSheetItem{
				Name:    "應付退休金",
				AccCode: "2105",
			},
			&reportgener.BalanceSheetItem{
				Name:    "應付員工團保費",
				AccCode: "2106",
			},
			&reportgener.BalanceSheetItem{
				Name:    "預收款項",
				AccCode: "2112",
			},
			&reportgener.BalanceSheetItem{
				Name:    "其他應付款",
				AccCode: "2109",
			},
			&reportgener.BalanceSheetItem{
				Name:    "應付所得稅",
				AccCode: "2110",
			},
			&reportgener.BalanceSheetItem{
				Name:    "應付利息",
				AccCode: "2115",
			},
			&reportgener.BalanceSheetItem{
				Name:    "長期負債-一年內到期",
				AccCode: "2116",
			},
			&reportgener.BalanceSheetItem{
				Name:    "暫收款",
				AccCode: "2117",
			},
			&reportgener.BalanceSheetItem{
				Name:    "待撥補零用金",
				AccCode: "2111",
			},
			&reportgener.BalanceSheetItem{
				Name:    "應付營業稅",
				AccCode: "2118",
			},
		},
	})

	bb.AddLiabSetting(&reportgener.SettingBalanceSheet{
		Name: "長期負債",
		Sub: []*reportgener.BalanceSheetItem{
			&reportgener.BalanceSheetItem{
				Name:    "長期借款",
				AccCode: "2260",
			},
			&reportgener.BalanceSheetItem{
				Name:    "其他長期負債",
				AccCode: "2261",
			},
		},
	})

	bb.AddLiabSetting(&reportgener.SettingBalanceSheet{
		Name: "其他負債",
		Sub: []*reportgener.BalanceSheetItem{
			&reportgener.BalanceSheetItem{
				Name:    "存入保證金",
				AccCode: "2370",
			},
			&reportgener.BalanceSheetItem{
				Name:    "雜項負債",
				AccCode: "2380",
			},
		},
	})

	bb.AddLiabSetting(&reportgener.SettingBalanceSheet{
		Name: "基金",
		Sub: []*reportgener.BalanceSheetItem{
			&reportgener.BalanceSheetItem{
				Name:    "資產基金",
				AccCode: "3111",
			},
			&reportgener.BalanceSheetItem{
				Name:    "準備基金",
				AccCode: "3112",
			},
		},
	})
	bb.AddLiabSetting(&reportgener.SettingBalanceSheet{
		Name: "餘絀",
		Sub: []*reportgener.BalanceSheetItem{
			&reportgener.BalanceSheetItem{
				Name:    "本期餘絀",
				AccCode: "3200",
			},
			&reportgener.BalanceSheetItem{
				Name:    "累計餘絀",
				AccCode: "3233",
			},
		},
	})

	di := rsrc.GetDI()
	mdb := di.GetMongoByKey("testKey")
	am := GetAccountModel(mdb)
	fa, err := am.GetFinalAccount(bson.ObjectIdHex("ffffffffffffffff00000000"), -1910)
	if err != nil {
		fmt.Println(err)
	}
	bb.SetData(fa)

	rm := GetReportModel(mdb)
	u := &input.ReqUser{
		Name:      "peter",
		Account:   "ch.focke@gmail.com",
		CompanyID: bson.ObjectIdHex("ffffffffffffffff00000000"),
	}
	err = rm.SaveSetting(bb, u)
	// url, err := rm.GeneratePDF(bb, nil)
	//fmt.Println(url)
	assert.True(t, false)
}*/

func Test_fireStorage(t *testing.T) {
	rsrc.InitConfByFile("../conf/test/config.yml", "Asia/Taipei")
	di := rsrc.GetDI()
	s := di.GetFBStorage()
	f, err := os.Open("week_report.pdf")
	if err != nil {
		fmt.Println(err)
		assert.True(t, false)
		return
	}
	path := "receipt/123456789/0612/849303.pdf"
	u, err := s.SaveByReader(path, f)
	if err != nil {
		fmt.Println(err)
		assert.True(t, false)
		return
	}
	fmt.Println(u)

	up, err := url.Parse(u)
	if err != nil {
		fmt.Println(err)
		assert.True(t, false)
		return
	}

	sp := strings.Split(up.EscapedPath(), "/")
	fmt.Println(sp[len(sp)-1])
	delObj := strings.ReplaceAll(sp[len(sp)-1], "%2F", "/")
	err = s.Delete(delObj)

	sp = strings.Split(delObj, "/")
	l := len(sp)
	sp[l-1] = util.StrAppend("thumb_", sp[l-1])
	delObj = strings.Join(sp, "/")
	fmt.Println(delObj)
	fmt.Println(err)
	assert.True(t, false)
}
