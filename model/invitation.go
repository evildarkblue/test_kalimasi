package model

import (
	"bytes"
	"errors"
	"html/template"
	"kalimasi/doc"
	"kalimasi/rsrc"
	"kalimasi/rsrc/log"
	"kalimasi/rsrc/mail"
	"kalimasi/util"
	"net/http"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type InvitationReponseHandler interface {
	InvitationCheck(email string, parameter map[string]interface{}) error
	InvitationAccept(email string, parameter map[string]interface{}) error
	InvitationDeny(email string, parameter map[string]interface{}) error
}

var (
	inviatationTitleMap = map[string]string{
		doc.InvitationTypeCompanyUser: `邀請您一請加入雲端會計`,
		doc.InvitationTypeConsult: `邀請您一成為我們的會計顧問`,
	}
)

type invitationModel struct {
	mgodb    *mgo.Database
	mailServ mail.MailServ
	log      *log.Logger
}

func NewInvitationModel(mgodb *mgo.Database, mailServ mail.MailServ, log *log.Logger) *invitationModel {
	return &invitationModel{
		mgodb:    mgodb,
		mailServ: mailServ,
		log:      log,
	}
}

func (im *invitationModel) Response(id bson.ObjectId, response string, handler InvitationReponseHandler) error {
	inv := &doc.Invitation{
		ID: id,
	}
	mgoModel := mgoDBModel{
		db:  im.mgodb,
		log: im.log,
	}
	err := mgoModel.FindByID(inv)
	if err != nil {
		return rsrc.NewApiError(http.StatusBadRequest, "invitation not found")
	}
	switch response {
	case doc.InvitationReponseCheck:
		return handler.InvitationCheck(inv.Email, inv.Parameter)
	case doc.InvitationReponseAccept:
		err = handler.InvitationAccept(inv.Email, inv.Parameter)
	case doc.InvitationReponseDeny:
		err = handler.InvitationDeny(inv.Email, inv.Parameter)
	}
	if err != nil {
		return err
	}
	return mgoModel.RemoveByID(inv)
}

func (im *invitationModel) Send(id bson.ObjectId) error {
	inv := doc.Invitation{
		ID: id,
	}
	mgoModel := mgoDBModel{
		db:  im.mgodb,
		log: im.log,
	}
	err := mgoModel.FindByID(&inv)
	if err != nil {
		return err
	}
	title := inviatationTitleMap[inv.Typ]
	err = im.mailServ.Subject(title).Html(inv.Html).PlaintText(inv.Plaint).SendSingle("", inv.Email)
	if err != nil {
		return err
	}
	return nil
}

func GetTplString(filename string, data interface{}) (string, error) {
	t, err := template.ParseFiles(filename)
	if err != nil {
		return "", err
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, data); err != nil {
		return "", err
	}
	return tpl.String(), nil
}

func GetInviteToken(inviteID bson.ObjectId) (string, error) {
	return util.EncodeMap(map[string]interface{}{
		"invite": inviteID.Hex(),
	})
}


func GetInviteIDFromToken(token string) (bson.ObjectId, error) {
	result, err := util.DecodeMap(token)
	if err != nil {
		return "", err
	}
	var val interface{}
	var ok bool
	if val, ok = result["invite"]; !ok {
		return "", errors.New("token error")
	}
	if !bson.IsObjectIdHex(val.(string)) {
		return "", errors.New("invalite id")
	}
	return bson.ObjectIdHex(val.(string)), nil
}
