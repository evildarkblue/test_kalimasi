package reportgener

import (
	"errors"
	"kalimasi/doc"
	"kalimasi/report"
	"kalimasi/rsrc"
	"kalimasi/util"
	"strconv"
	"time"

	"github.com/globalsign/mgo/bson"
)

type FinalAccount struct {
	BalanceBudget
}

func (bbr *FinalAccount) GetFileName() string {
	return util.StrAppend(bbr.CompanyID.Hex(), "/", strconv.Itoa(bbr.Year), "-finalAccount-", strconv.FormatInt(time.Now().Unix(), 16))
}

func (bbr *FinalAccount) GetType() string {
	return "finalAccount"
}

func (bbr *FinalAccount) GetReportID() bson.ObjectId {
	if !bbr.CompanyID.Valid() {
		panic("must set companyID")
	}
	return doc.GetReportID(bbr.GetType(), bbr.CompanyID)
}

func (bbr *FinalAccount) GetReportObj(title string, font map[string]string) (report.ReportInter, error) {
	if bbr.currentBudget == nil {
		return nil, errors.New("current budget or last budget not set")
	}
	di := rsrc.GetDI()
	year := bbr.Year + 1911
	start := time.Date(year, time.Month(1), 1, 0, 0, 0, 0, di.Location)
	end := time.Date(year, time.Month(12), 31, 0, 0, 0, 0, di.Location)
	style := report.BalanceBudgetStyle("default")
	result := report.FinalAccount{
		BalanceBudget: report.BalanceBudget{
			Title:     title,
			StartDate: start,
			EndDate:   end,
			Style:     &style,
			Font:      font,
		},
	}

	if len(bbr.settings) != 2 {
		return nil, errors.New("report setting errors")
	}
	income, expenses := 0, 0
	i := 0
	typ := doc.TypeAccIncome
	for _, s := range bbr.settings {
		if i == 1 {
			typ = doc.TypeAccExpand
		} else {
			typ = doc.TypeAccIncome
		}
		bbi := s.getAllItem(bbr.currentBudget, bbr.lastBudget, typ)
		result.AddItems(bbi)
		if i == 0 {
			income = bbi[0].Amount
		} else {
			expenses = bbi[0].Amount
		}
		i++
	}
	result.AddItem(
		&report.BalanceBudgetItem{
			AccTerm:    "本期餘絀",
			Category:   "3",
			Amount:     income - expenses,
			LastAmount: nil,
		})

	return &result, nil
}
