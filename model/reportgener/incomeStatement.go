package reportgener

import (
	"kalimasi/doc"
	"kalimasi/report"
	"kalimasi/rsrc"
	"strings"
)

type IncomeStatement struct {
	TaxFileTrans
	Costs []*doc.AccTerm
}

func (bbr *IncomeStatement) IsSetting() bool {
	return bbr.Income != nil && bbr.Expenses != nil &&
		bbr.Costs != nil && bbr.DataAccess != nil
}

func (bbr *IncomeStatement) GetReportObj(title string, font map[string]string) (report.ReportInter, error) {
	di := rsrc.GetDI()

	style := report.TaxCalStyle(true)
	result := report.NewIncomeStatement(
		report.TaxCal{
			StartTime:   bbr.StartDate,
			EndTime:     bbr.EndDate,
			OrgName:     bbr.Company.Name,
			UnitCode:    bbr.Company.UnitCode,
			Year:        bbr.Year,
			Style:       &style,
			Font:        di.GetFontMap(),
			Incomes:     []*doc.TaxItem{},
			Expenditure: []*doc.TaxItem{},
		},
	)

	var bi *doc.BudgetItem
	for _, v := range bbr.Income {
		bi = bbr.DataAccess.GetBudgetItem(v.Code, doc.TypeAccIncome)
		if bi == nil || bi.Amount == 0 {
			continue
		}
		ti := &doc.TaxItem{
			Term:         v.Code + v.Name,
			Amount:       bi.Amount,
			AdjustAmount: bi.Amount,
		}
		if strings.HasPrefix(v.Code, "7") {
			result.AddNonIncomes(ti)
		} else {
			result.AddIncome(ti)
		}

	}

	for _, v := range bbr.Expenses {
		bi = bbr.DataAccess.GetBudgetItem(v.Code, doc.TypeAccExpand)
		if bi == nil || bi.Amount == 0 {
			continue
		}
		ti := &doc.TaxItem{
			Term:         v.Code + v.Name,
			Amount:       bi.Amount,
			AdjustAmount: bi.Amount,
		}
		if strings.HasPrefix(v.Code, "8") {
			result.AddNonExpenditure(ti)
		} else {
			result.AddExpenditure(ti)
		}

	}

	for _, v := range bbr.Costs {
		bi = bbr.DataAccess.GetBudgetItem(v.Code, doc.TypeAccIncome)
		if bi == nil || bi.Amount == 0 {
			continue
		}
		result.AddCost(&doc.TaxItem{
			Term:         v.Code + v.Name,
			Amount:       bi.Amount * -1,
			AdjustAmount: bi.Amount * -1,
		})
	}

	return result, nil
}
