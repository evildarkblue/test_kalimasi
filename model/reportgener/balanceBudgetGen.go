package reportgener

import (
	"encoding/json"
	"errors"
	"kalimasi/doc"
	"kalimasi/report"
	"kalimasi/rsrc"
	"kalimasi/util"
	"strconv"
	"time"

	"github.com/globalsign/mgo/bson"
)

type SettingBalanceBudget struct {
	Category    string
	Name        string
	SubCategory []*SettingBalanceBudget
	AccCode     string
	AccTerm     string
}

type balanceBudgetDataInter interface {
	GetBudgetItem(code, typ string) *doc.BudgetItem
	IsNil() bool
}

type BalanceBudget struct {
	Title         string
	CompanyID     bson.ObjectId
	Year          int
	currentBudget balanceBudgetDataInter
	lastBudget    balanceBudgetDataInter
	settings      []*SettingBalanceBudget
}

func (bbr *BalanceBudget) GetFileName() string {
	if !bbr.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(bbr.CompanyID.Hex(), "/", strconv.Itoa(bbr.Year), "-balanceBudget-", strconv.FormatInt(time.Now().Unix(), 16))
}

func (bbr *BalanceBudget) IsSetting() bool {
	return bbr.settings != nil
}

func (bbr *BalanceBudget) GetReportID() bson.ObjectId {
	if !bbr.CompanyID.Valid() {
		panic("must set companyID")
	}
	return doc.GetReportID(bbr.GetType(), bbr.CompanyID)
}

func (bbr *BalanceBudget) GetData() string {
	data, _ := json.Marshal(bbr.settings)
	return string(data)
}

func (bbr *BalanceBudget) GetTitle() string {
	return bbr.Title
}

func (bbr *BalanceBudget) GetType() string {
	return "balanceBudget"
}

func (bbr *BalanceBudget) Setting(setting string) error {
	var s []*SettingBalanceBudget
	err := json.Unmarshal([]byte(setting), &s)
	if err != nil {
		return err
	}
	bbr.settings = s
	return nil
}

func (bbr *BalanceBudget) AddSetting(setting *SettingBalanceBudget) {
	bbr.settings = append(bbr.settings, setting)
}

func (bbr *BalanceBudget) SetCurrentBudget(bs balanceBudgetDataInter) {
	bbr.currentBudget = bs
}

func (bbr *BalanceBudget) SetLastBudget(bs balanceBudgetDataInter) {
	bbr.lastBudget = bs
}

func (bbr *BalanceBudget) GetReportObj(title string, font map[string]string) (report.ReportInter, error) {
	if bbr.currentBudget == nil {
		return nil, errors.New("current budget not set")
	}
	di := rsrc.GetDI()
	year := bbr.Year + 1911
	start := time.Date(year, time.Month(1), 1, 0, 0, 0, 0, di.Location)
	end := time.Date(year, time.Month(12), 31, 0, 0, 0, 0, di.Location)
	style := report.BalanceBudgetStyle("default")
	result := report.BalanceBudget{
		Title:     title,
		StartDate: start,
		EndDate:   end,
		Style:     &style,
		Font:      font,
	}

	if len(bbr.settings) != 2 {
		return nil, errors.New("report setting errors")
	}
	income, expenses := 0, 0
	i := 0
	typ := doc.TypeAccIncome
	for _, s := range bbr.settings {
		if i == 1 {
			typ = doc.TypeAccExpand
		}
		bbi := s.getAllItem(bbr.currentBudget, bbr.lastBudget, typ)
		result.AddItems(bbi)
		if i == 0 {
			income = bbi[0].Amount
		} else {
			expenses = bbi[0].Amount
		}
		i++
	}
	result.AddItem(
		&report.BalanceBudgetItem{
			AccTerm:    "本期結餘",
			Category:   "3",
			Amount:     income - expenses,
			LastAmount: nil,
		})

	return &result, nil
}

func (sbb *SettingBalanceBudget) getAllItem(curr, last balanceBudgetDataInter, typ string) []*report.BalanceBudgetItem {
	if sbb.AccCode != "" {
		bbi, category := sbb.getItemByBudget(curr, last, typ)
		bbi.Category = category
		return []*report.BalanceBudgetItem{bbi}
	}

	currAmount, lastAmount := 0, 0
	var sitem []*report.BalanceBudgetItem
	for _, sc := range sbb.SubCategory {
		if sc.AccCode != "" {
			bbi, category := sc.getItemByBudget(curr, last, typ)
			bbi.SubCategory = category
			sitem = append(sitem, bbi)
			currAmount = currAmount + bbi.Amount
			if bbi.LastAmount != nil {
				lastAmount = lastAmount + *bbi.LastAmount
			}

			continue
		}
		subCurrAmount, subLastAmount := 0, 0
		var ssitem []*report.BalanceBudgetItem
		for _, scc := range sc.SubCategory {
			bbi, category := scc.getItemByBudget(curr, last, typ)
			subCurrAmount = subCurrAmount + bbi.Amount
			if bbi.LastAmount != nil {
				subLastAmount = subLastAmount + *bbi.LastAmount
			}

			bbi.Item = category
			ssitem = append(ssitem, bbi)
		}
		var bbi *report.BalanceBudgetItem
		if last != nil && !last.IsNil() {
			bbi = sc.getItem(subCurrAmount, &subLastAmount)
		} else {
			bbi = sc.getItem(subCurrAmount, nil)
		}

		bbi.SubCategory = sc.Category
		sitem = append(sitem, bbi)
		sitem = append(sitem, ssitem...)
		currAmount += subCurrAmount
		lastAmount += subLastAmount
	}

	var bbi *report.BalanceBudgetItem
	if last != nil && !last.IsNil() {
		bbi = sbb.getItem(currAmount, &lastAmount)
	} else {
		bbi = sbb.getItem(currAmount, nil)
	}

	bbi.Category = sbb.Category
	result := []*report.BalanceBudgetItem{bbi}
	return append(result, sitem...)
}

func (sbb *SettingBalanceBudget) getItem(amount int, lastAmount *int) *report.BalanceBudgetItem {
	bbi := &report.BalanceBudgetItem{
		AccTerm:    sbb.Name,
		Amount:     amount,
		LastAmount: lastAmount,
	}
	subtotal := 0
	if lastAmount != nil {
		subtotal = amount - *lastAmount
	} else {
		subtotal = amount
	}

	if subtotal < 0 {
		bbi.Decrease = -1 * subtotal
	} else {
		bbi.Increase = subtotal
	}
	return bbi
}

func (sbb *SettingBalanceBudget) getItemByBudget(curr, last balanceBudgetDataInter, typ string) (*report.BalanceBudgetItem, string) {
	cbi := curr.GetBudgetItem(sbb.AccCode, typ)
	var lbi *doc.BudgetItem
	if last != nil && !last.IsNil() {
		lbi = last.GetBudgetItem(sbb.AccCode, typ)
	}

	bbi := &report.BalanceBudgetItem{
		AccTerm: sbb.Name,
		Amount:  cbi.Amount,
		Desc:    cbi.Desc,
	}

	if lbi == nil {
		bbi.LastAmount = nil
	} else {
		bbi.LastAmount = &lbi.Amount
	}

	subtotal := 0
	if lbi == nil {
		subtotal = cbi.Amount
	} else {
		subtotal = cbi.Amount - lbi.Amount
	}

	if subtotal < 0 {
		bbi.Decrease = -1 * subtotal
	} else {
		bbi.Increase = subtotal
	}
	return bbi, sbb.Category
}

func (sbb *SettingBalanceBudget) SetEmpty() {
	for _, p := range sbb.SubCategory {
		if p.SubCategory == nil {
			p.SubCategory = []*SettingBalanceBudget{}
		} else {
			p.SetEmpty()
		}
	}
}

func (sbb *SettingBalanceBudget) Validate() (err error) {
	for _, p := range sbb.SubCategory {
		if p.Category == "" {
			err = errors.New("invalid category")
			return err
		}
		if p.SubCategory != nil {
			err = p.Validate()
		}
		if err != nil {
			return err
		}
	}
	return err
}
