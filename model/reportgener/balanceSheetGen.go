package reportgener

import (
	"encoding/json"
	"errors"
	"kalimasi/doc"
	"kalimasi/report"
	"kalimasi/rsrc"
	"kalimasi/util"
	"strconv"
	"time"

	"github.com/globalsign/mgo/bson"
)

type balanceSheetDataInter interface {
	GetBalaceSheetItem(code string) *doc.BudgetItem
}

type BalanceSheetItem struct {
	Name       string
	AccCode    string
	AccTerm    string
	SpecialSub []*BalanceSheetItem
}

func (bsi *BalanceSheetItem) toReportColumn(data balanceSheetDataInter) *report.BalanceSheetItem {
	if bsi.SpecialSub != nil && len(bsi.SpecialSub) == 2 {
		item1 := data.GetBalaceSheetItem(bsi.SpecialSub[0].AccCode)
		item2 := data.GetBalaceSheetItem(bsi.SpecialSub[1].AccCode)
		return &report.BalanceSheetItem{
			Name1:   bsi.SpecialSub[0].Name,
			Amount1: item1.Amount,
			Code1:   bsi.SpecialSub[0].AccCode,
			Desc1:   item1.Desc,
			Name2:   bsi.SpecialSub[1].Name,
			Amount2: item2.Amount,
			Code2:   bsi.SpecialSub[1].AccCode,
			Desc2:   item2.Desc,
		}
	}

	item1 := data.GetBalaceSheetItem(bsi.AccCode)
	return &report.BalanceSheetItem{
		Name1:   bsi.Name,
		Amount1: item1.Amount,
		Code1:   bsi.AccCode,
		Desc1:   item1.Desc,
	}
}

type SettingBalanceSheet struct {
	Name    string
	Sub     []*BalanceSheetItem
	AccCode string
	AccTerm string
}

func (sbs *SettingBalanceSheet) getAccount(bs balanceSheetDataInter) []*report.BalanceSheetItem {
	first := &report.BalanceSheetItem{
		Name1:   sbs.Name,
		Amount1: 0,
		IsHead1: true,
		Code1:   sbs.AccCode,
	}
	var result []*report.BalanceSheetItem
	result = append(result, first)
	if len(sbs.Sub) == 0 {
		return result
	}
	for _, s := range sbs.Sub {
		b := s.toReportColumn(bs)
		result = append(result, b)
		first.Amount1 += (b.Amount1 - b.Amount2)
	}
	return result
}

func (sbs *SettingBalanceSheet) getItem(bs balanceSheetDataInter) []*report.BalanceSheetItem {
	first := &report.BalanceSheetItem{
		Name1:   sbs.Name,
		Amount1: 0,
		IsHead1: true,
		Code1:   sbs.AccCode,
	}
	var result []*report.BalanceSheetItem
	result = append(result, first)
	if len(sbs.Sub) == 0 {
		return result
	}
	for _, s := range sbs.Sub {
		b := s.toReportColumn(bs)
		result = append(result, b)
		first.Amount1 += (b.Amount1 - b.Amount2)
	}
	return result
}

type BalanceSheet struct {
	Title     string        `json:"-"`
	CompanyID bson.ObjectId `json:"-"`
	Year      int           `json:"-"`
	data      balanceSheetDataInter
	Assets    []*SettingBalanceSheet
	Liab      []*SettingBalanceSheet

	FinalAccount *FinalAccount
}

func getBalanceSheetItemAry(sbsAry []*SettingBalanceSheet, bs balanceSheetDataInter) []*report.BalanceSheetItem {
	var r []*report.BalanceSheetItem
	for _, sbs := range sbsAry {
		parent := sbs.getItem(bs)
		r = append(r, parent...)
	}
	return r
}

func (bbr *BalanceSheet) getAssetItems(bs balanceSheetDataInter) []*report.BalanceSheetItem {
	return getBalanceSheetItemAry(bbr.Assets, bs)
}

func (bbr *BalanceSheet) getLiabItems(bs balanceSheetDataInter) []*report.BalanceSheetItem {
	result := getBalanceSheetItemAry(bbr.Liab, bs)
	for i := 0; i < len(result); i++ {
		result[i].Amount1 *= -1
		result[i].Amount2 *= -1
	}
	return result
}

func (bbr *BalanceSheet) GetReportID() bson.ObjectId {
	if !bbr.CompanyID.Valid() {
		panic("must set companyID")
	}
	return doc.GetReportID(bbr.GetType(), bbr.CompanyID)
}

func (bbr *BalanceSheet) GetFileName() string {
	if !bbr.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(bbr.CompanyID.Hex(), "/", strconv.Itoa(bbr.Year), "-balanceSheet-", strconv.FormatInt(time.Now().Unix(), 16))
}

func (bbr *BalanceSheet) IsSetting() bool {
	return bbr.Assets != nil && bbr.Liab != nil
}

func (bbr *BalanceSheet) GetData() string {
	data, _ := json.Marshal(bbr)
	return string(data)
}

func (bbr *BalanceSheet) GetTitle() string {
	return bbr.Title
}

func (bbr *BalanceSheet) GetType() string {
	return "balanceSheet"
}

func (bbr *BalanceSheet) Setting(setting string) error {
	err := json.Unmarshal([]byte(setting), bbr)
	if err != nil {
		return err
	}
	return nil
}

func (bbr *BalanceSheet) AddAssetsSetting(setting *SettingBalanceSheet) {
	bbr.Assets = append(bbr.Assets, setting)
}

func (bbr *BalanceSheet) AddLiabSetting(setting *SettingBalanceSheet) {
	bbr.Liab = append(bbr.Liab, setting)
}

func (bbr *BalanceSheet) SetData(bs balanceSheetDataInter) {
	bbr.data = bs
}

func (bbr *BalanceSheet) GetReportObj(title string, font map[string]string) (report.ReportInter, error) {
	if bbr.data == nil {
		return nil, errors.New("data not set")
	}
	di := rsrc.GetDI()
	year := bbr.Year + 1911
	end := time.Date(year, time.Month(12), 31, 0, 0, 0, 0, di.Location)
	style := report.BalanceBudgetStyle("default")
	result := report.BalanceSheet{
		Title:    title,
		DateTime: end,
		Style:    &style,
		Font:     font,
	}
	assetItems := bbr.getAssetItems(bbr.data)
	liabItems := bbr.getLiabItems(bbr.data)
	balance := bbr.getBalance()
	assetTotal, liabTotal := 0, 0
	for _, li := range liabItems {
		if li.Name1 == "餘絀" {
			li.Amount1 += balance
		}
		if li.Name1 == "本期餘絀" {
			li.Amount1 = balance
		}
	}
	var rows []*report.BalanceSheetRow
	liabIndex, maxLiabIndex := 0, len(liabItems)-1
	for _, ai := range assetItems {
		if ai.Len() == 0 {
			continue
		}
		bsr := &report.BalanceSheetRow{}
		bsr.Asset = ai
		if ai.Len() == 1 && liabIndex <= maxLiabIndex {
			bsr.Liab = liabItems[liabIndex]
			liabIndex++
		} else if ai.Len() == 2 && liabIndex+1 <= maxLiabIndex {
			liabItems[liabIndex].Name2 = liabItems[liabIndex+1].Name1
			liabItems[liabIndex].Amount2 = liabItems[liabIndex+1].Amount1
			liabItems[liabIndex].IsHead2 = liabItems[liabIndex+1].IsHead1
			bsr.Liab = liabItems[liabIndex]
			liabIndex += 2
		} else if ai.Len() == 2 && liabIndex == maxLiabIndex {
			bsr.Liab = liabItems[liabIndex]
			liabIndex++
		} else {
			bsr.Liab = &report.BalanceSheetItem{}
		}
		if bsr.Asset.IsHead1 {
			assetTotal += bsr.Asset.Amount1
		}
		if bsr.Liab.IsHead1 {
			liabTotal += bsr.Liab.Amount1
		}
		rows = append(rows, bsr)
	}
	for liabIndex <= maxLiabIndex {
		bsr := &report.BalanceSheetRow{}
		bsr.Asset = &report.BalanceSheetItem{}
		bsr.Liab = liabItems[liabIndex]
		if bsr.Liab.IsHead1 {
			liabTotal += bsr.Liab.Amount1
		}
		rows = append(rows, bsr)
		liabIndex++
	}
	bsr := &report.BalanceSheetRow{}
	bsr.Asset = &report.BalanceSheetItem{
		Name1:   "合計",
		Amount1: assetTotal,
		IsHead1: true,
	}
	bsr.Liab = &report.BalanceSheetItem{
		Name1:   "合計",
		Amount1: liabTotal,
		IsHead1: true,
	}
	rows = append(rows, bsr)
	result.Item = rows
	return &result, nil
}

func (bbr *BalanceSheet) SetEmpty() {
	if bbr.Assets != nil {
		for _, p := range bbr.Assets {
			if p.Sub == nil {
				p.Sub = []*BalanceSheetItem{}
			} else {
				for _, p2 := range p.Sub {
					if p2.SpecialSub == nil {
						p2.SpecialSub = []*BalanceSheetItem{}
					} else {
						p2.setEmpty()
					}
				}

			}
		}
	} else {
		bbr.Assets = []*SettingBalanceSheet{}
	}

	if bbr.Liab != nil {
		for _, p := range bbr.Liab {
			if p.Sub == nil {
				p.Sub = []*BalanceSheetItem{}
			} else {
				for _, p2 := range p.Sub {

					if p2.SpecialSub == nil {
						p2.SpecialSub = []*BalanceSheetItem{}

					} else {
						p2.setEmpty()
					}
				}

			}
		}
	} else {
		bbr.Liab = []*SettingBalanceSheet{}
	}
}

func (bbr *BalanceSheet) getBalance() int {
	if bbr.FinalAccount == nil {
		return 0
	}
	income, expenses := 0, 0
	i := 0
	typ := doc.TypeAccIncome
	for _, s := range bbr.FinalAccount.settings {
		if i == 1 {
			typ = doc.TypeAccExpand
		} else {
			typ = doc.TypeAccIncome
		}
		bbi := s.getAllItem(bbr.FinalAccount.currentBudget, bbr.FinalAccount.lastBudget, typ)
		if i == 0 {
			income = bbi[0].Amount
		} else {
			expenses = bbi[0].Amount
		}
		i++
	}
	return income - expenses
}

func (bbr *BalanceSheetItem) setEmpty() {

	for _, p := range bbr.SpecialSub {
		if p.SpecialSub == nil {
			p.SpecialSub = []*BalanceSheetItem{}
		} else {
			p.setEmpty()
		}
	}
}

func (bbr *BalanceSheetItem) Validate() (err error) {
	for _, p := range bbr.SpecialSub {

		if p.Name == "" {
			err = errors.New("invalid name")

			return err
		}
		if p.SpecialSub != nil {
			err = p.Validate()
		}
		if err != nil {
			return err
		}
	}
	return err
}
