package reportgener

import (
	"kalimasi/report"

	"github.com/globalsign/mgo/bson"
)

type ReportGenInter interface {
	IsSetting() bool
	Setting(setting string) error
	GetReportID() bson.ObjectId
	GetData() string
	GetType() string
	GetTitle() string
	GetFileName() string
	GetReportObj(title string, font map[string]string) (report.ReportInter, error)
}
