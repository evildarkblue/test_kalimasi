package reportgener

import (
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/report"
	"kalimasi/rsrc/log"
	"kalimasi/util"
	"math"
	"strconv"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type GeneralLedgerGenInter interface {
	//ReportGenInter
	Source() report.GLSource
	GetReport() report.ReportInter
	GetFileName() string
}

func NewGeneralLedgerGen(db *mgo.Database, q input.QueryReportByYearAndTimeRage, companyID bson.ObjectId, list []*doc.AccTermList, font map[string]string, log *log.Logger) GeneralLedgerGenInter {
	return &generalLedgerGenImpl{
		companyID:   companyID,
		query:       q,
		pageCount:   30,
		db:          db,
		log:         log,
		accTermList: list,
		font:        font,
	}
}

type generalLedgerGenImpl struct {
	companyID bson.ObjectId
	query     input.QueryReportByYearAndTimeRage
	pageCount int

	db   *mgo.Database
	log  *log.Logger
	font map[string]string

	accTermList []*doc.AccTermList
}

func (glg *generalLedgerGenImpl) GetFileName() string {
	if !glg.companyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(glg.companyID.Hex(), "/", strconv.Itoa(glg.query.GetYear()), "-generalLedger-", strconv.FormatInt(time.Now().Unix(), 16))
}

func (glg *generalLedgerGenImpl) GetReport() report.ReportInter {
	c := doc.Company{}
	err := glg.db.C(c.GetC()).FindId(glg.companyID).One(&c)
	if err != nil {
		glg.log.Err(err.Error())
		return nil
	}
	codes := glg.GetAccountCodes()
	totalPage := 0
	for _, c := range codes {
		totalPage += glg.GetSubTotalPage(c)
	}

	return &report.GeneralLedger{
		Name:       c.Name,
		Owner:      c.OwnerName,
		Year:       glg.query.GetYear(),
		No:         1,
		Page:       totalPage,
		StartDate:  glg.query.GetStartTime(),
		EndDate:    glg.query.GetEndTime(),
		DataSource: glg.Source(),
		Style:      report.GLStyle("style"),
		Font:       glg.font,
	}
}

func (glg *generalLedgerGenImpl) Source() report.GLSource {
	return glg
}

func (glg *generalLedgerGenImpl) GetAccountCodes() []string {
	if glg.accTermList == nil {
		return nil
	}

	var result []string
	for _, g := range glg.accTermList {
		for _, a := range g.Terms {
			result = append(result, a.Code)
			if len(a.Sub) == 0 {
				continue
			}
			for _, s := range a.Sub {
				result = append(result, s.Code)
			}
		}
	}
	return result
}
func (glg *generalLedgerGenImpl) GetAccountName(code string) string {
	if glg.accTermList == nil {
		return "Not Found"
	}
	var at *doc.AccTerm
	for _, g := range glg.accTermList {
		if at = g.GetAccTerm(code); at != nil {
			return at.Name
		}
	}
	return code + " Not Found"
}
func (glg *generalLedgerGenImpl) PageData(code string, page int) []*doc.Account {
	account := &doc.Account{
		CompanyID: glg.companyID,
	}
	limit := glg.pageCount
	skip := (page - 1) * limit
	result := []*doc.Account{}
	q := glg.query.GetMgoQuery()
	q["code"] = code
	q["$or"] = []bson.M{
		{"isopening": false},
		{"isopening": bson.M{"$exists": false}},
	}
	err := glg.db.C(account.GetC()).Find(q).Sort("datetime").Limit(limit).Skip(skip).All(&result)
	if err != nil {
		glg.log.Err(err.Error())
		return nil
	}
	if len(result) == 0 {
		return nil
	}
	return result
}

func (glg *generalLedgerGenImpl) GetSubTotalPage(code string) int {
	account := &doc.Account{
		CompanyID: glg.companyID,
	}
	q := glg.query.GetMgoQuery()
	q["code"] = code
	q["$or"] = []bson.M{
		bson.M{"isopening": false},
		bson.M{"isopening": bson.M{"$exists": false}},
	}
	n, err := glg.db.C(account.GetC()).Find(q).Count()
	if err != nil {
		return 0
	}
	return int(math.Ceil(float64(n) / float64(glg.pageCount)))
}
func (glg *generalLedgerGenImpl) TotalDebit(code string) int {
	account := &doc.Account{
		CompanyID: glg.companyID,
	}
	result := []*doc.Account{}
	q := glg.query.GetMgoQuery()
	q["code"] = code
	q["typ"] = doc.TypeAccountDebit
	q["$or"] = []bson.M{
		bson.M{"isopening": false},
		bson.M{"isopening": bson.M{"$exists": false}},
	}
	err := glg.db.C(account.GetC()).Find(q).All(&result)
	if err != nil {
		return 0
	}
	total := 0
	for _, a := range result {
		total = total + a.Amount
	}
	return total
}
func (glg *generalLedgerGenImpl) TotalCredit(code string) int {
	account := &doc.Account{
		CompanyID: glg.companyID,
	}
	result := []*doc.Account{}
	q := glg.query.GetMgoQuery()
	q["code"] = code
	q["typ"] = doc.TypeAccountCredit
	q["$or"] = []bson.M{
		bson.M{"isopening": false},
		bson.M{"isopening": bson.M{"$exists": false}},
	}
	err := glg.db.C(account.GetC()).Find(q).All(&result)
	if err != nil {
		return 0
	}
	total := 0
	for _, a := range result {
		total = total + a.Amount
	}
	return total
}

func (glg *generalLedgerGenImpl) OldBalance(code string) int {
	account := &doc.Account{
		CompanyID: glg.companyID,
	}
	result := []*doc.Account{}
	q := glg.query.GetMgoQuery()
	q["code"] = code
	q["isopening"] = true
	err := glg.db.C(account.GetC()).Find(q).All(&result)
	if err != nil {
		return 0
	}
	total := 0
	for _, a := range result {
		if a.Typ == doc.TypeAccountCredit {
			total = total - a.Amount
		} else if a.Typ == doc.TypeAccountDebit {
			total = total + a.Amount
		}

	}
	return total
}
