package model

import (
	"errors"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/rsrc/log"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type bmsManagerModel struct {
	dbmodel *mgoDBModel
	log     *log.Logger
}

func GetBmsModel(mgodb *mgo.Database) *bmsManagerModel {
	mongo := GetMgoDBModel(mgodb)
	return &bmsManagerModel{
		dbmodel: mongo,
		log:     mongo.log,
	}
}

func (bmsmm *bmsManagerModel) Create(input *input.CreateBmsManager, u *input.ReqUser) error {
	bms := &doc.User{}
	bs := bson.M{"email": input.Account}
	_ = bmsmm.dbmodel.FindOne(bms, bs)

	if bms.ID.Valid() {
		return errors.New("Account or Phone Number exist")
	}
	return bmsmm.dbmodel.Save(input.ToDoc(), u)
}

func (bmsmm *bmsManagerModel) Get(key string) ([]*doc.User, error) {
	bs := bson.M{}
	if key != "" {
		bs["$or"] = []bson.M{{"displayname": key}, {"email": key}, {"phonenumber": key}}
	}

	bms := &doc.User{}

	inter, err := bmsmm.dbmodel.Find(bms, bs, 0, 0)
	if err != nil {
		return nil, err
	}
	result := inter.([]*doc.User)
	return result, nil
}
