package model

import (
	"fmt"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/rsrc"
	"testing"

	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
	"github.com/stretchr/testify/assert"
)

type M map[string]interface{}

func Test_Choan(t *testing.T) {
	rsrc.InitConfByFile("../conf/dev/config.yml", "Asia/Taipei")
	// txn.SetChaos(txn.Chaos{
	// 	KillChance: 1,
	// 	Breakpoint: "set-applying",
	// })

	opses := []txn.Op{{
		C:      "accounts",
		Id:     0,
		Insert: M{"balance": 100},
	}, {
		C:      "accounts",
		Id:     0,
		Remove: true,
	}, {
		C:      "accounts",
		Id:     0,
		Insert: M{"balance": 200},
	}, {
		C:      "accounts",
		Id:     0,
		Update: M{"$inc": M{"balance": 100}},
	}}

	di := rsrc.GetDI()
	//txn.SetChaos(txn.Chaos{KillChance: 1.0})
	c := di.GetMongoByKey("testKey").C(TxnC)
	r := txn.NewRunner(c)

	var last bson.ObjectId
	//for _, ops := range opses {
	last = bson.NewObjectId()
	err := r.Run(opses, last, nil)
	fmt.Println("aaa", err)
	//}

	err = r.Resume(last)
	fmt.Println("bbb", err)
	assert.True(t, false)
}

func Test_MongoSave(t *testing.T) {
	rsrc.InitConfByFile("../conf/dev/config.yml", "Asia/Taipei")
	r := &doc.Company{
		ID:       bson.ObjectIdHex("5e84052b363c913afc4725c9"),
		Typ:      doc.CompanyTypEnterprise,
		Name:     "中華社團領袖聯合總會",
		UnitCode: "82001029",
	}
	u := &input.ReqUser{
		Name:    "peter",
		Account: "ch.focke@gmail.com",
	}
	di := rsrc.GetDI()
	mdbm := GetMgoDBModel(di.GetMongoByKey("testKey"))
	err := mdbm.Save(r, u)
	fmt.Println("Test_MongoSave:", err)
	assert.True(t, true)

}

func Test_MongoFind(t *testing.T) {
	rsrc.InitConfByFile("../conf/dev/config.yml", "Asia/Taipei")
	var bID = "5e84052b363c913afc4725c9"
	r := &doc.Company{
		ID: bson.ObjectIdHex(bID),
	}
	di := rsrc.GetDI()
	mdbm := GetMgoDBModel(di.GetMongoByKey("testKey"))
	err := mdbm.FindByID(r)
	fmt.Println("Test_MongoFind:", err, r)

	assert := assert.New(t)
	//assert.NotEqual(r, nil, "Not Found bsonID like"+bID)
	assert.Equal(r, nil, "Not Found bsonID like"+bID)

}
