package model

import (
	"errors"
	"fmt"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/rsrc"
	"kalimasi/rsrc/log"
	"kalimasi/rsrc/storage"
	"kalimasi/util"

	"math"
	"net/http"
	"net/url"
	"strings"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

type receiptModel struct {
	dbmodel *mgoDBModel
	storage storage.Storage
	log     *log.Logger
}

func GetReceiptModel(mgodb *mgo.Database, storage storage.Storage) *receiptModel {
	mongo := GetMgoDBModel(mgodb)
	return &receiptModel{dbmodel: mongo, storage: storage, log: mongo.log}
}

func (bm *receiptModel) Create(bi *input.CreateReceipt, u *input.ReqUser) (bson.ObjectId, error) {
	bs := bi.GetReceiptDoc()
	var ops []txn.Op

	c := &doc.Company{
		ID: u.CompanyID,
	}
	bs.SetCompany(u.CompanyID)
	err := bm.dbmodel.FindByID(c)
	if err != nil {
		return "", err
	}
	sm := NewSerialModel(bm.dbmodel.db)
	serial, err := sm.GetAndUpdateSerial(bs)
	if err != nil {
		return "", err
	}
	bs.VoucherNumber = util.GetVoucherNumber(bs.DateTime, serial)
	bs.TaxInfo.Rate = c.TaxRate
	bs.TaxInfo.SysAmount = CalTaxFromAmount(bs.Amount, c.TaxRate)
	for _, s := range bi.CreditAccTerm {
		acc := s.GetAccount(bi.Date, doc.TypeAccountCredit, bi.BudgetId, u.CompanyID, bs.VoucherNumber, false)
		ops = append(ops, acc.GetSaveTxnOp(u))
		bs.CreditAccTerm = append(bs.CreditAccTerm, acc.GetMiniAccount())
	}

	for _, s := range bi.DebitAccTerm {
		acc := s.GetAccount(bi.Date, doc.TypeAccountDebit, bi.BudgetId, u.CompanyID, bs.VoucherNumber, false)
		ops = append(ops, acc.GetSaveTxnOp(u))
		bs.DebitAccTerm = append(bs.DebitAccTerm, acc.GetMiniAccount())
	}

	ops = append(ops, bs.GetSaveTxnOp(u))

	err = bm.dbmodel.RunTxn(ops)
	if err != nil {
		return "", err
	}
	return bs.ID, nil
}

// CalTaxFromAmount 依含稅金額算稅
func CalTaxFromAmount(amount int, taxRate int) int {
	ftr := float64(taxRate)
	tax := ftr / (1.0 + ftr) * float64(amount)
	return int(math.Floor(tax + 0.5))
}

func (bm *receiptModel) Modify(pi *input.PutReceipt, u *input.ReqUser) error {
	or := &doc.Receipt{ID: pi.ID, CompanyID: u.CompanyID}
	err := bm.dbmodel.FindByID(or)
	if or == nil || err != nil {
		return err
	}

	c := &doc.Company{
		ID: u.CompanyID,
	}
	err = bm.dbmodel.FindByID(c)
	if err != nil {
		return err
	}

	var ops []txn.Op
	delAcc := doc.Account{
		CompanyID: or.CompanyID,
	}
	if len(pi.DelCreditList) > 0 {
		for _, aid := range pi.DelCreditList {
			delAcc.ID = aid
			ops = append(ops, delAcc.GetDelTxnOp())
		}
	}
	if len(pi.DelDebitList) > 0 {
		for _, aid := range pi.DelDebitList {
			delAcc.ID = aid
			ops = append(ops, delAcc.GetDelTxnOp())
		}
	}
	var newDebit []*doc.MiniAccount
	for _, sd := range pi.DebitAccTerm {
		if sd == nil {
			continue
		}
		if sd.Id == nil {
			acc := sd.GetAccount(pi.Date, doc.TypeAccountDebit, pi.BudgetId, u.CompanyID, or.VoucherNumber, false)
			ops = append(ops, acc.GetSaveTxnOp(u))
			newDebit = append(newDebit, acc.GetMiniAccount())
			ops = append(ops)
		} else if a := or.GetDebitAccTerm(*sd.Id); a != nil {
			ops = append(ops, a.GetUpdateTxnOp(
				bson.D{
					{Name: "code", Value: sd.Code},
					{Name: "name", Value: sd.Name},
					{Name: "amount", Value: sd.Amount},
					{Name: "summary", Value: sd.Desc},
					{Name: "datetime", Value: pi.Date},
					{Name: "year", Value: pi.Date.Year() - 1911},
					{Name: "budgetid", Value: pi.BudgetId},
					{Name: "type", Value: doc.TypeAccountDebit},
				}, u.CompanyID))
			a.Code = sd.Code
			a.Name = sd.Name
			a.Desc = sd.Desc
			a.Amount = sd.Amount
			newDebit = append(newDebit, a)
		} else {
			acc := sd.GetAccount(pi.Date, doc.TypeAccountDebit, pi.BudgetId, u.CompanyID, or.VoucherNumber, false)
			ops = append(ops, acc.GetSaveTxnOp(u))
			newDebit = append(newDebit, acc.GetMiniAccount())
			ops = append(ops)
		}
	}

	var newCredit []*doc.MiniAccount
	for _, sd := range pi.CreditAccTerm {
		if sd == nil {
			continue
		}
		if sd.Id == nil {
			acc := sd.GetAccount(pi.Date, doc.TypeAccountCredit, pi.BudgetId, u.CompanyID, or.VoucherNumber, false)
			ops = append(ops, acc.GetSaveTxnOp(u))
			newCredit = append(newCredit, acc.GetMiniAccount())
			ops = append(ops)
		} else if a := or.GetCreditAccTerm(*sd.Id); a != nil {
			ops = append(ops, a.GetUpdateTxnOp(
				bson.D{
					{Name: "code", Value: sd.Code},
					{Name: "name", Value: sd.Name},
					{Name: "amount", Value: sd.Amount},
					{Name: "summary", Value: sd.Desc},
					{Name: "datetime", Value: pi.Date},
					{Name: "year", Value: pi.Date.Year() - 1911},
					{Name: "budgetid", Value: pi.BudgetId},
					{Name: "typ", Value: doc.TypeAccountCredit},
				}, u.CompanyID))
			a.Code = sd.Code
			a.Name = sd.Name
			a.Desc = sd.Desc
			a.Amount = sd.Amount
			newCredit = append(newCredit, a)
		} else {
			acc := sd.GetAccount(pi.Date, doc.TypeAccountCredit, pi.BudgetId, u.CompanyID, or.VoucherNumber, false)
			ops = append(ops, acc.GetSaveTxnOp(u))
			newCredit = append(newCredit, acc.GetMiniAccount())
			ops = append(ops)
		}
	}

	newTaxInfo := doc.Tax{
		Typ:       pi.TaxInfo.Typ,
		Amount:    pi.TaxInfo.Amount,
		Rate:      c.TaxRate,
		SysAmount: CalTaxFromAmount(pi.Amount, c.TaxRate),
	}
	ops = append(ops, or.GetUpdateTxnOp(
		bson.D{
			{Name: "datetime", Value: pi.Date},
			{Name: "year", Value: pi.Date.Year() - 1911},
			{Name: "typ", Value: pi.Typ},
			{Name: "budgetid", Value: pi.BudgetId},
			{Name: "typeacc", Value: pi.AccType},
			{Name: "number", Value: pi.Number},
			{Name: "vatnumber", Value: pi.VATnumber},
			{Name: "amount", Value: pi.Amount},
			{Name: "iscollect", Value: pi.IsCollect},
			{Name: "collectquantity", Value: pi.CollectQuantity},
			{Name: "isfixedasset", Value: pi.IsFixedAsset},
			{Name: "debitaccterm", Value: newDebit},
			{Name: "creditaccterm", Value: newCredit},
			{Name: "taxinfo", Value: newTaxInfo},
			{Name: "pictures", Value: pi.Pictures},
		}))

	err = bm.dbmodel.RunTxn(ops)
	if err != nil {
		return err
	}

	//TODO: delete firebase image
	for _, u := range pi.DelPictures {
		bm.delImageObj(u)
	}
	return bm.dbmodel.addDocLog(or, u, doc.ActUpdate)
}

func (bm *receiptModel) hasRelateData(rid, cid bson.ObjectId) (bool, string) {
	r := &doc.Rebate{CompanyID: cid}
	err := bm.dbmodel.FindOne(r, bson.M{"receiptid": rid})
	if err == nil && r.ID.Valid() {
		return true, "has rebate"
	}
	return false, ""
}

func (bm *receiptModel) Delete(pi doc.DocInter, u *input.ReqUser) error {
	or := &doc.Receipt{ID: pi.GetID(), CompanyID: u.CompanyID}
	err := bm.dbmodel.FindByID(or)
	if err != nil {
		return rsrc.NewApiError(http.StatusNotFound, err.Error())
	}

	ok, msg := bm.hasRelateData(pi.GetID(), u.CompanyID)
	if ok {
		return rsrc.NewApiError(http.StatusForbidden, msg)
	}

	var ops []txn.Op
	for _, ca := range or.CreditAccTerm {
		ops = append(ops, ca.GetDelTxnOp(u.CompanyID))
	}
	for _, da := range or.DebitAccTerm {
		ops = append(ops, da.GetDelTxnOp(u.CompanyID))
	}
	ops = append(ops, or.GetDelTxnOp())
	err = bm.dbmodel.RunTxn(ops)
	if err != nil {
		return err
	}

	// delete all image
	for _, u := range or.Pictures {
		bm.delImageObj(u)
	}
	return bm.dbmodel.addDocLog(or, u, doc.ActDelete)
}

func (bm *receiptModel) delImageObj(u string) {
	up, err := url.Parse(u)
	if err != nil {
		bm.log.Err(err.Error())
		return
	}
	if bm.storage == nil {
		bm.log.Err("storage not set")
		return
	}
	sp := strings.Split(up.EscapedPath(), "/")
	delObj := strings.ReplaceAll(sp[len(sp)-1], "%2F", "/")
	err = bm.storage.Delete(delObj)
	if err != nil {
		bm.log.Err(err.Error())
	}
	sp = strings.Split(delObj, "/")
	l := len(sp)
	sp[l-1] = util.StrAppend("thumb_", sp[l-1])
	delObj = strings.Join(sp, "/")
	err = bm.storage.Delete(delObj)
	if err != nil {
		bm.log.Err(err.Error())
	}
}

func (bm *receiptModel) ReplaceAccterm(inp *input.AccTermRepl, rep *doc.AccTerm, u *input.ReqUser) (failReceipt []bson.ObjectId, err error) {
	r := &doc.Receipt{CompanyID: inp.CompanyId}
	result, err := bm.dbmodel.Find(r, bson.M{"_id": bson.M{"$in": inp.Ids}}, 0, 0)
	if err != nil {
		return inp.Ids, err
	}
	reclist, ok := result.([]*doc.Receipt)
	if !ok {
		return inp.Ids, errors.New("data type error")
	}
	for _, r = range reclist {
		var ops []txn.Op
		l := len(r.DebitAccTerm)
		var a *doc.MiniAccount
		for i := 0; i < l; i++ {
			a = r.DebitAccTerm[i]
			if a.Code == inp.Target {
				r.DebitAccTerm[i].Code = rep.Code
				r.DebitAccTerm[i].Name = rep.Name
				ops = append(ops, a.GetUpdateTxnOp(
					bson.D{
						{Name: "code", Value: rep.Code},
						{Name: "name", Value: rep.Name},
					}, u.CompanyID))
			}
		}
		l = len(r.CreditAccTerm)
		for i := 0; i < l; i++ {
			a = r.CreditAccTerm[i]
			if a.Code == inp.Target {
				r.CreditAccTerm[i].Code = rep.Code
				r.CreditAccTerm[i].Name = rep.Name
				ops = append(ops, a.GetUpdateTxnOp(
					bson.D{
						{Name: "code", Value: rep.Code},
						{Name: "name", Value: rep.Name},
					}, u.CompanyID))
			}
		}
		ops = append(ops, r.GetUpdateTxnOp(
			bson.D{
				{Name: "debitaccterm", Value: r.DebitAccTerm},
				{Name: "creditaccterm", Value: r.CreditAccTerm},
			}))

		err = bm.dbmodel.RunTxn(ops)
		if err != nil {
			failReceipt = append(failReceipt, r.ID)
			bm.log.Err(fmt.Sprintf("replace receipt %s fail: %s", r.ID.Hex(), err.Error()))
		}
	}
	return failReceipt, err
}
