package model

import (
	"errors"
	"fmt"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/rsrc/log"
	"kalimasi/util"
	"math"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

type rebateModel struct {
	dbmodel *mgoDBModel
	log     *log.Logger
}

func GetRebateModel(mgodb *mgo.Database) *rebateModel {
	mongo := GetMgoDBModel(mgodb)
	return &rebateModel{dbmodel: mongo, log: mongo.log}
}

func (bm *rebateModel) Create(bi *input.CreateRebate, u *input.ReqUser) error {
	bs := bi.GetRebateDoc()
	var ops []txn.Op

	c := &doc.Company{
		ID: u.CompanyID,
	}
	bs.SetCompany(u.CompanyID)
	err := bm.dbmodel.FindByID(c)
	if err != nil {
		return err
	}

	var budgetId *bson.ObjectId
	if bi.ReceiptID != nil {
		r := &doc.Receipt{
			ID:        *bi.ReceiptID,
			CompanyID: u.CompanyID,
		}
		err = bm.dbmodel.FindByID(r)
		if err != nil {
			return err
		}
		budgetId = r.BudgetId
		bs.BudgetId = budgetId
	}

	sm := NewSerialModel(bm.dbmodel.db)
	serial, err := sm.GetAndUpdateSerial(bs)
	if err != nil {
		return err
	}
	voucherNumber := util.GetVoucherNumber(bs.DateTime, serial)
	bs.VoucherNumber = voucherNumber
	tax := float64(bs.Amount*c.TaxRate) / 100.0
	bs.TaxInfo.Rate = c.TaxRate
	bs.TaxInfo.SysAmount = int(math.Floor(tax + 0.5))
	for _, s := range bi.CreditAccTerm {
		acc := s.GetAccount(bi.Date, doc.TypeAccountCredit, budgetId, u.CompanyID, voucherNumber, false)
		ops = append(ops, acc.GetSaveTxnOp(u))
		bs.CreditAccTerm = append(bs.CreditAccTerm, acc.GetMiniAccount())
	}

	for _, s := range bi.DebitAccTerm {
		acc := s.GetAccount(bi.Date, doc.TypeAccountCredit, budgetId, u.CompanyID, voucherNumber, false)
		ops = append(ops, acc.GetSaveTxnOp(u))
		bs.DebitAccTerm = append(bs.DebitAccTerm, acc.GetMiniAccount())
	}

	ops = append(ops, bs.GetSaveTxnOp(u))

	return bm.dbmodel.RunTxn(ops)
}

func (bm *rebateModel) Modify(pi *input.PutRebate, u *input.ReqUser) error {
	or := &doc.Rebate{ID: pi.ID, CompanyID: u.CompanyID}
	err := bm.dbmodel.FindByID(or)
	if err != nil {
		return err
	}

	c := &doc.Company{
		ID: u.CompanyID,
	}
	err = bm.dbmodel.FindByID(c)
	if err != nil {
		return err
	}

	var budgetId *bson.ObjectId
	if pi.ReceiptID != nil {
		r := &doc.Receipt{
			ID:        *pi.ReceiptID,
			CompanyID: u.CompanyID,
		}
		err = bm.dbmodel.FindByID(r)
		if err != nil {
			return err
		}
		budgetId = r.BudgetId
	}

	var ops []txn.Op
	delAcc := doc.Account{
		CompanyID: or.CompanyID,
	}
	if len(pi.DelCreditList) > 0 {
		for _, aid := range pi.DelCreditList {
			delAcc.ID = aid
			ops = append(ops, delAcc.GetDelTxnOp())
		}
	}
	if len(pi.DelDebitList) > 0 {
		for _, aid := range pi.DelDebitList {
			delAcc.ID = aid
			ops = append(ops, delAcc.GetDelTxnOp())
		}
	}
	var newDebit []*doc.MiniAccount
	for _, sd := range pi.DebitAccTerm {
		if sd.Id == nil {
			acc := sd.GetAccount(pi.Date, doc.TypeAccountDebit, budgetId, u.CompanyID, or.VoucherNumber, false)
			ops = append(ops, acc.GetSaveTxnOp(u))
			newDebit = append(newDebit, acc.GetMiniAccount())
			ops = append(ops)
		} else if a := or.GetDebitAccTerm(*sd.Id); a != nil {
			ops = append(ops, a.GetUpdateTxnOp(
				bson.D{
					{Name: "code", Value: a.Code},
					{Name: "name", Value: a.Name},
					{Name: "amount", Value: a.Amount},
					{Name: "summary", Value: a.Desc},
					{Name: "datetime", Value: pi.Date},
					{Name: "year", Value: pi.Date.Year() - 1911},
					{Name: "budgetid", Value: budgetId},
					{Name: "type", Value: doc.TypeAccountDebit},
				}, u.CompanyID))
			newDebit = append(newDebit, a)
		} else {
			acc := sd.GetAccount(pi.Date, doc.TypeAccountDebit, budgetId, u.CompanyID, or.VoucherNumber, false)
			ops = append(ops, acc.GetSaveTxnOp(u))
			newDebit = append(newDebit, acc.GetMiniAccount())
			ops = append(ops)
		}
	}

	var newCredit []*doc.MiniAccount
	for _, sd := range pi.CreditAccTerm {
		if sd.Id == nil {
			acc := sd.GetAccount(pi.Date, doc.TypeAccountCredit, budgetId, u.CompanyID, or.VoucherNumber, false)
			ops = append(ops, acc.GetSaveTxnOp(u))
			newCredit = append(newCredit, acc.GetMiniAccount())
			ops = append(ops)
		} else if a := or.GetCreditAccTerm(*sd.Id); a != nil {
			ops = append(ops, a.GetUpdateTxnOp(
				bson.D{
					{Name: "code", Value: a.Code},
					{Name: "name", Value: a.Name},
					{Name: "amount", Value: a.Amount},
					{Name: "summary", Value: a.Desc},
					{Name: "datetime", Value: pi.Date},
					{Name: "year", Value: pi.Date.Year() - 1911},
					{Name: "budgetid", Value: budgetId},
					{Name: "type", Value: doc.TypeAccountCredit},
				}, u.CompanyID))
			newCredit = append(newCredit, a)
		} else {
			acc := sd.GetAccount(pi.Date, doc.TypeAccountCredit, budgetId, u.CompanyID, or.VoucherNumber, false)
			ops = append(ops, acc.GetSaveTxnOp(u))
			newCredit = append(newCredit, acc.GetMiniAccount())
			ops = append(ops)
		}
	}

	tax := float64(pi.Amount*c.TaxRate) / 100.0
	newTaxInfo := doc.Tax{
		Typ:       pi.TaxInfo.Typ,
		Amount:    pi.TaxInfo.Amount,
		Rate:      c.TaxRate,
		SysAmount: int(math.Floor(tax + 0.5)),
	}
	ops = append(ops, or.GetUpdateTxnOp(
		bson.D{
			{Name: "datetime", Value: pi.Date},
			{Name: "year", Value: pi.Date.Year() - 1911},
			{Name: "budgetid", Value: budgetId},
			{Name: "typeacc", Value: pi.AccType},
			{Name: "amount", Value: pi.Amount},
			{Name: "debitaccterm", Value: newDebit},
			{Name: "creditaccterm", Value: newCredit},
			{Name: "taxinfo", Value: newTaxInfo},
		}))

	err = bm.dbmodel.RunTxn(ops)
	if err != nil {
		return err
	}
	return bm.dbmodel.addDocLog(or, u, doc.ActUpdate)
}

func (bm *rebateModel) Delete(pi doc.DocInter, u *input.ReqUser) error {
	or := &doc.Rebate{ID: pi.GetID(), CompanyID: u.CompanyID}
	err := bm.dbmodel.FindByID(or)
	if err != nil {
		return err
	}
	var ops []txn.Op
	for _, ca := range or.CreditAccTerm {
		ops = append(ops, ca.GetDelTxnOp(u.CompanyID))
	}
	for _, da := range or.DebitAccTerm {
		ops = append(ops, da.GetDelTxnOp(u.CompanyID))
	}
	ops = append(ops, or.GetDelTxnOp())
	err = bm.dbmodel.RunTxn(ops)
	if err != nil {
		return err
	}
	return bm.dbmodel.addDocLog(or, u, doc.ActDelete)
}

func (bm *rebateModel) ReplaceAccterm(inp *input.AccTermRepl, rep *doc.AccTerm, u *input.ReqUser) (failReceipt []bson.ObjectId, err error) {
	r := &doc.Rebate{CompanyID: inp.CompanyId}
	result, err := bm.dbmodel.Find(r, bson.M{"_id": bson.M{"$in": inp.Ids}}, 0, 0)
	if err != nil {
		return inp.Ids, err
	}
	reclist, ok := result.([]*doc.Rebate)
	if !ok {
		return inp.Ids, errors.New("data type error")
	}
	for _, r = range reclist {
		var ops []txn.Op
		l := len(r.DebitAccTerm)
		var a *doc.MiniAccount
		for i := 0; i < l; i++ {
			a = r.DebitAccTerm[i]
			if a.Code == inp.Target {
				r.DebitAccTerm[i].Code = rep.Code
				r.DebitAccTerm[i].Name = rep.Name
				ops = append(ops, a.GetUpdateTxnOp(
					bson.D{
						{Name: "code", Value: rep.Code},
						{Name: "name", Value: rep.Name},
					}, u.CompanyID))
			}
		}
		l = len(r.CreditAccTerm)
		for i := 0; i < l; i++ {
			a = r.CreditAccTerm[i]
			if a.Code == inp.Target {
				r.CreditAccTerm[i].Code = rep.Code
				r.CreditAccTerm[i].Name = rep.Name
				ops = append(ops, a.GetUpdateTxnOp(
					bson.D{
						{Name: "code", Value: rep.Code},
						{Name: "name", Value: rep.Name},
					}, u.CompanyID))
			}
		}
		ops = append(ops, r.GetUpdateTxnOp(
			bson.D{
				{Name: "debitaccterm", Value: r.DebitAccTerm},
				{Name: "creditaccterm", Value: r.CreditAccTerm},
			}))

		err = bm.dbmodel.RunTxn(ops)
		if err != nil {
			failReceipt = append(failReceipt, r.ID)
			bm.log.Err(fmt.Sprintf("replace receipt %s fail: %s", r.ID.Hex(), err.Error()))
		}
	}
	return failReceipt, err
}
