package model

import (
	"fmt"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/rsrc"
	"log"
	"testing"

	"github.com/globalsign/mgo/bson"
	"github.com/stretchr/testify/assert"
)

func Test_Test(t *testing.T) {

	fmt.Println("Say hi")
	log.Println("test")
	t.Log("Say bye")

	assert := assert.New(t)

	var a string = "Hello"
	var b string = "Hello1"

	assert.Equal(a, b, "The two words should be the same.")
	//t.Error("TEST")

}

func Test_SaveAccTerm(t *testing.T) {
	rsrc.InitConfByFile("../conf/dev/config.yml", "Asia/Taipei")
	assetsGroup := &doc.AccTermGroup{Name: "銀行存款", Code: "1110"} //會計科目 -> 管理
	assetsGroup.AddSub("合作金庫", "111201")                         //子項目
	r := &doc.AccTermList{
		Typ:   "assets", //資產類
		Terms: []*doc.AccTermGroup{assetsGroup},
	}
	u := &input.ReqUser{
		Name:      "peter",
		Account:   "ch.focke@gmail.com",
		CompanyID: bson.ObjectIdHex("0000000000bc614e00000000"),
	}
	fmt.Println("CompanyID:", r.CompanyID)
	di := rsrc.GetDI()
	mdbm := GetMgoDBModel(di.GetMongoByKey("testKey"))
	err := mdbm.Save(r, u)
	fmt.Println("Test_SaveAccTerm:", err)
	assert.True(t, false)
}
/*
func Test_ClosingAccount(t *testing.T) {
	rsrc.InitConfByFile("../conf/dev/config.yml", "Asia/Taipei")
	di := rsrc.GetDI()
	mdb := di.GetMongoByKey("testKey")
	am := GetAccountModel(mdb)
	err := am.Closing(bson.ObjectIdHex("00000000007543d500000000"), 109)
	fmt.Println(err)
	assert.True(t, false)
}*/

func Test_StringMatch(t *testing.T) {
	rsrc.InitConfByFile("../conf/prod/config.yml", "Asia/Taipei")
	di := rsrc.GetDI()
	mdb := di.GetMongoByKey("testKey")
	atm := GetAccTermModel(mdb)
	//Copy from NoSQLBooster for MongoDB free edition. This message does not appear if you are using a registered version.
	cid := bson.ObjectIdHex("0000000004e33c8500000000")
	code := "600301"
	result := atm.Refrence(cid, code)
	if result != nil {
		fmt.Println(result.RefReport, result.HasCompanyPayMethod, result.HasCompanyIncome)
	} else {
		fmt.Println("not found")
	}

	assert.True(t, false)
}
