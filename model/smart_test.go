package model

import (
	"fmt"
	"testing"

	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/rsrc"

	"github.com/stretchr/testify/assert"
	"github.com/globalsign/mgo/bson"

)

func Test_ModelGet(t *testing.T){
	rsrc.InitConfByFile("../conf/dev/config.yml", "Asia/Taipei")
	bID := "5e84052b363c913afc4725c9"

	ui := &input.ReqUser{
		CompanyID: bson.ObjectIdHex(bID),
	}

	pi := &input.ComUpdateInput{
		VatNumber: "28862112",
	}

	di := rsrc.GetDI()
	mdbm := GetMgoDBModel(di.GetMongoByKey("testKey"))
	Vat := string(pi.VatNumber)
	comID := ui.GetCompany()

	r := &doc.MyFav{
		VatNumber: Vat,
		CompanyID: comID,
	}
	r.GetID()
	err := mdbm.FindByID(r)

	fmt.Println("Test_ModelGet:", err, r.FavComName)
	assert.True(t, false)
}

func Test_ModelSave(t *testing.T){
	rsrc.InitConfByFile("../conf/dev/config.yml", "Asia/Taipei")
	bID := "5e84052b363c913afc4725c9"

	ui := &input.ReqUser{
		CompanyID: bson.ObjectIdHex(bID),
	}

	testCom := &input.ComInfo{
		Name: "美香味小吃",
	}

	pi := &input.ComUpdateInput{
		VatNumber: "30148368",
		Company: *testCom,
	}

	di := rsrc.GetDI()
	mdbm := GetMgoDBModel(di.GetMongoByKey("testKey"))
	comID := ui.GetCompany()
	favCom := string(pi.Company.Name)
    favVat := string(pi.VatNumber)

	r := &doc.MyFav{
        CompanyID:comID,  
        FavComName:favCom,
    	VatNumber:favVat,
	}
	
	r.ID = r.GetID()
	mc := &doc.MyFav{
		ID:r.ID,
		CompanyID:comID, 
	}
	
	err := mdbm.FindByID(mc)
	if err == nil { 
		fmt.Println("in cond err nil:")
        err = mdbm.Update(r, ui)
	} else if err.Error() != "invalid id" {
		fmt.Println("in cond err invalid ID:")
        err = mdbm.Save(r, ui)
    } else {
		fmt.Println("in cond else:")
	}
	fmt.Println("Test_ModelSave:", err)

	assert.True(t, false)
}