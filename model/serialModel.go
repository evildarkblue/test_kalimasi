package model

import (
	"kalimasi/doc"
	"kalimasi/rsrc"
	"kalimasi/rsrc/log"

	"sync"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type serialDoc interface {
	GetDate() time.Time
	GetSerialType() string
	GetC() string
	GetCompany() bson.ObjectId
}

type serialModel struct {
	dbmodel *mgoDBModel
	log     *log.Logger
}

var (
	serialLock sync.Mutex
)

func NewSerialModel(mgodb *mgo.Database) *serialModel {
	mongo := GetMgoDBModel(mgodb)
	return &serialModel{dbmodel: mongo, log: mongo.log}
}

func (sm *serialModel) GetAndUpdateSerial(sd serialDoc) (int64, error) {
	dt := sd.GetDate()
	di := rsrc.GetDI()
	dt.In(di.Location)

	id, err := doc.GetSerialNumberId(sd.GetCompany(), dt)
	if err != nil {
		return -1, err
	}
	sn := &doc.SerialNumber{
		ID:        id,
		CompanyID: sd.GetCompany(),
	}
	serialLock.Lock()
	defer serialLock.Unlock()
	// serialNumber is a table which record the serail number in column "voucherNumber"
	err = sm.dbmodel.FindByID(sn)
	var result int64
	if err != nil {
		result = 1
	} else {
		result = sn.GetSerialNo(sd.GetSerialType())
	}
	if sn.NumberMap == nil {
		sn.NumberMap = make(map[string]int64)
	}
	sn.NumberMap[sd.GetSerialType()] = result

	err = sm.dbmodel.Upsert(sn, nil)
	if err != nil {
		return -1, err
	}

	sr := &doc.SerialRecord{
		CompanyID: sd.GetCompany(),
		Date:      sd.GetDate(),
		Key:       sd.GetSerialType(),
		C:         sd.GetC(),
		Serial:    result,
	}

	err = sm.dbmodel.Save(sr, nil)
	if err != nil {
		sm.log.Err(err.Error())
	}

	return result, nil
}
