package v1

import (
	"fmt"
	"kalimasi/model"
	"kalimasi/rsrc"
	"net/http"
	"time"
)

type NumaxSynAPI string

func (api NumaxSynAPI) GetName() string {
	return string(api)
}

func (a NumaxSynAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{
		{Path: "/v1/invoice", Next: a.invoiceEndpoint, Method: "POST", Auth: false},
		{Path: "/v1/hello", Next: a.helloEndpoint, Method: "GET", Auth: false},
	}
}

func (a NumaxSynAPI) Init() {

}

func (api *NumaxSynAPI) invoiceEndpoint(w http.ResponseWriter, req *http.Request) {
	d := rsrc.GetDI()
	mgoDB := model.GetMgoDBModelByReq(req)
	log := d.GetLog()
	syncNumax := model.NewNumaxSync(d.NumaxConf, mgoDB, log)
	err := syncNumax.SyncByDate(time.Now().AddDate(0, 0, -3))
	if err != nil {
		w.WriteHeader(http.StatusPartialContent)
		w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (api *NumaxSynAPI) helloEndpoint(w http.ResponseWriter, req *http.Request) {
	fmt.Println("hello")
}
