package v1

import (
	"encoding/json"
	"net/http"

	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/model"
	"kalimasi/rsrc"
	"kalimasi/util"
)

type AuthAPI string

func (api AuthAPI) GetName() string {
	return string(api)
}

func (a AuthAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{
		{Path: "/v1/consultant/invitation", Next: a.invitationEndpoint, Method: "POST", Auth: true},
		{Path: "/v1/login", Next: a.loginEndpoint, Method: "POST", Auth: false},
		{Path: "/info", Next: a.infoEndpoint, Method: "GET", Auth: false},
		{Path: "/v1/manager", Next: a.newManager, Method: "POST", Auth: true},
		{Path: "/v1/manager", Next: a.getManager, Method: "GET", Auth: true},
	}
}

func (a AuthAPI) Init() {

}

func (api *AuthAPI) loginEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.BasicLogin{
		Service: doc.UserServiceBSM,
	}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	cb.ClientInfo = util.GetClientInfo(req)
	mgodb := rsrc.GetDI().GetMongoByReq(req)
	um := model.GetUserModel(mgodb)
	u, p, s, err := um.BasicSignin(cb)
	if err != nil {
		switch err.Error() {
		case "company not found":
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte(err.Error()))
		case "not found":
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte("account or password error"))
		default:
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"token":      u,
		"permission": p,
		"state":      s,
	})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *AuthAPI) infoEndpoint(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte("bms"))
}

func (api *AuthAPI) invitationEndpoint(w http.ResponseWriter, req *http.Request) {
	cim := &input.CreateInviteMail{}
	err := json.NewDecoder(req.Body).Decode(cim)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cim.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	csm := model.GetConsultModel(dbclt)
	if csm.IsInviteMailExist(cim.Mail) {
		w.WriteHeader(http.StatusConflict)
		w.Write([]byte("mail is exist"))
		return
	}

	di := rsrc.GetDI()
	mailServ := di.GetMailServ()

	if err = csm.SendConsultInvitation(mailServ, cim, ui); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Write([]byte("ok"))
}

func (api *AuthAPI) newManager(w http.ResponseWriter, req *http.Request) {
	cbm := &input.CreateBmsManager{}
	err := json.NewDecoder(req.Body).Decode(cbm)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cbm.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	bmd := model.GetBmsModel(dbclt)
	err = bmd.Create(cbm, ui)
	if err != nil {
		if err.Error() == "Account or Phone Number exist" {
			w.WriteHeader(http.StatusConflict)
			w.Write([]byte(err.Error()))
			return
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}

	}
	w.Write([]byte("ok"))
}

func (api *AuthAPI) getManager(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"key"}, true)
	key := qv["key"].(string)

	dbclt := rsrc.GetDI().GetMongoByReq(req)
	bmd := model.GetBmsModel(dbclt)
	bmsMans, err := bmd.Get(key)
	if len(bmsMans) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	type bmsOut struct {
		Account string `json:"account"`
		Name    string `json:"name"`
		Phone   string `json:"phone"`
	}
	var output []bmsOut
	for _, man := range bmsMans {
		tmp := bmsOut{
			Account: man.Email,
			Name:    man.DisplayName,
			Phone:   man.PhoneNumber,
		}
		output = append(output, tmp)
	}
	w.Header().Set("Content-Type", "application/json")
	if err = json.NewEncoder(w).Encode(output); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(http.StatusOK)
}
