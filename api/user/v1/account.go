package v1

import (
	"encoding/json"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/model"
	"kalimasi/model/reportgener"
	"kalimasi/rsrc"
	"kalimasi/util"
	"net/http"

	"github.com/globalsign/mgo/bson"
)

type AccountAPI string

func (api AccountAPI) GetName() string {
	return string(api)
}

func (a AccountAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{
		&rsrc.APIHandler{Path: "/v1/account/opening", Next: a.openEndpoint, Method: "POST", Auth: true},
		&rsrc.APIHandler{Path: "/v1/account/opening", Next: a.openingEndpoint, Method: "GET", Auth: true},
		&rsrc.APIHandler{Path: "/v1/account/opening/{ID}", Next: a.modifyOpeingEndpoint, Method: "PUT", Auth: true},
		&rsrc.APIHandler{Path: "/v1/account/_close", Next: a.closeEndpoint, Method: "POST", Auth: true},
		&rsrc.APIHandler{Path: "/v1/account/closing", Next: a.getClosingAccEndpoint, Method: "GET", Auth: true},
	}
}

func (a AccountAPI) Init() {

}

func (api *AccountAPI) modifyOpeingEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.PutAccountOpen{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)
	if err != nil || qid != cb.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	rm := model.GetAccountModel(dbclt)
	err = rm.ModifyOpening(cb, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *AccountAPI) openingEndpoint(w http.ResponseWriter, req *http.Request) {
	ui := input.GetUserInfo(req)
	mgodb := model.GetMgoDBModelByReq(req)
	oa := &doc.OpeningAccount{
		CompanyID: ui.CompanyID,
	}
	result, err := mgodb.Find(oa, bson.M{}, 0, 0)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	out, _ := doc.Format(result, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.OpeningAccount); ok {
			var debit []map[string]interface{}
			for _, da := range d.DebitAccTerm {
				debit = append(debit, map[string]interface{}{
					"id":     da.ID.Hex(),
					"code":   da.Code,
					"name":   da.Name,
					"amount": da.Amount,
					"desc":   da.Desc,
				})
			}
			var credit []map[string]interface{}
			for _, da := range d.CreditAccTerm {
				credit = append(credit, map[string]interface{}{
					"id":     da.ID.Hex(),
					"code":   da.Code,
					"name":   da.Name,
					"amount": da.Amount,
					"desc":   da.Desc,
				})
			}
			return map[string]interface{}{
				"id":                d.ID.Hex(),
				"year":              d.Year,
				"debitAccTermList":  debit,
				"creditAccTermList": credit,
			}
		}
		return nil
	})
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *AccountAPI) openEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.CreateAccountOpen{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	rm := model.GetAccountModel(dbclt)
	err = rm.Opening(cb, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *AccountAPI) closeEndpoint(w http.ResponseWriter, req *http.Request) {
	//ui := input.GetUserInfo(req)

}

func (api *AccountAPI) getClosingAccEndpoint(w http.ResponseWriter, req *http.Request) {
	ui := input.GetUserInfo(req)
	bb := &reportgener.BalanceSheet{
		CompanyID: ui.CompanyID,
	}
	di := rsrc.GetDI()
	mdb := di.GetMongoByReq(req)
	rm := model.GetReportModel(mdb)
	if err := rm.LoadSetting(bb); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	const empty = ""
	var assets, liabs []map[string]interface{}
	for _, a := range bb.Assets {
		if a.AccCode != empty {
			assets = append(assets, map[string]interface{}{
				"code": a.AccCode,
				"name": a.Name,
			})
		}
		for _, as := range a.Sub {
			if as.AccCode != empty {
				assets = append(assets, map[string]interface{}{
					"code": as.AccCode,
					"name": as.Name,
				})
			}
		}
	}
	for _, l := range bb.Liab {
		if l.AccCode != empty {
			liabs = append(assets, map[string]interface{}{
				"code": l.AccCode,
				"name": l.Name,
			})
		}
		for _, ls := range l.Sub {
			if ls.AccCode != empty {
				liabs = append(assets, map[string]interface{}{
					"code": ls.AccCode,
					"name": ls.Name,
				})
			}
		}
	}
	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(map[string]interface{}{
		"assets": assets,
		"liabs":  liabs,
	})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}
