/*
 * 轉帳管理
 */
package doc

import (
	"kalimasi/util"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

const (
	transferC = "transfer"
)

type Transfer struct {
	ID            bson.ObjectId `bson:"_id"`
	DateTime      time.Time
	VoucherNumber string
	Year          int
	Summary       string
	CompanyID     bson.ObjectId

	BudgetId *bson.ObjectId
	Budget   []BudgetStatement `bson:"lookupBudget"`

	DebitAccTerm  []*MiniAccount
	CreditAccTerm []*MiniAccount

	CommonDoc `bson:"meta"`
	AuditID
	LookUpAudit `bson:"lookupAudit,omitempty"`
}

func (r *Transfer) GetDoc() interface{} {
	if !r.ID.Valid() {
		r.ID = r.newID()
	}
	r.Year = r.DateTime.Year() - 1911
	return r
}

func (r *Transfer) GetID() bson.ObjectId {
	return r.ID
}

func (r *Transfer) GetC() string {
	if !r.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(r.CompanyID.Hex(), transferC)
}

func (u *Transfer) SetCompany(c bson.ObjectId) {
	u.CompanyID = c
}

func (u *Transfer) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *Transfer) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *Transfer) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}

func (u *Transfer) GetPipeline(q bson.M) PipelineQryInter {
	bs := &BudgetStatement{CompanyID: u.CompanyID}
	a := &Audit{CompanyID: u.CompanyID}
	return &CommonPipeline{
		q: q,
		pipeline: []bson.M{
			bson.M{"$match": q},
			bson.M{
				"$lookup": bson.M{
					"from":         bs.GetC(),
					"localField":   "budgetid",
					"foreignField": "_id",
					"as":           "lookupBudget",
				},
			},
			bson.M{
				"$lookup": bson.M{
					"from":         a.GetC(),
					"localField":   "auditid",
					"foreignField": "_id",
					"as":           "lookupAudit",
				},
			},
		},
	}
}

func (r *Transfer) GetMongoIndexes() []mgo.Index {
	return []mgo.Index{
		{
			Key:        []string{"companyid", "year"},
			Background: true, // can be used while index is being built
		},
	}
}

// serialDoc interface
func (r *Transfer) GetCompany() bson.ObjectId {
	return r.CompanyID
}

func (r *Transfer) GetDate() time.Time {
	return r.DateTime
}

func (r *Transfer) GetSerialType() string {
	return TypeSerialVN
}
