package doc

import (
	"kalimasi/util"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

const (
	auditC = "audit"

	AuditStateNew     = "new"
	AuditStateProcess = "process"
	AuditStateReject  = "reject"
	AuditStatePass    = "pass"

	AuditProcTypAcc  = "account"
	AuditProcTypPerm = "permission"
)

type Audit struct {
	ID          bson.ObjectId `bson:"_id"`
	CompanyID   bson.ObjectId
	PreAuditID  []bson.ObjectId
	State       string
	Typ         string
	Summary     map[string]interface{}
	SerialJSON  []byte
	Checksum    string
	Applicant   SimpleUser  // 申請者
	Assessor    *SimpleUser // 待審核者
	UpdateDate  time.Time
	Records     []auditRecord
	Process     []AuditProcess //審核流程
	CurrProcess uint16         //目前審核
	Method      string         // POST || PUT || DELETE
	ActURL      string

	CommonDoc `bson:"meta"`
}

type auditRecord struct {
	UserID     bson.ObjectId
	Name       string
	Result     string // pass || reject
	Message    string
	CreateDate time.Time
}

// 審核流程
type AuditProcess struct {
	Typ   string // acc || perm
	Value string
}

func (a *Audit) CanApply(id, perm string) bool {
	if util.IsStrInList(a.State, AuditStatePass, AuditStateReject) {
		return false
	}
	if a.CurrProcess >= uint16(len(a.Process)) {
		return false
	}
	cp := a.Process[a.CurrProcess]
	if cp.Typ != AuditProcTypPerm && cp.Typ != AuditProcTypAcc {
		return false
	} else if cp.Typ == AuditProcTypPerm && perm != cp.Value {
		return false
	} else if cp.Typ == AuditProcTypAcc && id != cp.Value {
		return false
	}
	return true
}

func (a *Audit) AddRecord(su SimpleUser, r string, msg string) []auditRecord {
	if r != AuditStateReject && r != AuditStatePass {
		return a.Records
	}
	return append(a.Records, auditRecord{
		UserID:     su.ID,
		Name:       su.Name,
		Result:     r,
		Message:    msg,
		CreateDate: time.Now(),
	})
}

func (a *Audit) GetC() string {
	if !a.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(a.CompanyID.Hex(), auditC)
}
func (a *Audit) GetDoc() interface{} {
	if !a.ID.Valid() {
		a.ID = bson.NewObjectId()
	}
	return a
}
func (a *Audit) GetID() bson.ObjectId {
	return a.ID
}

func (a *Audit) GetUpdateField() bson.M {
	return bson.M{
		"records":     a.Records,
		"state":       a.State,
		"currprocess": a.CurrProcess,
		"updatedate":  time.Now(),
	}
}

func (a *Audit) GetSaveTxnOp(lu LogUser) txn.Op {
	return a.CommonDoc.getSaveTxnOp(a, lu)
}

func (a *Audit) GetUpdateTxnOp(data bson.D) txn.Op {
	return a.CommonDoc.getUpdateTxnOp(a, data)
}

func (a *Audit) SetCompany(c bson.ObjectId) {
	a.CompanyID = c
}

func (a *Audit) GetDelTxnOp() txn.Op {
	return a.CommonDoc.getDelTxnOp(a)
}

func (a *Audit) GetMongoIndexes() []mgo.Index {
	return []mgo.Index{
		{
			Key:        []string{"state", "typ"},
			Background: true, // can be used while index is being built
		},
		{
			Key:        []string{"typ"},
			Background: true, // can be used while index is being built
		},
		{
			Key:        []string{"state"},
			Background: true, // can be used while index is being built
		},
	}
}

type AuditSetting struct {
	IsAudit      bool
	AuditProcess []AuditProcess
}

type AuditUpdateDoc struct {
	DocInter
	AuditID bson.ObjectId
}

func (aud *AuditUpdateDoc) GetUpdateField() bson.M {
	return bson.M{"auditid": aud.AuditID}
}

type AuditID bson.ObjectId

func (aid AuditID) GetAuditID() bson.ObjectId {
	return bson.ObjectId(aid)
}

type LookUpAudit []*Audit

func (lua LookUpAudit) GetAuditState() string {
	if len(lua) == 0 {
		return "none"
	}
	return lua[0].State
}

type CompanyAudit struct {
	ID bson.ObjectId `bson:"_id"`
	AuditSetting
	emptyDoc
}

func (u *CompanyAudit) GetDoc() interface{} {
	if !u.ID.Valid() {
		panic("ID not set")
	}
	return u
}

func (u *CompanyAudit) GetC() string {
	return companyC
}

func (u *CompanyAudit) GetID() bson.ObjectId {
	return u.ID
}

func (u *CompanyAudit) SetCompany(c bson.ObjectId) {
	u.ID = c
}

func (u *CompanyAudit) GetUpdateField() bson.M {
	return bson.M{
		"auditsetting": u.AuditSetting,
	}
}

func (u *CompanyAudit) GetSaveTxnOp(lu LogUser) txn.Op {
	panic("not support save")
}

func (u *CompanyAudit) GetUpdateTxnOp(data bson.D) txn.Op {
	return txn.Op{
		C:      u.GetC(),
		Id:     u.GetID(),
		Assert: txn.DocExists,
		Update: bson.D{{Name: "$set", Value: bson.D{{
			Name: "auditsetting", Value: u.AuditSetting,
		}}}},
	}
}
