package doc

import (
	"encoding/binary"
	"kalimasi/rsrc"
	"kalimasi/util"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

const (
	openingAccount = "openingAcc"
)

type OpeningAccount struct {
	ID            bson.ObjectId `bson:"_id"`
	CompanyID     bson.ObjectId
	Year          uint16
	VoucherNumber string

	DebitAccTerm  []*MiniAccount
	CreditAccTerm []*MiniAccount

	CreateDate time.Time

	CommonDoc `bson:"meta"`
}

func (d *OpeningAccount) newID() bson.ObjectId {
	var b [12]byte
	bs := make([]byte, 8)
	binary.BigEndian.PutUint16(bs, d.Year)
	for i := 0; i < 2; i++ {
		b[i] = bs[i]
	}
	return bson.ObjectId(b[:])
}

func (u *OpeningAccount) GetID() bson.ObjectId {
	if !u.ID.Valid() {
		u.ID = u.newID()
	}
	return u.ID
}

func (u *OpeningAccount) GetDoc() interface{} {
	if !u.ID.Valid() {
		u.ID = u.newID()
	}
	return u
}

func (u *OpeningAccount) SetCompany(c bson.ObjectId) {
	u.CompanyID = c
}

func (u *OpeningAccount) GetC() string {
	if !u.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(u.CompanyID.Hex(), openingAccount)
}

func (u *OpeningAccount) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *OpeningAccount) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *OpeningAccount) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}

// serialDoc interface
func (u *OpeningAccount) GetCompany() bson.ObjectId {
	return u.CompanyID
}

func (u *OpeningAccount) GetDate() time.Time {
	di := rsrc.GetDI()
	l := di.GetLocation()
	return time.Date(int(u.Year)+1911, 1, 1, 0, 0, 0, 0, l)
}

func (r *OpeningAccount) GetSerialType() string {
	return TypeSerialVN
}
