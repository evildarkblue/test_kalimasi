package doc

import (
	"encoding/binary"
	"errors"
	"kalimasi/util"
	"strconv"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

const (
	memberC = "member"

	TypeMemberPerson     = "person"
	TypeMemberEnterprise = "enterprise"

	SexGirl = 0
	SexBoy  = 1
	SexNone = 2

	TypeContactHome   = "home"
	TypeContactOffice = "office"
	TypeContactMobile = "mobile"
)

type Contact struct {
	Typ   string `json:"ctType"`
	Value string `json:"value"`
}

type Member struct {
	ID          bson.ObjectId `bson:"_id"`
	Typ         string
	IDnumber    string // 身份證字號(統一編號)
	Name        string
	Sex         int
	Birthday    time.Time
	Email       string
	Address     string
	ServiceUnit string
	Contact     []*Contact

	PayRecord []*PayRecord `json:"-" bson:"lookupPayRecord,omitempty"`

	CompanyID bson.ObjectId

	CommonDoc `bson:"meta"`
}

var (
	idTransMap = map[byte]string{
		'A': "10", 'F': "15", 'K': "20", 'O': "24", 'S': "28", 'W': "32",
		'B': "11", 'G': "16", 'L': "21", 'P': "25", 'T': "29", 'X': "33",
		'C': "12", 'H': "17", 'M': "22", 'Q': "26", 'U': "30", 'Y': "34",
		'D': "13", 'I': "18", 'N': "23", 'R': "27", 'V': "31", 'Z': "35",
		'E': "14", 'J': "19",
	}

	AllowContactType = []string{
		TypeContactMobile, TypeContactOffice, TypeContactHome,
	}
)

func getMemberId(unitCode string) (bson.ObjectId, error) {
	if !util.IsIdNumber(unitCode) {
		return "", errors.New("invalid idNumber")
	}
	unitCode = util.StrAppend(idTransMap[unitCode[0]], unitCode[1:])

	var b [12]byte
	i, err := strconv.ParseUint(unitCode, 10, 64)
	if err != nil {
		return "", errors.New("invalid unitCode:" + err.Error())
	}
	bs := make([]byte, 8)
	binary.BigEndian.PutUint64(bs, i)
	for i = 0; i < 8; i++ {
		b[i] = bs[i]
	}
	return bson.ObjectId(b[:]), nil
}

func (d *Member) newID() bson.ObjectId {
	var id bson.ObjectId
	var err error
	if d.Typ == TypeMemberEnterprise {
		id, err = GetCompanyId(d.IDnumber, false)
	} else {
		id, err = getMemberId(d.IDnumber)
	}

	if err != nil {
		panic(err)
	}
	return id
}

func (u *Member) GetDoc() interface{} {
	if !u.ID.Valid() {
		u.ID = u.newID()
	}
	return u
}

func (u *Member) GetC() string {
	if !u.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(u.CompanyID.Hex(), memberC)
}

func (u *Member) GetID() bson.ObjectId {
	if !u.ID.Valid() {
		u.ID = u.newID()
	}
	return u.ID
}

func (u *Member) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *Member) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *Member) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}

func (u *Member) GetUpdateField() bson.M {
	return bson.M{
		"name":        u.Name,
		"sex":         u.Sex,
		"birthday":    u.Birthday,
		"email":       u.Email,
		"address":     u.Address,
		"serviceunit": u.ServiceUnit,
		"contact":     u.Contact,
	}
}

func (u *Member) GetPipeline(q bson.M) PipelineQryInter {
	pr := &PayRecord{
		CompanyID: u.CompanyID,
	}
	return &CommonPipeline{
		q: q,
		pipeline: []bson.M{
			bson.M{"$match": q},
			bson.M{
				"$lookup": bson.M{
					"from":         pr.GetC(),
					"localField":   "_id",
					"foreignField": "memberid",
					"as":           "lookupPayRecord",
				},
			},
		},
	}
}
