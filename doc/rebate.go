/*
 * 折讓管理
 */
package doc

import (
	"kalimasi/util"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

const (
	rebateC = "rebate"
)

type Rebate struct {
	ID            bson.ObjectId `bson:"_id"`
	VoucherNumber string
	DateTime      time.Time
	Year          int
	TypeAcc       string
	Amount        int
	TaxInfo       Tax

	ReceiptID *bson.ObjectId

	CompanyID bson.ObjectId
	BudgetId  *bson.ObjectId

	DebitAccTerm  []*MiniAccount
	CreditAccTerm []*MiniAccount

	CommonDoc `bson:"meta"`
}

func (r *Rebate) GetDebitAccTerm(id bson.ObjectId) *MiniAccount {
	for _, d := range r.DebitAccTerm {
		if d.ID == id {
			return d
		}
	}
	return nil
}

func (r *Rebate) GetCreditAccTerm(id bson.ObjectId) *MiniAccount {
	for _, d := range r.CreditAccTerm {
		if d.ID == id {
			return d
		}
	}
	return nil
}

func (r *Rebate) GetDoc() interface{} {
	if !r.ID.Valid() {
		r.ID = r.newID()
	}
	r.Year = r.DateTime.Year() - 1911
	return r
}

func (r *Rebate) GetID() bson.ObjectId {
	return r.ID
}

func (r *Rebate) GetC() string {
	if !r.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(r.CompanyID.Hex(), rebateC)
}

func (u *Rebate) SetCompany(c bson.ObjectId) {
	u.CompanyID = c
}

func (u *Rebate) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *Rebate) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *Rebate) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}

func (r *Rebate) GetMongoIndexes() []mgo.Index {
	return []mgo.Index{
		{
			Key:        []string{"year", "typeacc"},
			Background: true, // can be used while index is being built
		},
	}
}

// serialDoc interface
func (u *Rebate) GetCompany() bson.ObjectId {
	return u.CompanyID
}

func (u *Rebate) GetDate() time.Time {
	return u.DateTime
}

func (u *Rebate) GetSerialType() string {
	return TypeSerialVN
}

type RebateJoinReceipt struct {
	Rebate      `bson:",inline"`
	LookReceipt Receipt `bson:"lookupReceipt"`
}

func (u *RebateJoinReceipt) GetPipeline(q bson.M) PipelineQryInter {
	r := &Receipt{
		CompanyID: u.CompanyID,
	}
	return &CommonPipeline{
		q: q,
		pipeline: []bson.M{
			{"$match": q},
			{
				"$lookup": bson.M{
					"from":         r.GetC(),
					"localField":   "receiptid",
					"foreignField": "_id",
					"as":           "lookupReceipt",
				},
			},
			{
				"$unwind": bson.M{
					"path":                       "$lookupReceipt",
					"preserveNullAndEmptyArrays": false,
				},
			},
		},
	}
}
