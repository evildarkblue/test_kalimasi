package doc

import (
	"errors"
	"kalimasi/util"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

const (
	UserPermOwn  = "own"
	UserPermPro  = "pro"
	UserPermUser = "user"

	UserStateNew    = "new"
	UserStateNormal = "normal"

	UserServiceBSM     = "bsm"     // 後台管理服務
	UserServiceConsult = "consult" // 顧問服務

	ContactTypeMobile = "mobile"
	ContactTypeHome   = "home"
	ContactTypeOffice = "office"

	userC        = "user"
	userCompanyC = "userCompany"
)

type ContactInfo struct {
	Contacts []contactRecord // 聯絡電話
}

func (ci ContactInfo) GetContractRecord(i int) contactRecord {
	l := len(ci.Contacts)
	if l == 0 {
		return contactRecord{
			"", "", "",
		}
	}
	if i < 0 {
		i = 0
	} else if i > l {
		i = l - 1
	}
	return ci.Contacts[i]
}

func (ci ContactInfo) GetPhone() string {
	if len(ci.Contacts) == 0 {
		return ""
	}
	return ci.Contacts[0].Value
}

func (ci *ContactInfo) AddContact(typ string, val string) {
	if !util.IsStrInList(typ, ContactTypeMobile, ContactTypeHome, ContactTypeOffice) {
		return
	}
	ci.Contacts = append(ci.Contacts, contactRecord{
		ID:    bson.NewObjectId().Hex(),
		Typ:   typ,
		Value: val,
	})
}

type contactRecord struct {
	ID    string
	Typ   string
	Value string
}

type LoginInfo struct {
	Times    int       // 登入次數
	LastTime time.Time // 最近一次登入時間
	Records  []loginRecord
}

func (li *LoginInfo) AddLoginRecord(data map[string]interface{}) {
	if len(li.Records) >= 5 {
		li.Records = li.Records[1:]
	}
	li.Times++
	li.LastTime = time.Now()
	li.Records = append(li.Records, loginRecord{
		Time: li.LastTime,
		Info: data,
	})
}

type loginRecord struct {
	Time time.Time
	Info map[string]interface{}
}

type User struct {
	ID            bson.ObjectId `bson:"_id"`
	Email         string
	EmailVerified bool `bson:"-"`
	PhoneNumber   string
	Pwd           string
	DisplayName   string
	Disabled      bool
	Service       []string
	Permission    string

	CompanyPerm []*UserCompanyPerm `bson:"lookupUCP,omitempty"`

	ContactInfo `bson:"contactinfo"`
	LoginInfo   `bson:"logininfo"`
	CommonDoc   `bson:"meta"`
}

func (u *User) GetState() string {
	if u.Times == 1 {
		return UserStateNew
	}
	return UserStateNormal
}

func (u *User) GetC() string {
	return userC
}
func (u *User) GetDoc() interface{} {
	u.ID = bson.NewObjectId()
	return u
}
func (u *User) GetID() bson.ObjectId {
	return u.ID
}
func (u *User) GetUpdateField() bson.M {
	return bson.M{
		"logininfo": u.LoginInfo,
	}
}
func (u *User) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *User) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *User) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}

func (u *User) GetMongoIndexes() []mgo.Index {
	return []mgo.Index{
		{
			Key:        []string{"email", "pwd"},
			Background: true, // can be used while index is being built
		},
		{
			Key:        []string{"email"},
			Background: true, // can be used while index is being built
		},
	}
}

func (u *User) GetAcc() string {
	return u.Email
}

func (u *User) GetName() string {
	return u.DisplayName
}

func (u *User) GetCompany() bson.ObjectId {
	return ""
}

func (u *User) GetUserCompanyPerm(unitCode string, isFake bool) (*UserCompanyPerm, error) {
	cid, err := GetCompanyId(unitCode, isFake)
	if err != nil {
		return nil, err
	}
	for _, c := range u.CompanyPerm {
		if c.CompanyID == cid {
			return c, nil
		}
	}
	return nil, errors.New("company not found")
}

func (u *User) GetPipeline(q bson.M) PipelineQryInter {
	return &CommonPipeline{
		q: q,
		pipeline: []bson.M{
			bson.M{"$match": q},
			bson.M{
				"$lookup": bson.M{
					"from":         userCompanyC,
					"localField":   "_id",
					"foreignField": "userid",
					"as":           "lookupUCP",
				},
			},
		},
	}
}

type SimpleUser struct {
	ID   bson.ObjectId
	Name string
}

const (
	UserCompanyPermEdit   = "edit"
	UserCompanyPermExpert = "expert"
)

type UserCompanyPerm struct {
	ID         bson.ObjectId `bson:"_id"`
	UserID     bson.ObjectId
	CompanyID  bson.ObjectId
	Permission string
	Note       string

	UserInfo []User `bson:"lookupUser,omitempty"`

	CommonDoc
}

func (u *UserCompanyPerm) GetC() string {
	return userCompanyC
}

func (u *UserCompanyPerm) GetDoc() interface{} {
	u.ID = bson.NewObjectId()
	return u
}

func (u *UserCompanyPerm) GetID() bson.ObjectId {
	return u.ID
}

func (u *UserCompanyPerm) GetUpdateField() bson.M {
	return bson.M{
		"permission": u.Permission,
		"note":       u.Note,
	}
}

func (u *UserCompanyPerm) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *UserCompanyPerm) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *UserCompanyPerm) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}

func (u *UserCompanyPerm) GetPipeline(q bson.M) PipelineQryInter {
	return &CommonPipeline{
		q: q,
		pipeline: []bson.M{
			bson.M{"$match": q},
			bson.M{
				"$lookup": bson.M{
					"from":         userC,
					"localField":   "userid",
					"foreignField": "_id",
					"as":           "lookupUser",
				},
			},
		},
	}
}
