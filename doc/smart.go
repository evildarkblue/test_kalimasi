package doc

import (
	"encoding/binary"
	"errors"
	"strconv"
	"kalimasi/util"

	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

const (
	smartC = "smart"
)

type MyFav struct {
	ID         bson.ObjectId `bson:"_id"`
	FavComName string
	VatNumber  string
	CompanyID  bson.ObjectId
	CommonDoc  `bson:"meta"`
}


func getVat2Id(unitCode string) (bson.ObjectId, error) {
	var b [12]byte
	i, err := strconv.ParseUint(unitCode, 10, 64)
	if err != nil {
		return "", errors.New("invalid unitCode")
	}
	bs := make([]byte, 8)
	binary.BigEndian.PutUint64(bs, i)
	for i = 0; i < 8; i++ {
		b[i] = bs[i]
	}
	return bson.ObjectId(b[:]), nil
}

func (d *MyFav) newID() bson.ObjectId {
	id, err := getVat2Id(d.VatNumber)
	if err != nil {
		panic(err)
	}
	return id
}

func (u *MyFav) GetID() bson.ObjectId {
	if !u.ID.Valid() {
		u.ID = u.newID()
	}
	return u.ID
}

func (r *MyFav) GetC() string {
	if !r.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(r.CompanyID.Hex(),smartC)
}

func (u *MyFav) GetDoc() interface{} {
	if !u.ID.Valid() {
		u.ID = u.newID()
	}
	return u
}

func (u *MyFav) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *MyFav) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *MyFav) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}

func (u *MyFav) GetUpdateField() bson.M {
	return bson.M{
		"favcomname": u.FavComName,
	}
}