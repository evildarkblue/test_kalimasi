package doc

import (
	"kalimasi/util"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

const (
	payRecordC = "payRecord"
)

type PayRecord struct {
	ID            bson.ObjectId `bson:"_id"`
	Date          time.Time
	VoucherNumber string
	Name          string // 付款項目
	Display       string // 顯示中文名稱
	Method        string // 付款方式
	DisplayMethod string // 付款方式顯示
	Amount        int    // 付款金額
	Desc          string // 備註

	CompanyID bson.ObjectId
	MemberId  *bson.ObjectId // 不一定有會員付款

	DebitAccTerm  *MiniAccount
	CreditAccTerm *MiniAccount

	CommonDoc `bson:"meta"`
}

func (u *PayRecord) GetDoc() interface{} {
	if !u.ID.Valid() {
		u.ID = u.newID()
	}
	return u
}

func (u *PayRecord) GetC() string {
	if !u.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(u.CompanyID.Hex(), payRecordC)
}

func (u *PayRecord) GetID() bson.ObjectId {
	return u.ID
}

func (u *PayRecord) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *PayRecord) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *PayRecord) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}

// serialDoc interface

func (u *PayRecord) GetCompany() bson.ObjectId {
	return u.CompanyID
}

func (u *PayRecord) GetDate() time.Time {
	return u.Date
}

func (u *PayRecord) GetSerialType() string {
	return TypeSerialVN
}
