package doc

import (
	"fmt"
	"kalimasi/util"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

const (
	accountC = "account"

	TypeAccountDebit  = "debit"
	TypeAccountCredit = "credit"
)

type MiniAccount struct {
	ID     bson.ObjectId `json:"id"`
	Code   string        `json:"code"`
	Name   string        `json:"name"`
	Desc   string        `json:"desc"`
	Amount int           `json:"amount"`
}

func (u *MiniAccount) GetUpdateTxnOp(data bson.D, companyID bson.ObjectId) txn.Op {
	acc := Account{CompanyID: companyID}
	fmt.Println(acc.GetC(), u.ID, data)
	return txn.Op{
		C:      acc.GetC(),
		Id:     u.ID,
		Assert: txn.DocExists,
		Update: bson.D{{Name: "$set", Value: data}},
	}
}

func (u *MiniAccount) GetDelTxnOp(companyID bson.ObjectId) txn.Op {
	acc := Account{CompanyID: companyID}
	return txn.Op{
		C:      acc.GetC(),
		Id:     u.ID,
		Assert: txn.DocExists,
		Remove: true,
	}
}

type Account struct {
	ID        bson.ObjectId `bson:"_id"`
	CompanyID bson.ObjectId
	DateTime  time.Time
	Year      int

	BudgetID *bson.ObjectId // budgetStatementID

	VoucherNumber string
	Summary       string
	Amount        int
	Typ           string // 借(+) / 貸(-)
	Code          string
	Name          string
	IsOpening     bool // 是否為開帳紀錄

	CommonDoc `bson:"meta"`
}

func (u *Account) GetMiniAccount() *MiniAccount {
	return &MiniAccount{
		ID:     u.ID,
		Code:   u.Code,
		Name:   u.Name,
		Desc:   u.Summary,
		Amount: u.Amount,
	}
}

func (u *Account) SetCompany(c bson.ObjectId) {
	u.CompanyID = c
}

func (u *Account) GetDoc() interface{} {
	if !u.ID.Valid() {
		u.ID = u.newID()
	}
	return u
}

func (u *Account) GetC() string {
	if !u.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(u.CompanyID.Hex(), accountC)
}

func (u *Account) GetID() bson.ObjectId {
	return u.ID
}

func (u *Account) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *Account) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *Account) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}

// 取得決算pipeline
func (u *Account) GetFinalPipeline(q bson.M) PipelineQryInter {
	return &CommonPipeline{
		q: q,
		pipeline: []bson.M{
			bson.M{"$match": q},
			bson.M{
				"$group": bson.M{
					"_id":       bson.M{"code": "$code", "typ": "$typ"},
					"subAmount": bson.M{"$sum": "$amount"},
					"name":      bson.M{"$first": "$name"},
				},
			},
		},
	}
}

type GroupAccount struct {
	ID struct {
		Code string
		Typ  string
	} `bson:"_id"`
	Amount int `bson:"subAmount"`
	Name   string
	Account
}

func (b *Account) GetMongoIndexes() []mgo.Index {
	return []mgo.Index{
		{
			Key:        []string{"companyid", "datetime"},
			Background: true, // can be used while index is being built
		},
		{
			Key:        []string{"bsid", "datetime"},
			Background: true, // can be used while index is being built
		},
	}
}

type AccountJoinReceipt struct {
	Account `bson:",inline"`
	Receipt *Receipt `bson:"lookupReceipt,omitempty"`
}

// 取得決算pipeline
func (u *AccountJoinReceipt) GetPipeline(q bson.M) PipelineQryInter {
	return &CommonPipeline{
		q: q,
		pipeline: []bson.M{
			{"$match": q},
			{
				"$lookup": bson.M{
					"from":         u.Receipt.GetC(),
					"localField":   "vouchernumber",
					"foreignField": "vouchernumber",
					"as":           "lookupReceipt",
				},
			},
			{
				"$unwind": bson.M{
					"path":                       "$lookupReceipt",
					"preserveNullAndEmptyArrays": false,
				},
			},
		},
	}
}
