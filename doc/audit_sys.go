package doc

import (
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

const (
	auditSysC = "auditSys"
)

type AuditSys struct {
	ID          bson.ObjectId `bson:"_id"`
	State       string
	Typ         string
	Summary     map[string]interface{}
	SerialJSON  []byte
	Checksum    string
	Applicant   SimpleUser  // 申請者
	Assessor    *SimpleUser // 待審核者
	UpdateDate  time.Time
	Records     []auditRecord
	Process     []AuditProcess //審核流程
	CurrProcess uint16         //目前審核
	Method      string         // POST || PUT || DELETE
	ActURL      string

	CommonDoc `bson:"meta"`
}

func (a *AuditSys) CanApply(id, perm string) bool {
	if a.CurrProcess >= uint16(len(a.Process)) {
		return false
	}
	cp := a.Process[a.CurrProcess]
	if cp.Typ != AuditProcTypPerm && cp.Typ != AuditProcTypAcc {
		return false
	} else if cp.Typ == AuditProcTypPerm && perm != cp.Value {
		return false
	} else if cp.Typ == AuditProcTypAcc && id != cp.Value {
		return false
	}
	return true
}

func (a *AuditSys) AddRecord(su SimpleUser, r string, msg string) []auditRecord {
	if r != AuditStateReject && r != AuditStatePass {
		return a.Records
	}
	return append(a.Records, auditRecord{
		UserID:     su.ID,
		Name:       su.Name,
		Result:     r,
		Message:    msg,
		CreateDate: time.Now(),
	})
}

func (a *AuditSys) GetC() string {
	return auditC
}
func (a *AuditSys) GetDoc() interface{} {
	if !a.ID.Valid() {
		a.ID = bson.NewObjectId()
	}
	return a
}
func (a *AuditSys) GetID() bson.ObjectId {
	return a.ID
}

func (a *AuditSys) GetUpdateField() bson.M {
	return bson.M{
		"records":     a.Records,
		"state":       a.State,
		"currprocess": a.CurrProcess,
		"updateDate":  time.Now(),
	}
}

func (a *AuditSys) GetSaveTxnOp(lu LogUser) txn.Op {
	return a.CommonDoc.getSaveTxnOp(a, lu)
}

func (a *AuditSys) GetUpdateTxnOp(data bson.D) txn.Op {
	return a.CommonDoc.getUpdateTxnOp(a, data)
}

func (a *AuditSys) GetDelTxnOp() txn.Op {
	return a.CommonDoc.getDelTxnOp(a)
}

func (a *AuditSys) GetMongoIndexes() []mgo.Index {
	return []mgo.Index{
		{
			Key:        []string{"state", "typ"},
			Background: true, // can be used while index is being built
		},
		{
			Key:        []string{"typ"},
			Background: true, // can be used while index is being built
		},
		{
			Key:        []string{"state"},
			Background: true, // can be used while index is being built
		},
	}
}
