package doc

import (
	"errors"
	"reflect"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

type DocInter interface {
	GetC() string
	GetDoc() interface{}
	GetID() bson.ObjectId
	GetUpdateField() bson.M

	GetSaveTxnOp(lu LogUser) txn.Op
	GetUpdateTxnOp(data bson.D) txn.Op
	GetDelTxnOp() txn.Op

	GetMongoIndexes() []mgo.Index
	SetCreator(u LogUser)
	SetCompany(u bson.ObjectId)
}

type ListDoc interface {
	GetID() string
	GetC() string
	GetList(slice interface{}) []ListDoc
	BeforeSave() error
}

type emptyDoc struct{}

func (u *emptyDoc) GetC() string {
	panic("not support")
}
func (u *emptyDoc) GetDoc() interface{} {
	panic("not support")
}
func (u *emptyDoc) GetID() bson.ObjectId {
	panic("not support")
}
func (u *emptyDoc) GetUpdateField() bson.M {
	panic("not support")
}

func (u *emptyDoc) GetSaveTxnOp(lu LogUser) txn.Op {
	panic("not support")
}
func (u *emptyDoc) GetUpdateTxnOp(data bson.D) txn.Op {
	panic("not support")
}
func (u *emptyDoc) GetDelTxnOp() txn.Op {
	panic("not support")
}

func (u *emptyDoc) GetMongoIndexes() []mgo.Index {
	panic("not support create")
}
func (u *emptyDoc) SetCreator(user LogUser) {
	panic("not support")
}
func (u *emptyDoc) SetCompany(user bson.ObjectId) {
	panic("not support")
}

type CommonDoc struct {
	Creator    string
	CreateAcc  string
	CreateTime time.Time
}

func (u *CommonDoc) newID() bson.ObjectId {
	return bson.NewObjectId()
}

func (u *CommonDoc) SetCompany(c bson.ObjectId) {
}

func (u *CommonDoc) SetCreator(lu LogUser) {
	u.Creator = lu.GetName()
	u.CreateAcc = lu.GetAcc()
	u.CreateTime = time.Now()
}

func (u *CommonDoc) GetUpdateField() bson.M {
	return nil
}

func (u *CommonDoc) GetMongoIndexes() []mgo.Index {
	return nil
}

func (u *CommonDoc) getSaveTxnOp(doc DocInter, lu LogUser) txn.Op {
	u.SetCreator(lu)
	doc.SetCompany(lu.GetCompany())
	d := doc.GetDoc()
	return txn.Op{
		C:      doc.GetC(),
		Id:     doc.GetID(),
		Assert: txn.DocMissing,
		Insert: d,
	}
}

func (u *CommonDoc) getUpdateTxnOp(doc DocInter, data bson.D) txn.Op {
	return txn.Op{
		C:      doc.GetC(),
		Id:     doc.GetID(),
		Assert: txn.DocExists,
		Update: bson.D{{Name: "$set", Value: data}}, 
	}
}

func (u *CommonDoc) getDelTxnOp(doc DocInter) txn.Op {
	return txn.Op{
		C:      doc.GetC(),
		Id:     doc.GetID(),
		Assert: txn.DocExists,
		Remove: true,
	}
}

func (u *CommonDoc) GetC() string {
	panic("must override")
}

func Format(inter interface{}, f func(i interface{}) map[string]interface{}) (interface{}, int) {
	ik := reflect.TypeOf(inter).Kind()
	if ik == reflect.Ptr {
		return f(inter), 1
	}
	if ik != reflect.Slice {
		return nil, 0
	}
	v := reflect.ValueOf(inter)
	l := v.Len()
	ret := make([]map[string]interface{}, l)
	for i := 0; i < l; i++ {
		ret[i] = f(v.Index(i).Interface())
	}
	return ret, l
}

func GetObjectID(id interface{}) (bson.ObjectId, error) {
	var myID bson.ObjectId
	switch dtype := reflect.TypeOf(id).String(); dtype {
	case "string":
		str := id.(string)
		if str == "" || !bson.IsObjectIdHex(str) {
			return "", errors.New("id is error: " + str)
		}
		myID = bson.ObjectIdHex(str)
	case "bson.ObjectId":
		myID = id.(bson.ObjectId)
	default:
		return "", errors.New("not support type: " + dtype)
	}
	return myID, nil
}

type PipelineQryInter interface {
	GetQ() bson.M
	GetPipeline() []bson.M
}

type CommonPipeline struct {
	q        bson.M
	pipeline []bson.M
}

func (u *CommonPipeline) GetQ() bson.M {
	return u.q
}

func (u *CommonPipeline) GetPipeline() []bson.M {
	return u.pipeline
}
