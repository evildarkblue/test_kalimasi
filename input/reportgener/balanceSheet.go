package input

import (
	"encoding/json"
	"errors"
	"kalimasi/doc"
	"kalimasi/model/reportgener"

	"github.com/globalsign/mgo/bson"
)

type PutSettingBalanceSheet struct {
	ID                  bson.ObjectId
	SettingBalanceSheet *reportgener.BalanceSheet
}

func (pb *PutSettingBalanceSheet) Validate() error {
	var err error
	for _, p := range pb.SettingBalanceSheet.Assets {

		if p.Name == "" {
			return errors.New("invalid name")
		}

		if p.Sub != nil {
			for _, psub := range p.Sub {
				if psub.Name == "" {
					return errors.New("invalid name")
				}
				if psub.SpecialSub != nil {
					err = psub.Validate()
					if err != nil {
						return err
					}
				}
			}
		}

	}
	return err
}

func (pi *PutSettingBalanceSheet) GetReportDoc() *doc.Report {

	b, _ := json.Marshal(pi.SettingBalanceSheet)
	data := string(b)

	r := &doc.Report{
		JsonSetting: data,
	}

	return r
}
