package input

import (
	"errors"
	"time"

	"kalimasi/doc"
	"kalimasi/util"

	"github.com/globalsign/mgo/bson"
)

type CreateRebate struct {
	AccType string    `json:"accType"`
	Date    time.Time `json:"rDate"`
	Amount  int
	TaxInfo Tax `json:"tax"`

	ReceiptID *bson.ObjectId `json:"receiptId"`

	DebitAccTerm  []*InputAccTerm `json:"debitAccTermList"`
	CreditAccTerm []*InputAccTerm `json:"creditAccTermList"`
}

func (bi *CreateRebate) GetRebateDoc() *doc.Rebate {
	r := &doc.Rebate{
		DateTime:  bi.Date,
		TypeAcc:   bi.AccType,
		Amount:    bi.Amount,
		Year:      bi.Date.Year() - 1911,
		ReceiptID: bi.ReceiptID,
		TaxInfo: doc.Tax{
			Typ:    bi.TaxInfo.Typ,
			Amount: bi.TaxInfo.Amount,
		},
	}
	return r
}

func (cu *CreateRebate) Validate() error {
	if !util.IsStrInList(cu.AccType, allowAccType...) {
		return errors.New("invalid account type")
	}

	if cu.Amount < 0 {
		return errors.New("amount can not be negative")
	}
	if len(cu.CreditAccTerm) == 0 {
		return errors.New("creditAccTerm length can not be empty")
	}

	if len(cu.DebitAccTerm) == 0 {
		return errors.New("creditAccTerm length can not be empty")
	}

	if err := cu.TaxInfo.Validate(); err != nil {
		return err
	}

	return nil
}

type PutRebate struct {
	CreateRebate
	ID            bson.ObjectId
	DelCreditList []bson.ObjectId `json:"delCreditAccTermList"`
	DelDebitList  []bson.ObjectId `json:"delDebitAccTermList"`
}

func (pb *PutRebate) Validate() error {
	err := pb.CreateRebate.Validate()
	if err != nil {
		return err
	}
	if !pb.ID.Valid() {
		return errors.New("invalid receipt id")
	}
	return nil
}

type QueryRebate struct {
	Code string
}

func (qb *QueryRebate) Validate() error {
	return nil
}

func (qb *QueryRebate) GetMgoQuery() bson.M {
	q := bson.M{}
	if qb.Code != "" {
		q["$or"] = []bson.M{
			{"debitaccterm": bson.M{"$elemMatch": bson.M{"code": qb.Code}}},
			{"creditaccterm": bson.M{"$elemMatch": bson.M{"code": qb.Code}}},
		}
	}
	return q
}
