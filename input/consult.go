package input

import (
	"errors"
	"kalimasi/doc"
	"kalimasi/util"

	"github.com/globalsign/mgo/bson"
)

type CreateConsult struct {
	doc.Consult
}

func (ccs *CreateConsult) Validate() error {
	if !util.IsStrInList(ccs.Contact.CtType, "home", "office", "mobile") {
		return errors.New("Contact.Ctype enum [home office mobile]")
	}
	return nil
}

func (ccs *CreateConsult) ToDoc() doc.DocInter {
	return ccs
}

func (ccs *CreateConsult) HasAuditID() bool {
	return false
}

func (ccs *CreateConsult) GetAuditUpdateDoc(companyID bson.ObjectId) *doc.AuditUpdateDoc {
	return nil
}

func (ccs *CreateConsult) GetAuditType() string {
	return "consult-new"
}

func (ccs *CreateConsult) GetAuditSummary() map[string]interface{} {
	return map[string]interface{}{
		"account": ccs.Account,
		"name":    ccs.Name,
		"contact": ccs.Contact,
	}
}

type CreateInviteMail struct {
	doc.InviteMail
}

func (cim *CreateInviteMail) Validate() error {
	return nil
}

func (cim *CreateInviteMail) ToDoc() doc.DocInter {
	return cim
}
