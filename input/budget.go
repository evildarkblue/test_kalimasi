package input

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	"kalimasi/doc"
	"kalimasi/util"

	"github.com/globalsign/mgo/bson"
)

type CreateBudget struct {
	Year      int              `json:"year"`
	Title     string           `json:"title"`
	Typ       string           `json:"type"`
	StartDate time.Time        `json:"startDate"`
	EndDate   time.Time        `json:"endDate"`
	Income    []doc.BudgetItem `json:"income,omitempty"`
	Expenses  []doc.BudgetItem `json:"expenses,omitempty"`
	Amount    int              `json:"amount"`
	Project   []string         `json:"project,omitempty"`
	ParentID  string           `json:"parentId"`
}

var (
	allowBudgetType = []string{
		doc.BudgetTypYear,
		doc.BudgetTypProject,
	}
)

func (bi *CreateBudget) ToDoc() *doc.BudgetStatement {
	bs := &doc.BudgetStatement{
		Title:     bi.Title,
		Year:      bi.Year,
		Typ:       bi.Typ,
		Income:    bi.Income,
		Expenses:  bi.Expenses,
		StartDate: bi.StartDate,
		EndDate:   bi.EndDate,
		Amount:    bi.Amount,
	}
	if bson.IsObjectIdHex(bi.ParentID) {
		oid := bson.ObjectIdHex(bi.ParentID)
		bs.ParentID = &oid
	} else {
		bs.ParentID = nil
	}
	return bs
}

func (cu *CreateBudget) Validate() error {
	if !util.IsStrInList(cu.Typ, allowBudgetType...) {
		return errors.New("invalid budget type")
	}
	return nil
}

func (cu *CreateBudget) GetAuditType() string {
	return fmt.Sprintf("budget-%s", cu.Typ)
}

func (cu *CreateBudget) HasAuditID() bool {
	return false
}

func (cu *CreateBudget) GetAuditUpdateDoc(companyID bson.ObjectId) *doc.AuditUpdateDoc {
	return nil
}

func (cu *CreateBudget) GetAuditSummary() map[string]interface{} {
	incomeAmount, expensesAmount := 0, 0
	for _, b := range cu.Income {
		incomeAmount += b.Amount
	}
	for _, b := range cu.Expenses {
		expensesAmount += b.Amount
	}
	return map[string]interface{}{
		"year":           cu.Year,
		"name":           cu.Title,
		"incomeAmount":   incomeAmount,
		"expensesAmount": expensesAmount,
		"projectCount":   len(cu.Project),
	}
}

type PutBudget struct {
	CreateBudget
	ID         bson.ObjectId
	DelProject []string `json:"delProject"`
}

func (cu *PutBudget) HasAuditID() bool {
	return true
}

func (cu *PutBudget) GetAuditUpdateDoc(companyID bson.ObjectId) *doc.AuditUpdateDoc {
	return &doc.AuditUpdateDoc{
		DocInter: &doc.BudgetStatement{
			ID:        cu.ID,
			CompanyID: companyID,
		},
	}
}

func (pb *PutBudget) Validate() error {
	err := pb.CreateBudget.Validate()
	if err != nil {
		return err
	}
	if !pb.ID.Valid() {
		return errors.New("invalid budget id")
	}
	return nil
}

type QueryBudget struct {
	Year      string
	Typ       string
	Title     string
	CompanyID bson.ObjectId
}

func (qb *QueryBudget) Validate() error {
	if !qb.CompanyID.Valid() {
		return errors.New("invalid companyID")
	}
	if !util.IsStrInList(qb.Typ, allowBudgetType...) {
		return errors.New("invalid budget type")
	}
	return nil
}

func (qb *QueryBudget) GetMgoQuery() bson.M {
	q := bson.M{
		"companyid": qb.CompanyID,
	}
	if qb.Title != "" {
		q["title"] = bson.RegEx{Pattern: qb.Title, Options: "m"}
	}
	if qb.Typ != "" {
		q["typ"] = qb.Typ
	}
	if y, err := strconv.Atoi(qb.Year); err == nil {
		q["year"] = y
	}
	return q
}
