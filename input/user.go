package input

import (
	"context"
	"errors"
	"net"
	"net/http"

	"github.com/asaskevich/govalidator"

	"github.com/globalsign/mgo/bson"
)

type ReqUser struct {
	ID        string
	Name      string
	Account   string
	CompanyID bson.ObjectId
	Perm      string
}

const ctxKeyReqUser = "userInfo"

func (ru *ReqUser) GetName() string {
	return ru.Name
}

func (ru *ReqUser) GetAcc() string {
	return ru.Account
}

func (ru *ReqUser) GetCompany() bson.ObjectId {
	return ru.CompanyID
}

func (ru *ReqUser) SaveToContext(r *http.Request) *http.Request {
	ctx := context.WithValue(r.Context(), ctxKeyReqUser, ru)
	return r.WithContext(ctx)
}

func GetUserInfo(req *http.Request) *ReqUser {
	ctx := req.Context()
	reqID := ctx.Value(ctxKeyReqUser)

	if ret, ok := reqID.(*ReqUser); ok {
		return ret
	}
	return nil
}

func GetGuestInfo(req *http.Request) *ReqUser {
	ip, _, _ := net.SplitHostPort(req.RemoteAddr)
	return &ReqUser{
		Name:    ip,
		Account: "guest",
	}
}

type LoginWithCompany struct {
	BasicLogin
	Company       string
	IsFakeCompany bool
}

func (qb *LoginWithCompany) Validate() error {
	if err := qb.BasicLogin.Validate(); err != nil {
		return err
	}

	if len(qb.Company) != 8 {
		return errors.New("invalid company")
	}
	return nil
}

type BasicLogin struct {
	Account string
	Pwd     string
	Service string

	ClientInfo map[string]interface{}
}

func (qb *BasicLogin) Validate() error {
	if !govalidator.IsEmail(qb.Account) {
		return errors.New("invalid account")
	}
	if qb.Pwd == "" {
		return errors.New("miss password")
	}
	return nil
}

type SignUp struct {
	Account string
	Pwd     string
	Name    string

	Contact struct {
		CtType string // enum: ["home", "office", "mobile"]
		Value  string
	}
}

func (qb *SignUp) Validate() error {
	if !govalidator.IsEmail(qb.Account) {
		return errors.New("invalid account")
	}
	if qb.Pwd == "" {
		return errors.New("miss password")
	}

	if qb.Name == "" {
		return errors.New("miss name")
	}

	if qb.Contact.CtType != "home" && qb.Contact.CtType != "office" && qb.Contact.CtType != "mobile" {
		return errors.New("not valid contact type")
	}

	if qb.Contact.Value == "" {
		return errors.New("miss contact value")
	}

	return nil
}
