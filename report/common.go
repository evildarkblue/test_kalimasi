package report

import (
	"errors"
	"io"
)

type ReportInter interface {
	PDF(w io.Writer) error
	Json(w io.Writer) error
	TXT(w io.Writer) error
}

type commonReport struct {
}

func (c *commonReport) TXT(w io.Writer) error {
	return errors.New("not support txt type")
}

func (c *commonReport) PDF(w io.Writer) error {
	return errors.New("not support pdf type")
}

func (c *commonReport) Json(w io.Writer) error {
	return errors.New("not support json type")
}
