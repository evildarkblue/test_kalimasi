// 損益表
package report

import (
	"encoding/json"
	"fmt"
	"io"
	"kalimasi/doc"
	"kalimasi/report/pdf"
	"time"
)

func NewIncomeStatement(tc TaxCal) *incomeStatement {
	return &incomeStatement{
		TaxCal:         tc,
		Costs:          []*doc.TaxItem{},
		NonIncomes:     []*doc.TaxItem{},
		NonExpenditure: []*doc.TaxItem{},
	}
}

type incomeStatement struct {
	TaxCal         `json:",inline"`
	Costs          []*doc.TaxItem `json:"costs"`       // 銷貨成本
	NonIncomes     []*doc.TaxItem `json:"nonIncomes"`  // 非營業收入
	NonExpenditure []*doc.TaxItem `json:"nonExpenses"` // 非營業支出
}

func (bb *incomeStatement) AddNonIncomes(bbi *doc.TaxItem) {
	bb.NonIncomes = append(bb.NonIncomes, bbi)
}

func (bb *incomeStatement) AddNonExpenditure(bbi *doc.TaxItem) {
	bb.NonExpenditure = append(bb.NonExpenditure, bbi)
}

func (bb *incomeStatement) AddCost(bbi *doc.TaxItem) {
	bb.Costs = append(bb.Costs, bbi)
}

func (bb *incomeStatement) getTableColumns(preTerm string, serial int, i *doc.TaxItem) []*pdf.TableColumn {
	newTerm, line := autoEnter(i.Term, 15)
	fl := float64(line)
	rowHeight := (fl-1)*0.5*20 + 20

	return []*pdf.TableColumn{
		pdf.GetTableColumn(20, rowHeight, singleAlignLeft, "  "+newTerm),
		pdf.GetTableColumn(80, rowHeight, singleAlignRight, currencyNoMinusFormat(&i.Amount)),
		pdf.GetTableColumn(75, rowHeight, singleAlignRight, ""),
		pdf.GetTableColumn(75, rowHeight, singleAlignRight, ""),
		pdf.GetTableColumn(90, rowHeight, singleAlignCenter, ""),
	}
}
func (bb *incomeStatement) Before(p *pdf.PDF) {
	style := bb.Style
	// page one report

	p.Text("負責人：　　　　　　　(蓋章)", style.GetContent(), pdf.AlignLeft)
	p.Text("　主辦會計：　　　　　　　(蓋章)", style.GetContent(), pdf.AlignLeft)
	p.Text("　製表：　　　　　　　(蓋章)", style.GetContent(), pdf.AlignLeft)

}

func (bb *incomeStatement) After(p *pdf.PDF) {
	style := bb.Style
	// page one report
	p.Text(bb.OrgName, style.GetTitle(), pdf.AlignCenter)
	p.Br(20)

	p.Text("損益表", style.GetSubTitle(), pdf.AlignCenter)
	p.Br(18)
	now := time.Now()
	p.Text(fmt.Sprintf("製表日期：%d-%d-%d",
		now.Year()-1911, now.Month(), now.Day()), style.GetContent(), pdf.AlignLeft)
	p.Text(fmt.Sprintf("期間：%d年 %d月 %d日起至 %d年 %d月 %d日止",
		bb.StartTime.Year()-1911, bb.StartTime.Month(), bb.StartTime.Day(),
		bb.EndTime.Year()-1911, bb.EndTime.Month(), bb.EndTime.Day()),
		style.GetContent(), pdf.AlignCenter)
	p.Text(fmt.Sprintf("頁次：%d", p.GetPage()), style.GetContent(), pdf.AlignRight)
	p.Br(20)
}

func (bb *incomeStatement) Json(w io.Writer) error {
	return json.NewEncoder(w).Encode(bb)
}

func (bb *incomeStatement) PDF(w io.Writer) error {
	style := bb.Style
	p := pdf.GetA4PDF(bb.Font, 20, 20, 20, 20)
	p.AddPage(bb)

	nt := pdf.GetNormalTable()
	ha := bb.getHeaderAry()
	for _, h := range ha {
		nt.AddHeader(h)
	}
	total, _ := calTotal(bb.Incomes)

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "營業收入"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})
	for i, v := range bb.Incomes {
		nt.AddRow(bb.getTableColumns("41", i+1, v))
	}
	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "營業收入淨額"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, currencyFormat(&total)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	total51, _ := calTotal(bb.Costs)

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, ""),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "銷貨成本"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})
	for i, v := range bb.Costs {
		nt.AddRow(bb.getTableColumns("51", i+1, v))
	}
	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "銷貨成本(買賣業)"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, currencyFormat(&total51)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "銷貨總成本"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, currencyFormat(&total51)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "營業成本"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, currencyFormat(&total51)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "成本率"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, fmt.Sprintf("%.2f%%", float64(total51)/float64(total)*100)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	gross := total - total51
	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "營業毛利"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, currencyFormat(&gross)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "毛利率"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, fmt.Sprintf("%.2f%%", float64(gross)/float64(total)*100)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	total6, _ := calTotal(bb.Expenditure)

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, ""),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "營業費用"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})
	for i, v := range bb.Expenditure {
		nt.AddRow(bb.getTableColumns("6", i+1, v))
	}

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "營業費用總額"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, currencyFormat(&total6)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "費用率"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, fmt.Sprintf("%.2f%%", float64(total6)/float64(total)*100)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})
	oi := gross - total6
	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "營業利益"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, currencyFormat(&oi)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "營業利益率"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, fmt.Sprintf("%.2f%%", float64(oi)/float64(total)*100)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, ""),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	total7, _ := calTotal(bb.NonIncomes)
	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "非營業收入"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})
	for i, v := range bb.NonIncomes {
		nt.AddRow(bb.getTableColumns("7", i+1, v))
	}

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "非營業收益總額"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, currencyFormat(&total7)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, ""),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	total8, _ := calTotal(bb.NonExpenditure)
	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "非營業損失及費用"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})
	for i, v := range bb.NonExpenditure {
		nt.AddRow(bb.getTableColumns("06", i+1, v))
	}

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "非營業損失及費用總額"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, currencyFormat(&total8)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, ""),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	subTotal := oi + total7 - total8
	//subAdjustTotal := totalAjust - totalAjust06
	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "本期損益"),
		pdf.GetTableColumn(20, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, currencyFormat(&subTotal)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "純益率"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, fmt.Sprintf("%.2f%%", float64(subTotal)/float64(total)*100)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "課稅所得額"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, currencyFormat(&subTotal)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "稅後淨利"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, ""),
		pdf.GetTableColumn(80, 20, singleAlignRight, ""),
		pdf.GetTableColumn(75, 20, singleAlignRight, currencyFormat(&subTotal)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})

	nt.Draw(&p, style, bb)
	p.Br(20)
	bb.Before(&p)
	return p.Write(w)
}

func (bb *incomeStatement) getHeaderAry() []pdf.TableHeaderColumn {
	var ha []pdf.TableHeaderColumn
	ha = append(ha,
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"會計項目"},
				Width:  160,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"金額"},
				Width:  100,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"小計"},
				Width:  100,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"總計"},
				Width:  120,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"同業標準率"},
				Width:  70,
				Height: 20,
			}},
	)

	return ha
}
