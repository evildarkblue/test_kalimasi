/*
 * 資產負債表表
 */
package report

import (
	"encoding/json"
	"fmt"
	"io"
	"kalimasi/util"
	"time"

	"kalimasi/report/pdf"
)

type BalanceSheetItem struct {
	Name1   string
	Amount1 int
	IsHead1 bool
	Code1   string
	Desc1   string
	Name2   string
	Amount2 int
	IsHead2 bool
	Code2   string
	Desc2   string
}

func (bsi *BalanceSheetItem) Len() int {
	l := 0
	if bsi.Name1 != "" {
		l++
	}
	if bsi.Name2 != "" {
		l++
	}
	return l
}

type BalanceSheetRow struct {
	Asset *BalanceSheetItem
	Liab  *BalanceSheetItem
}

func (bsr *BalanceSheetRow) getHeight() float64 {
	l := bsr.Asset.Len()
	if bsr.Liab.Len() > l {
		l = bsr.Liab.Len()
	}
	return 20.0 * float64(l)
}

func (bsr *BalanceSheetRow) liabName() []string {
	if bsr.Liab.Len() == 0 {
		return []string{""}
	}
	if bsr.Liab.Len() == 1 {
		return []string{bsr.Liab.Name1}
	}
	return []string{bsr.Liab.Name1, bsr.Liab.Name2}
}

func (bsr *BalanceSheetRow) liabAmount() []string {
	if bsr.Liab.Len() == 0 {
		return []string{""}
	}
	if bsr.Liab.Len() == 1 {
		if bsr.Liab.Amount1 < 0 {
			amount := bsr.Liab.Amount1 * -1
			return []string{fmt.Sprintf("(%s)", currencyFormat(&amount))}
		}
		return []string{currencyFormat(&bsr.Liab.Amount1)}
	}
	amount1Str, amount2Str := "", ""
	if bsr.Liab.Amount1 < 0 {
		amount := bsr.Liab.Amount1 * -1
		amount1Str = fmt.Sprintf("(%s)", currencyFormat(&amount))
	} else {
		amount1Str = currencyFormat(&bsr.Liab.Amount1)
	}

	if bsr.Liab.Amount1 < 0 {
		amount := bsr.Liab.Amount2 * -1
		amount2Str = fmt.Sprintf("(%s)", currencyFormat(&amount))
	} else {
		amount2Str = currencyFormat(&bsr.Liab.Amount2)
	}
	return []string{amount1Str, amount2Str}
}

func (bsr *BalanceSheetRow) getAssetAlign() []int {
	if bsr.Asset.IsHead1 {
		return []int{pdf.AlignLeft}
	}
	return []int{pdf.AlignCenter}
}

func (bsr *BalanceSheetRow) getLiabAlign() []int {
	if bsr.Liab.Len() == 0 {
		return []int{pdf.AlignLeft}
	}
	if bsr.Liab.Len() == 1 {
		if bsr.Liab.IsHead1 {
			return []int{pdf.AlignLeft}
		}
		return []int{pdf.AlignCenter}
	}
	result := make([]int, 2)
	if bsr.Liab.IsHead1 {
		result[0] = pdf.AlignLeft
	} else {
		result[0] = pdf.AlignCenter
	}
	if bsr.Liab.IsHead2 {
		result[1] = pdf.AlignLeft
	} else {
		result[1] = pdf.AlignCenter
	}
	return result
}

func (bsr *BalanceSheetRow) assetName() string {
	if bsr.Asset.Len() == 0 {
		return ""
	}
	if bsr.Asset.Len() == 1 {
		return bsr.Asset.Name1
	}
	return util.StrAppend(bsr.Asset.Name1, "  減\n", bsr.Asset.Name2)
}

func (bsr *BalanceSheetRow) assetAmount() string {
	if bsr.Asset.Len() == 0 {
		return ""
	}
	if bsr.Asset.Len() == 1 {
		return currencyFormat(&bsr.Asset.Amount1)
	}
	sub := bsr.Asset.Amount1 - bsr.Asset.Amount2
	return util.StrAppend(
		currencyFormat(&bsr.Asset.Amount1),
		"\n(", currencyFormat(&bsr.Asset.Amount2), ")  ",
		currencyFormat(&sub),
	)
}

type BalanceSheet struct {
	Title    string
	DateTime time.Time
	Item     []*BalanceSheetRow

	Style reportStyle       `json:"-"`
	Font  map[string]string `json:"-"`

	commonReport
}

func (bb *BalanceSheet) AddItem(bbi *BalanceSheetRow) {
	bb.Item = append(bb.Item, bbi)
}

func (bb *BalanceSheet) AddItems(bbi []*BalanceSheetRow) {
	bb.Item = append(bb.Item, bbi...)
}

func (bb *BalanceSheet) getDate() string {
	const format = "中華民國%d年%d月%d日"
	return fmt.Sprintf(format,
		bb.DateTime.Year()-1911, bb.DateTime.Month(), bb.DateTime.Day(),
	)
}

func (bb *BalanceSheet) Json(w io.Writer) error {
	return json.NewEncoder(w).Encode(bb)
}

func (bb *BalanceSheet) PDF(w io.Writer) error {
	const subtitle = "資產負債表"
	p := pdf.GetA4PDF(bb.Font, 20, 20, 20, 20)
	p.AddPage()
	style := bb.Style
	// page one report
	p.Text(bb.Title, style.GetTitle(), pdf.AlignCenter)
	p.Br(25)
	p.Text(subtitle, style.GetSubTitle(), pdf.AlignCenter)
	p.Br(25)
	p.Text(bb.getDate(), style.GetSubTitle(), pdf.AlignRight)
	p.Br(25)
	p.Line(2)
	p.Br(5)

	nt := pdf.GetNormalTable()
	ha := bb.getHeaderAry()

	for _, h := range ha {
		nt.AddHeader(h)
	}
	for _, i := range bb.Item {
		h := i.getHeight()

		nt.AddRow([]*pdf.TableColumn{
			pdf.GetTableColumn(100, h, i.getAssetAlign(), i.assetName()),
			pdf.GetTableColumn(100, h, singleAlignRight, i.assetAmount()),
			pdf.GetTableColumn(100, h, i.getLiabAlign(), i.liabName()...),
			pdf.GetTableColumn(100, h, singleAlignRight, i.liabAmount()...),
		})
	}
	nt.Draw(&p, style)

	p.Br(5)
	p.Text("(以下請核章)", style.GetSubTitle(), pdf.AlignLeft)
	p.Br(20)
	p.RectFillColor("團體負責人：", style.TextBlock(12), 400, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.RectFillColor("秘書長：", style.TextBlock(12), 100, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.Br(30)
	p.RectFillColor("會計：", style.TextBlock(12), 400, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.RectFillColor("製表：", style.TextBlock(12), 100, 20, pdf.AlignLeft, pdf.ValignMiddle)
	return p.Write(w)
}

func (bb *BalanceSheet) getHeaderAry() []pdf.TableHeaderColumn {
	var ha []pdf.TableHeaderColumn
	ha = append(ha,
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"資產"},
				Width:  250,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"負債、基金暨餘絀"},
				Width:  250,
				Height: 20,
			}},
	)
	ha[0].AddSub("科目", 150, 20)
	ha[0].AddSub("金額", 100, 20)

	ha[1].AddSub("科目", 150, 20)
	ha[1].AddSub("金額", 100, 20)

	return ha
}
