package report

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"kalimasi/doc"
	"kalimasi/util"
	"strconv"
	"time"
)

type enterIncomeItem struct {
	FormatCode      string // 格式代號
	YearMonth       string // 憑證年月
	BuyerVATnumber  string // 買方統編
	SalesVATnumber  string // 賣方統編
	Number          string // 發票號碼
	Amount          int    // 銷售金額
	IsCollect       bool   // 是否為匯總
	CollectQuantity int    // 匯總張數
	TaxType         string
	Tax             int    // 營業稅
	Deduct          string // 扣抵代號
}

func (i *enterIncomeItem) isIncome() bool {
	return i.FormatCode[0] == '3'
}

const (
	_blank   = " "
	_newLine = "\r\n"

	taxAble = "1"
	taxFree = "3"
	taxZero = "2"
)

func (i *enterIncomeItem) txt(serial int, taxID string) (string, error) {
	result := util.StrAppend(
		i.FormatCode,
		taxID,
		fmt.Sprintf("%07d", serial),
		i.YearMonth,
		util.FixStrLen(i.BuyerVATnumber, 8, ' '),
		util.FixStrLen(i.SalesVATnumber, 8, ' '),
		i.Number,
		fmt.Sprintf("%012d", i.Amount),
		i.TaxType,
		fmt.Sprintf("%010d", i.Tax),
		i.Deduct,
		_blank, // 特種稅額稅率
		_blank, // 彙加註記
		_blank, // 通關方式註記
		_newLine,
	)
	return result, nil
}

type EnterIncome struct {
	taxID       string // 稅籍編號
	vatNumber   string // 公司統編
	companyName string
	receiptNum  int // 發票份數

	incomeItem []*enterIncomeItem

	commonReport
}

func (bb *EnterIncome) SetCompanyName(v string) {
	bb.companyName = v
}

func (bb *EnterIncome) SetVATnumber(v string) error {
	l := len(v)
	if l != 8 {
		return errors.New("VATnumber length must be 8")
	}
	bb.vatNumber = v
	return nil
}

func (bb *EnterIncome) SetTaxNumber(v string) error {
	l := len(v)
	if l != 9 {
		return errors.New("TaxNumber length must be 9")
	}
	bb.taxID = v
	return nil
}

// 加入折讓
func (ei *EnterIncome) AddRebate(r *doc.RebateJoinReceipt) {
	var salesVat, buyerVat, deduct string
	if r.TypeAcc == doc.TypeAccIncome {
		salesVat = ei.vatNumber
		buyerVat = r.LookReceipt.VATnumber
		deduct = _blank
	} else {
		salesVat = r.LookReceipt.VATnumber
		buyerVat = ei.vatNumber
		if r.LookReceipt.IsFixedAsset {
			if r.TaxInfo.Typ == doc.TypeTaxNone {
				deduct = "4"
			} else {
				deduct = "2"
			}
		} else {
			if r.TaxInfo.Typ == doc.TypeTaxNone {
				deduct = "3"
			} else {
				deduct = "1"
			}
		}
	}
	taxType := ""
	switch r.TaxInfo.Typ {
	case doc.TypeTaxTable:
		taxType = taxAble
	case doc.TypeTaxFree:
		taxType = taxFree
	case doc.TypeTaxZero:
		taxType = taxZero
	}
	item := &enterIncomeItem{
		FormatCode:      getRebateFormatCode(r),
		YearMonth:       getYearMonth(r.DateTime),
		BuyerVATnumber:  buyerVat,
		SalesVATnumber:  salesVat,
		Number:          r.LookReceipt.Number,
		Amount:          r.Amount - r.TaxInfo.Amount,
		IsCollect:       false,
		CollectQuantity: 0,
		Tax:             r.TaxInfo.Amount,
		TaxType:         taxType,
		Deduct:          deduct,
	}
	ei.incomeItem = append(ei.incomeItem, item)
}

func getRebateFormatCode(r *doc.RebateJoinReceipt) string {
	code := make([]byte, 2)
	if r.TypeAcc == doc.TypeAccIncome {
		code[0] = '3'
		switch r.LookReceipt.Typ {
		case doc.TypeReceiptTriplicate, doc.TypeReceiptElectronic:
			code[1] = '3'
		case doc.TypeReceiptDuplicate:
			code[1] = '4'
		}
	} else {
		code[0] = '2'
		switch r.LookReceipt.Typ {
		case doc.TypeReceiptTriplicate, doc.TypeReceiptElectronic:
			code[1] = '3'
		case doc.TypeReceiptDuplicate:
			code[1] = '4'
		}
	}
	return string(code)
}

/*
 * deduct:
 * 1代表進項可扣抵之進貨及費用
 * 2代表進項可扣抵之固定資產
 * 3代表進項不可扣抵之進貨及費用
 * 4代表進項不可扣抵之固定資產
 */
func (ei *EnterIncome) AddReceipt(r *doc.Receipt) {
	var salesVat, buyerVat, deduct string
	if r.TypeAcc == doc.TypeAccIncome {
		salesVat = ei.vatNumber
		buyerVat = r.VATnumber
		deduct = _blank
		if r.IsCollect && r.Typ == doc.TypeTaxTable {
			ei.receiptNum += r.CollectQuantity
		} else {
			ei.receiptNum++
		}
	} else {
		salesVat = r.VATnumber
		buyerVat = ei.vatNumber
		if r.IsFixedAsset {
			if r.TaxInfo.Typ == doc.TypeTaxNone {
				deduct = "4"
			} else {
				deduct = "2"
			}
		} else {
			if r.TaxInfo.Typ == doc.TypeTaxNone {
				deduct = "3"
			} else {
				deduct = "1"
			}
		}
	}
	taxType := ""
	switch r.TaxInfo.Typ {
	case doc.TypeTaxTable:
		taxType = taxAble
	case doc.TypeTaxFree:
		taxType = taxFree
	case doc.TypeTaxZero:
		taxType = taxZero
	}
	ei.incomeItem = append(ei.incomeItem, &enterIncomeItem{
		FormatCode:      getReceiptFormatCode(r),
		YearMonth:       getYearMonth(r.DateTime),
		BuyerVATnumber:  buyerVat,
		SalesVATnumber:  salesVat,
		Number:          r.Number,
		Amount:          r.Amount - r.TaxInfo.Amount,
		IsCollect:       r.IsCollect,
		CollectQuantity: r.CollectQuantity,
		Tax:             r.TaxInfo.Amount,
		TaxType:         taxType,
		Deduct:          deduct,
	})

}

func getYearMonth(d time.Time) string {
	y := d.Year() - 1911
	m := d.Format("01")
	return strconv.Itoa(y) + m
}

func getReceiptFormatCode(r *doc.Receipt) string {
	code := make([]byte, 2)
	if r.TypeAcc == doc.TypeAccIncome {
		code[0] = '3'
		switch r.Typ {
		case doc.TypeReceiptTriplicate:
			code[1] = '1'
		case doc.TypeReceiptDuplicate:
			code[1] = '2'
		case doc.TypeReceiptElectronic:
			code[1] = '5'
		}
	} else {
		code[0] = '2'
		switch r.Typ {
		case doc.TypeReceiptTriplicate:
			if r.IsCollect {
				code[1] = '6'
			} else {
				code[1] = '1'
			}
		case doc.TypeReceiptDuplicate:
			if r.IsCollect {
				code[1] = '7'
			} else {
				code[1] = '2'
			}
		case doc.TypeReceiptElectronic:
			code[1] = '5'
		case doc.TypeReceiptCustoms:
			code[1] = '8'
		}
	}

	return string(code)
}

func (bb *EnterIncome) TXT(w io.Writer) error {
	var t string
	var err error
	index := 1
	for _, i := range bb.incomeItem {
		t, err = i.txt(index, bb.taxID)
		if err != nil {
			return err
		}
		_, err := w.Write([]byte(t))
		if err != nil {
			return err
		}
		index++
	}
	return nil
}

type r401 struct {
	TaxID       string `json:"taxId"`     // 稅籍編號
	VatNumber   string `json:"vatNumber"` // 公司統編
	CompanyName string `json:"companyName"`
	Owner       string `json:"owner"`
	ReceiptNum  int    `json:"receiptNumber"` // 發票份數

	// 銷售額、稅額、零稅率銷售額
	// 三聯式發票、電子計算機發票
	F1 int `json:"f1"`
	F2 int `json:"f2"`
	F3 int `json:"f3"`
	// 收銀機發票(三聯式)及電子發票
	F5 int `json:"f5"`
	F6 int `json:"f6"`
	F7 int `json:"f7"`
	// 二聯式發票、收銀機發票(二聯式)
	F9  int `json:"f9"`
	F10 int `json:"f10"`
	F11 int `json:"f11"`
	// 免用發票
	F13 int `json:"f13"`
	F14 int `json:"f14"`
	F15 int `json:"f15"`
	// 減：退回及折讓
	F17 int `json:"f17"`
	F18 int `json:"f18"`
	F19 int `json:"f19"`
	// 合計
	F21 int `json:"f21"`
	F22 int `json:"f22"`
	F23 int `json:"f23"`
	// 銷售額總計
	F25 int `json:"f25"`
	F27 int `json:"f27"`

	// 進項金額、稅額
	// 統一發票扣抵聯(進貨及費用)
	F28 int `json:"f28"`
	F29 int `json:"f29"`
	// 統一發票扣抵聯(固定資產)
	F30 int `json:"f30"`
	F31 int `json:"f31"`
	// 三聯式收銀機發票扣抵聯(進貨及費用)
	F32 int `json:"f32"`
	F33 int `json:"f33"`
	// 三聯式收銀機發票扣抵聯(固定資產)
	F34 int `json:"f34"`
	F35 int `json:"f35"`
	// 載有稅額之其他憑證(進貨及費用)
	F36 int `json:"f36"`
	F37 int `json:"f37"`
	// 載有稅額之其他憑證(固定資產)
	F38 int `json:"f38"`
	F39 int `json:"f39"`
	// 海關代徵營業稅繳納證扣抵聯(進貨及費用)
	F78 int `json:"f78"`
	F79 int `json:"f79"`
	// 海關代徵營業稅繳納證扣抵聯(固定資產)
	F80 int `json:"f80"`
	F81 int `json:"f81"`
	// 減：退回及折讓(進貨及費用)
	F40 int `json:"f40"`
	F41 int `json:"f41"`
	// 減：退回及折讓(固定資產)
	F42 int `json:"f42"`
	F43 int `json:"f43"`
	// 合計(進貨及費用)
	F44 int `json:"f44"`
	F45 int `json:"f45"`
	// 合計(固定資產)
	F46 int `json:"f46"`
	F47 int `json:"f47"`
	// 進項總金額(進貨及費用)
	F48 int `json:"f48"`
	// 進項總金額(固定資產)
	F49 int `json:"f49"`
	// 進口免稅貨物
	F73 int `json:"f73"`
	// 購買國外勞務
	F74 int `json:"f74"`
	// 本期銷項稅額合計
	F101 int `json:"f101"`
	// 得扣抵進項稅額合計
	F107 int `json:"f107"`
	// 上期累積留抵稅額
	F108 int `json:"f108"`
	// 小計
	F110 int `json:"f110"`
	// 本期應實繳稅額
	F111 int `json:"f111"`
	// 本期申報留抵稅額
	F112 int `json:"f112"`
	// 得退稅限額合計
	F113 int `json:"f113"`
	// 本期應退稅額
	F114 int `json:"f114"`
	// 本期累積留抵稅額
	F115 int `json:"f115"`
}

// type enterIncomeItem struct {
// 	FormatCode      string // 格式代號
// 	YearMonth       string // 憑證年月
// 	BuyerVATnumber  string // 買方統編
// 	SalesVATnumber  string // 賣方統編
// 	Number          string // 發票號碼
// 	Amount          int    // 銷售金額
// 	IsCollect       bool   // 是否為匯總
// 	CollectQuantity int    // 匯總張數
// 	TaxType         string
// 	Tax             int    // 營業稅
// 	Deduct          string // 扣抵代號
// }
func (bb *EnterIncome) Json(w io.Writer) error {
	r := r401{
		TaxID:       bb.taxID,
		VatNumber:   bb.vatNumber,
		CompanyName: bb.companyName,
	}
	for _, i := range bb.incomeItem {
		switch i.FormatCode {
		case "21", "26":
			// 進項三聯式、電子計算機統一發票
			switch i.Deduct {
			case "1":
				//進項可扣抵之進貨及費用
				r.F28 += i.Amount
				r.F29 += i.Tax
			case "2":
				//進項可扣抵之固定資產
				r.F30 += i.Amount
				r.F31 += i.Tax
			case "3":
				// 進項不可扣抵之進貨及費用
				r.F28 += i.Amount
			case "4":
				// 進項不可扣抵之固定資產
				r.F30 += i.Amount
			}
		case "22", "27":
			// 載有稅額之其他憑證
			switch i.Deduct {
			case "1":
				//進項可扣抵之進貨及費用
				r.F36 += i.Amount
				r.F37 += i.Tax
			case "2":
				//進項可扣抵之固定資產
				r.F38 += i.Amount
				r.F39 += i.Tax
			case "3":
				// 進項不可扣抵之進貨及費用
				r.F36 += i.Amount
			case "4":
				// 進項不可扣抵之固定資產
				r.F38 += i.Amount
			}
		case "25":
			// 三聯式收銀機發票扣抵聯
			switch i.Deduct {
			case "1":
				//進項可扣抵之進貨及費用
				r.F32 += i.Amount
				r.F33 += i.Tax
			case "2":
				//進項可扣抵之固定資產
				r.F34 += i.Amount
				r.F35 += i.Tax
			case "3":
				// 進項不可扣抵之進貨及費用
				r.F32 += i.Amount
			case "4":
				// 進項不可扣抵之固定資產
				r.F34 += i.Amount
			}
		case "28":
			// 海關代徵營業稅繳納證
			switch i.Deduct {
			case "1":
				//進項可扣抵之進貨及費用
				r.F78 += i.Amount
				r.F79 += i.Tax
			case "2":
				//進項可扣抵之固定資產
				r.F80 += i.Amount
				r.F81 += i.Tax
			case "3":
				// 進項不可扣抵之進貨及費用
				r.F78 += i.Amount
			case "4":
				// 進項不可扣抵之固定資產
				r.F80 += i.Amount
			}
		case "23", "24", "29":
			// 折讓
			switch i.Deduct {
			case "1":
				//進項可扣抵之進貨及費用
				r.F40 += i.Amount
				r.F41 += i.Tax
			case "2":
				//進項可扣抵之固定資產
				r.F42 += i.Amount
				r.F43 += i.Tax
			case "3":
				// 進項不可扣抵之進貨及費用
				r.F40 += i.Amount
			case "4":
				// 進項不可扣抵之固定資產
				r.F42 += i.Amount
			}
		case "31":
			// 銷項三聯式、電子計算機統一發票
			switch i.TaxType {
			case taxZero:
				r.F3 += i.Amount
			case taxAble:
				r.F1 += i.Amount
				r.F2 += i.Tax
			}

		case "35":
			// 銷項三聯式收銀機統一發票
			switch i.TaxType {
			case taxZero:
				r.F7 += i.Amount
			case taxAble:
				r.F5 += i.Amount
				r.F6 += i.Tax
			}
		case "32":
			// 銷項二聯式、二聯式收銀機統一發票
			switch i.TaxType {
			case taxZero:
				r.F11 += i.Amount
			case taxAble:
				r.F9 += i.Amount
				r.F10 += i.Tax
			}
		case "36":
			// 銷項免用統一發票
			switch i.TaxType {
			case taxZero:
				r.F15 += i.Amount
			case taxAble:
				r.F13 += i.Amount
				r.F14 += i.Tax
			}

		case "33", "34":
			switch i.TaxType {
			case taxZero:
				r.F19 += i.Amount
			case taxAble:
				r.F17 += i.Amount
				r.F18 += i.Tax
			}
		}
	}

	r.F21 = r.F1 + r.F5 + r.F9 + r.F13 - r.F17
	r.F22 = r.F2 + r.F6 + r.F10 + r.F14 - r.F18
	r.F23 = r.F3 + r.F7 + r.F11 + r.F15 - r.F19
	r.F25 = r.F21 + r.F23
	r.F44 = r.F28 + r.F32 + r.F36 + r.F78 - r.F40
	r.F45 = r.F29 + r.F33 + r.F37 + r.F79 - r.F41
	r.F46 = r.F30 + r.F34 + r.F38 + r.F80 - r.F42
	r.F47 = r.F31 + r.F35 + r.F39 + r.F81 - r.F43
	r.F48 = r.F44 + r.F45
	r.F49 = r.F46 + r.F47
	r.F101 = r.F22
	r.F107 = r.F45 + r.F47
	r.F110 = r.F107 + r.F108
	r.F111 = r.F101 - r.F110
	r.F112 = r.F110 - r.F101
	// 四捨五入取整數
	r.F113 = (r.F23*5+50)/100 + r.F110
	if r.F112 > r.F113 {
		r.F114 = r.F113
	} else {
		r.F114 = r.F112
	}
	r.F115 = r.F112 - r.F114

	return json.NewEncoder(w).Encode(r)
}
