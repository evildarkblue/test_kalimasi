package pdf

import (
	"io"
	"os"
	"strings"

	"github.com/94peter/gopdf"
)

type AddPagePipe interface {
	Before(p *PDF)
	After(p *PDF)
}

type PDF struct {
	myPDF                                            *gopdf.GoPdf
	width, height                                    float64
	leftMargin, topMargin, rightMargin, bottomMargin float64
	page                                             uint8
}

func GetA4PDF(fontMap map[string]string, leftMargin, rightMargin, topMargin, bottomMargin float64) PDF {
	gpdf := gopdf.GoPdf{}
	width, height := 595.28, 841.89
	gpdf.Start(gopdf.Config{PageSize: gopdf.Rect{W: width, H: height}}) //595.28, 841.89 = A4
	var err error
	for key, value := range fontMap {
		err = gpdf.AddTTFFont(key, value)
		if err != nil {
			panic(err)
		}
	}
	gpdf.SetLeftMargin(leftMargin)
	gpdf.SetTopMargin(topMargin)
	return PDF{
		myPDF:        &gpdf,
		width:        width,
		height:       height,
		leftMargin:   leftMargin,
		rightMargin:  rightMargin,
		topMargin:    topMargin,
		bottomMargin: bottomMargin,
	}
}

func GetA4HPDF(fontMap map[string]string, leftMargin, rightMargin, topMargin, bottomMargin float64) PDF {
	gpdf := gopdf.GoPdf{}
	height, width := 595.28, 841.89
	gpdf.Start(gopdf.Config{PageSize: gopdf.Rect{W: width, H: height}}) //595.28, 841.89 = A4
	var err error
	for key, value := range fontMap {
		err = gpdf.AddTTFFont(key, value)
		if err != nil {
			panic(err)
		}
	}
	gpdf.SetLeftMargin(leftMargin)
	gpdf.SetTopMargin(topMargin)
	return PDF{
		myPDF:        &gpdf,
		width:        width,
		height:       height,
		leftMargin:   leftMargin,
		rightMargin:  rightMargin,
		topMargin:    topMargin,
		bottomMargin: bottomMargin,
	}
}

const (
	ValignTop    = 1
	ValignMiddle = 2
	ValignBottom = 3
	AlignLeft    = 4
	AlignCenter  = 5
	AlignRight   = 6
)

func (p *PDF) WriteToFile(filepath string) error {
	pdf := p.myPDF
	return pdf.WritePdf(filepath)
}

func (p *PDF) Write(w io.Writer) error {
	pdf := p.myPDF
	return pdf.Write(w)
}
func (p *PDF) GetPage() uint8 {
	return p.page
}
func (p *PDF) AddPage(pp ...AddPagePipe) {
	for _, t := range pp {
		t.Before(p)
	}
	p.page++
	pdf := p.myPDF
	pdf.AddPage()
	for _, t := range pp {
		t.After(p)
	}
}

func (p *PDF) Br(h float64) {
	pdf := p.myPDF
	pdf.Br(h)
	pdf.SetX(p.leftMargin)
}

func (p *PDF) GetWidth() float64 {
	return p.width - p.leftMargin - p.rightMargin
}

func (p *PDF) GetBottomHeight() float64 {
	return p.height - p.bottomMargin
}

func (p *PDF) GetHeight() float64 {
	return p.height
}

func (p *PDF) Line(width float64) {
	pdf := p.myPDF
	p.LineWithPosition(width, p.leftMargin, pdf.GetY(), p.width-p.rightMargin, pdf.GetY())
}

func (p *PDF) LineWithPosition(width float64, x1, y1, x2, y2 float64) {
	pdf := p.myPDF
	pdf.SetLineWidth(width)
	pdf.Line(x1, y1, x2, y2)
}

func (p *PDF) TextWithPosition(text string, style TextStyle, x, y float64) {
	pdf := p.myPDF
	pdf.SetFont(style.Font, "", style.FontSize)
	textw, _ := pdf.MeasureTextWidth(text)
	rightLimit := p.width - p.rightMargin - textw
	if x < p.leftMargin {
		x = p.leftMargin
	} else if x > rightLimit {
		x = rightLimit
	}
	oy := pdf.GetY()
	ox := pdf.GetX()
	pdf.SetX(x)
	pdf.SetY(y)

	color := style.Color
	pdf.SetTextColor(color.R, color.G, color.B)
	pdf.SetFillColor(color.R, color.G, color.B)

	pdf.Cell(nil, text)
	pdf.SetX(ox)
	pdf.SetY(oy)
}

func (p *PDF) Text(text string, style TextStyle, align int) {
	pdf := p.myPDF
	pdf.SetFont(style.Font, "", style.FontSize)
	color := style.Color
	pdf.SetTextColor(color.R, color.G, color.B)
	pdf.SetFillColor(color.R, color.G, color.B)
	ox := pdf.GetX()
	if ox < p.leftMargin {
		ox = p.leftMargin
	}
	x := ox
	textw, _ := pdf.MeasureTextWidth(text)
	switch align {
	case AlignCenter:
		x = (p.width / 2) - (textw / 2)
	case AlignRight:
		x = p.width - textw - p.rightMargin
	}
	pdf.SetX(x)
	pdf.Cell(nil, text)
	pdf.SetX(ox + textw)
}

func (p *PDF) TwoColumnText(text1, text2 string, style TextStyle) {
	pdf := p.myPDF
	pdf.SetFont(style.Font, "", style.FontSize)
	color := style.Color
	pdf.SetTextColor(color.R, color.G, color.B)
	pdf.SetX(p.leftMargin)
	pdf.Cell(nil, text1)
	pdf.SetX(p.width/2 + p.leftMargin)
	pdf.Cell(nil, text2)
}

func (p *PDF) ImageReader(imageByte io.Reader) {
	//use image holder by io.Reader
	imgH2, err := gopdf.ImageHolderByReader(imageByte)
	if err != nil {
		panic(err)
	}
	pdf := p.myPDF
	pdf.ImageByHolder(imgH2, p.leftMargin, pdf.GetY(), nil)
}

func (p *PDF) Image(imagePath string) {
	//use image holder by io.Reader
	file, err := os.Open(imagePath)
	if err != nil {
		panic(err)
	}
	imgH2, err := gopdf.ImageHolderByReader(file)
	if err != nil {
		panic(err)
	}
	pdf := p.myPDF
	pdf.ImageByHolder(imgH2, p.leftMargin, pdf.GetY(), nil)
}

func (p *PDF) RectDrawColor(text string,
	style TextBlockStyle,
	w, h float64,
	align, valign int,
) {
	p.rectColorText(text, style.Font, style.Style, style.FontSize, style.Color, w, h, style.BackGround, align, valign, "D")
}

func (p *PDF) RectDrawColorWithPosition(text string,
	style TextBlockStyle,
	w, h float64,
	align, valign int,
	x, y float64,
) {
	pdf := p.myPDF
	ox, oy := pdf.GetX(), pdf.GetY()
	pdf.SetX(x)
	pdf.SetY(y)
	p.rectColorText(text, style.Font, style.Style, style.FontSize, style.Color, w, h, style.BackGround, align, valign, "D")
	pdf.SetX(ox)
	pdf.SetY(oy)
}

func (p *PDF) RectFillDrawColor(text string,
	style TextBlockStyle,
	w, h float64,
	align, valign int,
) {
	p.rectColorText(text, style.Font, style.Style, style.FontSize, style.Color, w, h, style.BackGround, align, valign, "FD")
}

func (p *PDF) RectFillDrawColorWithPosition(text string,
	style TextBlockStyle,
	w, h float64,
	align, valign int,
	x, y float64,
) {
	pdf := p.myPDF
	ox, oy := pdf.GetX(), pdf.GetY()
	pdf.SetX(x)
	pdf.SetY(y)
	p.rectColorText(text, style.Font, style.Style, style.FontSize, style.Color, w, h, style.BackGround, align, valign, "FD")
	pdf.SetX(ox)
	pdf.SetY(oy)
}

func (p *PDF) RectFillColorWithPosition(text string,
	style TextBlockStyle,
	w, h float64,
	align, valign int,
	x, y float64,
) {
	pdf := p.myPDF
	ox, oy := pdf.GetX(), pdf.GetY()
	pdf.SetX(x)
	pdf.SetY(y)
	p.rectColorText(text, style.Font, style.Style, style.FontSize, style.Color, w, h, style.BackGround, align, valign, "F")
	pdf.SetX(ox)
	pdf.SetY(oy)
}

func (p *PDF) RectFillColor(text string,
	style TextBlockStyle,
	w, h float64,
	align, valign int,
) {
	p.rectColorText(text, style.Font, style.Style, style.FontSize, style.Color, w, h, style.BackGround, align, valign, "F")
}

func (p *PDF) rectColorText(text string,
	font string,
	style string,
	fontSize int,
	textColor Color,
	w, h float64,
	color Color,
	align, valign int,
	rectType string,
) {
	pdf := p.myPDF
	pdf.SetLineWidth(0.1)
	pdf.SetFont(font, style, fontSize)
	pdf.SetFillColor(color.R, color.G, color.B) //setup fill color
	ox, x := pdf.GetX(), 0.0

	if ox < p.leftMargin {
		ox = p.leftMargin
	}
	x = ox
	pdf.RectFromUpperLeftWithStyle(x, pdf.GetY(), w, h, rectType)
	pdf.SetFillColor(0, 0, 0)

	s := strings.Split(text, "\n")
	text = s[0]
	for _, t := range s {
		if len(t) > len(text) {
			text = t
		}
	}
	if align == AlignCenter {
		textw, _ := pdf.MeasureTextWidth(text)
		x = x + (w / 2) - (textw / 2)
	} else if align == AlignRight {
		textw, _ := pdf.MeasureTextWidth(text)
		x = x + w - textw
	} else {
		x = x + 5
	}

	pdf.SetX(x)
	oy, y := pdf.GetY(), 0.0
	if valign == ValignMiddle {
		y = oy + (h / 2) - (float64(fontSize) * float64(len(s)) / 2)
	} else if valign == ValignBottom {
		y = oy + h - float64(fontSize)*float64(len(s))
	}
	pdf.SetY(y)

	pdf.SetTextColor(textColor.R, textColor.G, textColor.B)

	i := 0.0
	for _, t := range s {
		pdf.SetY(y + float64(fontSize)*i)
		pdf.SetX(x)
		pdf.Cell(nil, t)
		i++
	}

	pdf.SetY(oy)
	pdf.SetX(ox + w)

}

func (p *PDF) GetX() float64 {
	return p.myPDF.GetX()
}

func (p *PDF) GetY() float64 {
	return p.myPDF.GetY()
}
