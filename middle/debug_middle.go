package middle

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"kalimasi/input"
	"log"
	"net/http"
	"time"

	"github.com/globalsign/mgo/bson"

	"github.com/gorilla/mux"
)

type DebugMiddle string

func (lm DebugMiddle) GetName() string {
	return string(lm)
}

func (lm DebugMiddle) GetMiddleWare() func(f http.HandlerFunc) http.HandlerFunc {
	return func(f http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			getLog().Debug("-------Debug Request-------")
			ru := &input.ReqUser{
				ID:        "ffffffffffffffff00000000",
				Name:      "Peter",
				Account:   "Test",
				CompanyID: bson.ObjectIdHex("ffffffffffffffff00000000"),
				Perm:      "user",
			}
			r = ru.SaveToContext(r)
			path, _ := mux.CurrentRoute(r).GetPathTemplate()
			path = fmt.Sprintf("%s,%s?%s", r.Method, r.URL.Path, r.URL.RawQuery)
			getLog().Debug("path: " + path)
			getLog().Debug("remote address: " + r.RemoteAddr)
			getLog().Debug("User Agent: " + r.UserAgent())
			header, _ := json.Marshal(r.Header)
			getLog().Debug("header: " + string(header))
			b, err := ioutil.ReadAll(r.Body)
			r.Body.Close()
			r.Body = ioutil.NopCloser(bytes.NewBuffer(b))
			if err != nil {
				log.Printf("Error reading body: %v", err)
				http.Error(w, "can't read body", http.StatusBadRequest)
				return
			}
			getLog().Debug("body: " + string(b))

			start := time.Now()

			f(w, r)
			delta := time.Now().Sub(start)
			if delta.Seconds() > 3 {
				getLog().Err("too slow")
			}
			getLog().Debug("-------End Debug Request-------")
		}
	}
}
